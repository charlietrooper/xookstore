<?php
class ControllerAccountLibuser extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/libuser', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}



        $customer_id = $this->session->data['customer_id'];
        $email = $this->customer->getEmail();
        $this->load->model('account/libuser');
        $this->model_account_libuser->autenticacion($customer_id, $email);
        $kobo_id = $this->session->data['kobo_id'];

		$this->load->language('account/libuser');

		$this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/account/libuser.css');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_libuser'),
			'href' => $this->url->link('account/libuser', '', true)
		);



		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_order_id'] = "Tìtulo";
		$data['column_name'] = "ISBN";
		$data['column_size'] = "Editorial";
		$data['column_date_added'] = "Fecha de compra";
        $data['column_url'] = "URL de Descarga";


        $data['button_libuser'] = $this->language->get('button_download');
		$data['button_continue'] = $this->language->get('button_continue');
        $userID=$this->model_account_libuser->libreria($kobo_id);
        if (isset($userID))
        {
            $lib = (array)get_object_vars($this->model_account_libuser->libreria($kobo_id));
            $library = json_decode(json_encode($lib),true);
        }


        $data{'libuser'}=array();
        if (isset ($library['Content']))
        {
            for($i=0;$i < $library['Content']['TotalResults'];$i++)
            {
                $title= $library['Content']['Items']['LibraryInfo'][$i]['Title'];
                $isbn = $library['Content']['Items']['LibraryInfo'][$i]['Isbn'];
                $imprint = $library['Content']['Items']['LibraryInfo'][$i]['Imprint'];
                $purchasedate = $library['Content']['Items']['LibraryInfo'][$i]['PurchaseDate'];

                if ($purchasedate)
                {
                    $aa = str_replace(array('T','Z'),' ',$purchasedate);
                    $dateWithFormat = date("d-m-Y H:i:s", strtotime($aa));
                }

                $data{'libuser'}[] = array(
                    'title' => $title,
                    'isbn' => $isbn,
                    'imprint' => $imprint,
                    'purchase_date' => $dateWithFormat,
                );

            }
        }




        if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}


		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/libuser', $data));
	}

}