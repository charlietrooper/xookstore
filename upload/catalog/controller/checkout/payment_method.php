<?php
class ControllerCheckoutPaymentMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		if (isset($this->session->data['payment_address'])) {
			// Totals
			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);
					
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_total_' . $result['code']}->getTotal($total_data);
				}
			}

			// Payment Methods
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('payment');

			$recurring = $this->cart->hasRecurringProducts();

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('payment/' . $result['code']);

					$method = $this->{'model_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

					if ($method) {
                        if($method['code']=='openpay_cards')
                        {
                            $method['src']="catalog/view/theme/default/image/openpay-cc.png";
                        }
                        else if($method['code']=='openpay_stores')
                        {
                            $method['src']="catalog/view/theme/default/image/openpay_stores.png";
                        }
                        else if($method['code']=='openpay_banks')
                        {
                            $method['src']="catalog/view/theme/default/image/spei.png";
                        }

						if ($recurring) {
							if (property_exists($this->{'model_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_payment_' . $result['code']}->recurringPayments()) {
								$method_data[$result['code']] = $method;
							}
						} else {
							$method_data[$result['code']] = $method;
						}
					}
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);
            //Con este chequeo vamos a mandar una variable para la vista que detecte si es free checkout
        /*    if(count($method_data)===1 && $method_data[0]['code']=='free_checkout')
            {
                $data['freeCheckout']= true;

            }

            else{
                $data['freeCheckout']= false;
            }
        */

			$this->session->data['payment_methods'] = $method_data;
		}

		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['payment_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['payment_methods'])) {
			$data['payment_methods'] = $this->session->data['payment_methods'];
		} else {
			$data['payment_methods'] = array();
		}

		if (isset($this->session->data['payment_method']['code'])) {
			$data['code'] = $this->session->data['payment_method']['code'];
		} else {
			$data['code'] = '';
		}
/*
		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}*/

		$data['scripts'] = $this->document->getScripts();

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_checkout_id'), true), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->session->data['agree'])) {
			$data['agree'] = $this->session->data['agree'];
		} else {
			$data['agree'] = '';
		}
        /*------------------------------------------*/

        $verification=false;
        $products = $this->cart->getProducts();
        $verify = array();
        $msg='';
            if(!$verification) {
                //Se determina la fecha para nombrar el archivo de log
                $fecha_log = date('Y-m-d');
                $logger = new Log("purchases/purchases_on_$fecha_log.log");
                $customer_id = $this->session->data['customer_id'];
                $email = $this->customer->getEmail();
                $this->load->model('checkout/api');
                $api_aut = $this->model_checkout_api->autenticacion($customer_id, $email);

                if(!$api_aut) {
                    $verify['api']['error'] = 'Error de conexión';
                    $msg='errConexion';
                    $logger->write('Error: ' . json_encode($verify['api']['error']));
                } elseif(isset($api_aut['error_autentication'])) {
                    $verify['api']['error'] = $api_aut['error'];
                    $msg='errAut';
                    $logger->write('Error: ' . json_encode($verify['api']['error']));
                } elseif(isset($api_aut["Result"])) {
                    if($api_aut["Result"] == "Success") {
                        $logger->write('Respuesta de la autenticación: ' . json_encode($api_aut));
                        unset($res);
                        $direccion = array();
                        $direccion['City'] = $this->session->data['payment_address']['city'];
                        $direccion['Country'] = $this->session->data['payment_address']['iso_code_2'];
                        $direccion['StateProvince'] = $this->session->data['payment_address']['zone'];
                        $direccion['ZipPostalCode'] = $this->session->data['payment_address']['postcode'];

                        $product_ids = array();
                        $prods = array();
                        foreach ($products as $product) {
                            $product_ids[] = $product['model'];
                            $prods[] = array(
                                'id' => $product['product_id'],
                                'active_id' => $product['model']
                            );
                        }

                        $post_fields = json_encode(array(
                            'user_id' => $this->session->data['kobo_id'],
                            'product_ids' => $product_ids,
                            'address' => $direccion
                        ));

                        $api_verif = $this->model_checkout_api->verificacion($post_fields,$prods);
                        if(!$api_verif) {
                            $verify['api']['error'] = 'Error de conexión';
                            $msg='errConexion';
                        } elseif(isset($api_verif['error_purchase'])) {
                            $verify['api']['error'] = $api_verif['error'];
                            $msg='errPurchase';
                        } elseif(isset($api_verif["resultado"])) {
                            if($api_verif["resultado"]["Result"] == "Success") {
                                $logger->write('Respuesta de la verificación: ' . json_encode($api_verif));
                                $verify['api'] = $api_verif;
                                $msg='Success';
                            }
                            else {
                                $verify['api']['error']['verificacion'] = $api_verif;
                                $msg='errProblema';
                                $logger->write('Error: ' . json_encode($api_verif));
                            }
                        } else {
                            $verify['api']['error'] = 'Error de verificacicación: intente de nuevo';
                            $msg='errVerificacion';
                            $logger->write('Error: ' . json_encode($verify['api']['error']));
                        }
                    } else {
                        $verify['api']['error']['autenticacion'] = json_encode($api_aut);
                        $msg='errAut';
                        $logger->write('Error: ' . json_encode($api_aut));
                    }
                }else {
                    $verify['api']['error'] = 'Error de autenticación: intente de nuevo';
                    $msg='errAut';
                    $logger->write('Error: ' . json_encode($verify['api']['error']));
                }
            }

        $btnContinue=$data["button_continue"];
        $textLoading=$data["text_loading"];

        if($msg=='Success')
        {
            $data['mostrarPanel']=false;
            $data['librosError']=null;
            $data['btninfo']="<input type='button' value='$btnContinue' id='button-payment-method' data-loading-text='$textLoading' class='btn btn-primary'";

        }
        else{
            $data['mostrarPanel']=true;
            $data['librosError']=$this->verifySuccess($verify, $msg);
            //$data['btninfo']="<input disabled type='button' value='$btnContinue' id='button-payment-method' data-loading-text='$textLoading' class='btn btn-primary'";
            $data['btninfo']="<a href='index.php?route=checkout/cart' class='btn btn-info'>Regresar al carrito <i class='fa fa-shopping-cart'></i></a>";


        }

       // $data['verify']=$verify;

		$this->response->setOutput($this->load->view('checkout/payment_method', $data));
	}

    public function verifySuccess($verifyresult, $msg)
    {
        $arrLibrosError=array();

        switch($msg){
            case("errProblema"):
                foreach($verifyresult['api']['error']['verificacion']['resultado']['Items']['Item'] as $libro)
                {
                    if($libro['Result']!=='Success')
                    {
                        if($libro['Result']=='ContentAlreadyOwned')
                        {
                            $msg="<p class='msgLibro'> El libro '". $libro['Titulo'] ."' ya se encuentra en su biblioteca, por favor revise la sección de Librería de Usuario en su cuenta. </p>";
                            array_push($arrLibrosError, $msg);
                        }
                        else if($libro['Result']=='ContentPending')
                        {
                            $msg="<p class='msgLibro'> No puede volver a comprar el libro '". $libro['Titulo'] ."' , pues su cuenta ya tiene una orden pendiente por el mismo libro . </p>";
                            array_push($arrLibrosError, $msg);
                        }
                        else if($libro['Result']=='InternalError' || $libro['Result']=='ContentNotActive')
                        {
                            $msg="<p class='msgLibro'> Lo sentimos, el libro '". $libro['Titulo'] ."' no se encuentra disponible actualmente. </p>";
                            array_push($arrLibrosError, $msg);
                        }
                        else{
                            $msg="<p class='msgLibro'> Lo sentimos. Existe un error interno del sistema al intentar obtener los datos del libro '". $libro['Titulo'] ."' . </p>";
                            array_push($arrLibrosError, $msg);
                        }
                    }
                }
                break;
            case("errAut"):
                $msg="<p class='msgLibro'> Lo sentimos, hubo un error de autenticación con su cuenta. Por favor, inténtelo más tarde.</p>";
                array_push($arrLibrosError, $msg);
                break;
            case("errConexion"):
                $msg="<p class='msgLibro'> Lo sentimos, hubo un error de conexión con el servidor. Favor de intentar más tarde. </p>";
                array_push($arrLibrosError, $msg);

                break;
            case("errVerificacion"):
                $msg="<p class='msgLibro'> Lo sentimos, hubo un problema al intentar verificar sus libros. Por favor, inténtelo más tarde. </p>";
                array_push($arrLibrosError, $msg);
                break;
            default:
                $msg="<p class='msgLibro'> Lo sentimos. Existe un error interno de nuestro sistema al intentar verificar sus libros. Por favor, inténtelo más tarde.</p>";
                array_push($arrLibrosError, $msg);
                break;
        }


        return $arrLibrosError;

    }

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!isset($this->request->post['payment_method'])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		} elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		}

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		if (!$json) {
			$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];

			//$this->session->data['comment'] = strip_tags($this->request->post['comment']);
            $this->session->data['comment'] = "";
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
