<?php
class ControllerCommonSearch extends Controller {
	public function index() {
		$this->load->language('common/search');

		$data['text_search'] = $this->language->get('text_search');
		$data['text_searchCategories'] = $this->language->get('text_searchCategories');
		$data['text_cbAutor'] = $this->language->get('text_cbAutor');
		$data['text_cbTexto'] = $this->language->get('text_cbTexto');
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/common/search.css');

        $data['categories'] = array();
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategories(59);

       if ($categories) {
           foreach($categories as $child) {

               $children_data[] = array(
                   'category_id' => $child['category_id'],
                   'name' => $child['name']
               );
           }
       }

        $data['categories']=$children_data;


		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}

        if (isset($this->request->get['searchCat'])) {
            $data['searchCat'] = $this->request->get['searchCat'];
        } else {
            $data['searchCat'] = '';
        }

        if (isset($this->request->get['searchText'])) {
            $data['searchText'] = $this->request->get['searchText'];
        } else {
            $data['searchText'] = '';
        }



		return $this->load->view('common/search', $data);
	}
}