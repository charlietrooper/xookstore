<?php
class ControllerModuleBarraCategorias extends Controller {
    public function index($setting) {
        static $module = 1;
        $this->load->language('module/barraCategorias');

        $data['heading_title'] = $setting['name'];

        //Llamamos método para el color del header
        $data['color1'] = $this->colorHeader($setting['color1']);
        $data['color2'] = $this->colorHeader($setting['color2']);
        $data['color3'] = $this->colorHeader($setting['color3']);
        $data['color4'] = $this->colorHeader($setting['color4']);

        $data['alias1'] = $this->splitCategories($setting['alias1']);
        $data['alias2'] = $this->splitCategories($setting['alias2']);
        $data['alias3'] = $this->splitCategories($setting['alias3']);
        $data['alias4'] = $this->splitCategories($setting['alias4']);

        $data['href1']=$this->url->link('product/category', 'path=59' . '_' . $setting['categories1']);
        $data['href2']=$this->url->link('product/category', 'path=59' . '_' . $setting['categories2']);
        $data['href3']=$this->url->link('product/category', 'path=59' . '_' . $setting['categories3']);
        $data['href4']=$this->url->link('product/category', 'path=59' . '_' . $setting['categories4']);





        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/module/barraCategorias.css');


        return $this->load->view('module/barraCategorias', $data);

    }

    public function splitCategories($category)
    {
        $code='';
        $bandera=strpos($category, 'e');
       // echo $bandera;
        $titulo=explode('e',$category);
       // return $titulo;

        if($bandera)
        {
            $code.= "<p>";
            for($parte=0;$parte<sizeof($titulo);$parte++)
            {
                $code.= "<span>".$titulo[$parte]."</span>";
                if(($parte+1)!==sizeof($titulo))
                {
                    $code.= "<b>e</b>";
                }

            }
            $code.= "</p>";
        }

        else
        {
            $code.= "<p>";
            $code.= "<span>".$category."</span>";
            $code.= "</p>";
        }
        return $code;

    }

    public function colorHeader($selectedColor)
    {
        $headerColor='';
        switch($selectedColor)
        {
            case('Rojo'):
            {
                $headerColor="rojo";
                break;
            }
            case('Verde'):
            {
                $headerColor="verde";
                break;
            }
            case('Azul'):
            {
                $headerColor="azul";
                break;
            }

            case('Morado'):
            {
                $headerColor="morado";
                break;
            }

            case('Gris'):
            {
                $headerColor="gris";
                break;
            }
            default:
            {
                $headerColor="verde";
            }


        }

        return $headerColor;
    }

}