<?php
class ControllerModuleFavoritos extends Controller {
	public function index($setting) {
		static $module = 1;
		$this->load->language('module/favoritos');

		$data['heading_title'] = $setting['name'];

        //Llamamos método para el color del header
        //Llamamos método para el color del header

		$data['text_tax'] = $this->language->get('text_tax');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.theme.css');
		$this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/module/favoritos.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');


		$data['products'] = array();


		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, 4);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					$additional_image ="";
					if ($product_info['image']) {
                        if ($product_info['image']) {
                            $image = $this->model_tool_image->resize($product_info['image'], 200, 200);
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', 200, 200);
                        }

						$additional_images = $this->model_catalog_product->getProductImages($product_id);
						if($additional_images){
							$additional_image=$this->model_tool_image->resize($additional_images[0]['image'], 200, 200);
						}else{
							$additional_image =$this->model_tool_image->resize($product_info['image'], 200, 200);
						}
					} else {
						$additional_image =$image = $this->model_tool_image->resize('placeholder.png', 200, 200);
					}


					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}


                    $this->load->model('catalog/productdata');
                    $precioFinal=$this->model_catalog_productdata->obtenerPrecioFinal($price, $special);
                    $nombreReducido=$this->model_catalog_productdata->reducirNombre($product_info['name']);

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'additional_image'       => $additional_image,
						'name'        => $product_info['name'],
                        'headerName'  => $nombreReducido,
                        'author'      => $product_info['author'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                        //'resize'      => $resize,
                        'precioFinal' => $precioFinal
					);
				}
			}
		}
		$data['module'] = $module++;
		$data['linkFavoritos'] = $this->url->link('seccion/favoritos');
		if ($data['products']) {
			return $this->load->view('module/favoritos', $data);
		}
	}



}