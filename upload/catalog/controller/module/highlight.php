<?php
class ControllerModuleHighlight extends Controller {
    public function index($setting) {
        $this->load->language('module/highlight');

        $data['heading_title'] = $setting['name'];
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/module/highlight.css');
        $setting['limit'] = 1;
        //$this->document->addScript('catalog/view/javascript/jquery/jquery.fittext.js');
        $data['color'] = $setting['color'];
        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['products'] = array();

   /*     if (!$setting['limit']) {
            $setting['limit'] = 1;
        }*/

        if (!empty($setting['product'])) {
            $products = array_slice($setting['product'], 0, (int)$setting['limit']);

            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);

                if ($product_info) {
                    if ($product_info['image']) {
                        if(substr($product_info['image'], 0,4) == 'http') {
                            $image = $product_info['image'];
                        } else {
                            $image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                        }
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float)$product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_info['rating'];
                    } else {
                        $rating = false;
                    }

                    $infoLibro=$this->obtenerInformacion($product_info['model']);
                    $descripcion=$this->obtenerDescripcion($infoLibro);
                    $autor=$this->obtenerAutor($infoLibro);
                    $precioFinal=$this->obtenerPrecioFinal($price, $special);
                    $thumbHeight=$this->obtenerHeight($image);

                    $data['products'][] = array(
                        'product_id'  => $product_info['product_id'],
                        'thumb'       => $image,
                        'name'        => $product_info['name'],
                        'activeID'    => $product_info['model'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price'       => $precioFinal,
                        'special'     => $special,
                        'tax'         => $tax,
                        'rating'      => $rating,
                        'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                        'descripcion' =>$descripcion,
                        'autor'       =>$autor,
                        'imgHeight'   =>$thumbHeight
                    );


                }
            }
        }

        if ($data['products']) {
            return $this->load->view('module/highlight', $data);
        }
    }

    public function obtenerInformacion($activeID)
    {
        $url="https://catalog.orbile.com/books/getBook/rev/".$activeID;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);

        //Se decodifica el Json y se asigna al objeto
        $result = json_decode($resultado, true);
        return $result;
    }

    public function obtenerDescripcion($infoLibro)
    {
        //$descripcion='Descripción no disponible';
        $descripcion=array();
        $descripcionBig='Descripción no disponible';
        $descripcionSmall='Descripción no disponible';
        if($infoLibro)
        {
            $desc=strip_tags($infoLibro['activeRevision']['description']);
            if(strlen($desc)>600)
            {
                $temp2 = substr($desc,0,600);
                $temp3 = substr($desc,0,200);
                $descripcionBig=$temp2.'...';
                $descripcionSmall=$temp3.'...';
            }
            else{
                $descripcionBig=$desc;
                $temp3 = substr($desc,0,250);
                $descripcionSmall=$temp3.'...';
            }

        }
        array_push($descripcion,$descripcionBig);
        array_push($descripcion,$descripcionSmall);

        return $descripcion;
    }

    public function obtenerAutor($infoLibro)
    {
        $aut='';
        if ($infoLibro)
        {
            $aut=' / ';
           // $what=isset($infoLibro['activeRevision']['contibutors']['contributor']['@value']);
            if(isset($infoLibro['activeRevision']['contibutors']['contributor']['@value']))
            {
                $aut.=$infoLibro['activeRevision']['contibutors']['contributor']['@value'];
            }
            else
            {
                $autores=$infoLibro['activeRevision']['contibutors']['contributor'];
                $count=count($autores);
                $n=1;
                foreach($autores as $autor)
                {
                    if (!($n>2))
                    {
                        $holder=$autor['@value'];
                        if (!($n==$count))
                        {
                            $aut.=$holder.", ";
                        }
                        else{
                            $aut.=$holder;
                        }
                    }

                    $n++;

                }
                if($count>2)
                {
                    $temp=$aut;
                    $aut=substr($temp, 0, -2);
                }

            }
        }

        return $aut;
    }

    public function obtenerHeight($img)
    {
        $height=0;
        if($img)
        {
            $imageSize=getimagesize($img);
            $height=$imageSize[1];
        }

        return $height;
    }

    public function obtenerPrecioFinal($precio, $special)
    {
        $precioFinal=0;
        if ($precio)
        {
            $precioFinal=0;

            if (!$special)
            {
                $precioFinal=$precio;
            }
            else
            {
                $precioFinal=$special;
            }
        }
        return $precioFinal;
    }
}