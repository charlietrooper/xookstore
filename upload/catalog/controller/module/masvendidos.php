<?php
class ControllerModuleMasvendidos extends Controller {
	public function index($setting) {
        static $module = 0;
		$this->load->language('module/masvendidos');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.theme.css');
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/module/masvendidos.css');
        $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

        if (!$setting['masvendidos_autoplay_speed']) {
            $data['masvendidos_autoplay_speed'] = 0;
            $data['masvendidos_autoplay_speed_status'] = "false";
        }else{
            $data['masvendidos_autoplay_speed'] = $setting['masvendidos_autoplay_speed'];
            $data['masvendidos_autoplay_speed_status'] = "true";
        }

		$data['products'] = array();

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

                $this->load->model('catalog/productdata');
                $precioFinal=$this->model_catalog_productdata->obtenerPrecioFinal($price, $special);
                $nombreReducido=$this->model_catalog_productdata->reducirNombre($result['name']);

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
                    'headerName'  => $nombreReducido,
                    'author'      => $result['author'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                    //'resize'      => $resize,
                    'precioFinal' => $precioFinal
				);
			}
            $data['module'] = $module++;
			return $this->load->view('module/masvendidos', $data);
		}
	}

}
