<?php
class ControllerModulePromotions extends Controller {
	public function index($setting) {
		static $module = 1;
		$this->load->language('module/promotions');

		$data['heading_title'] = $setting['name'];

        //Llamamos método para el color del header
        //$data['color'] = $this->colorHeader($setting['color']);

		$data['text_tax'] = $this->language->get('text_tax');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.theme.css');
		$this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/module/promotions.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }


		$data['products'] = array();


		if (!empty($setting['product'])) {
            $limit=20;
            $start = ($page - 1) * $limit;
            $length = $limit;
            $totalCount=count($setting['product']);
			$products = array_slice($setting['product'], $start, $length);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					$additional_image ="";
					if ($product_info['image']) {
                        if ($product_info['image']) {
                            $image = $this->model_tool_image->resize($product_info['image'], 200, 200);
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', 200, 200);
                        }
                         /*
                        if(substr($product_info['image'], 0,4) == 'http') {
                            $image = $product_info['image'];
                        } else {
                            $image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                        }*/
						$additional_images = $this->model_catalog_product->getProductImages($product_id);
						if($additional_images){
							$additional_image=$this->model_tool_image->resize($additional_images[0]['image'], 200, 200);
						}else{
							$additional_image =$this->model_tool_image->resize($product_info['image'], 200, 200);
						}
					} else {
						$additional_image =$image = $this->model_tool_image->resize('placeholder.png', 200, 200);
					}


					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

                    //Este método manda true si hay que hacerle resize a la imagen si esta muy chiquita en altura
                    //$resize=$this->resizeCheck($image);
                    //$this->load->model('tool/sizecheck');
                   // $resize=$this->model_tool_sizecheck->imageDimensions($image);

                    $precioFinal=$this->obtenerPrecioFinal($price, $special);
                    $nombreReducido=$this->reducirNombre($product_info['name']);

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'additional_image'       => $additional_image,
						'name'        => $product_info['name'],
                        'headerName'  => $nombreReducido,
                        'author'      => $product_info['author'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                        //'resize'      => $resize,
                        'precioFinal' => $precioFinal
					);
				}
			}
		}
		//$data['module'] = $module++;
		if ($data['products']) {

            $linkPaginacion='promo/promocion0'.$setting['paginaPromo'];
            $url = '';
            $product_total = $totalCount;
            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link($linkPaginacion, $url . '&page={page}');;

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total);

            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
            if ($page == 1) {
                $this->document->addLink($this->url->link($linkPaginacion, true), 'canonical');
            } elseif ($page == 2) {
                $this->document->addLink($this->url->link($linkPaginacion, true), 'prev');
            } else {
                $this->document->addLink($this->url->link($linkPaginacion, '&page=' . ($page - 1), true), 'prev');
            }

            if ($limit && ceil($product_total / $limit) > $page) {
                $this->document->addLink($this->url->link($linkPaginacion, '&page=' . ($page + 1), true), 'next');
            }

         /*   $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit; */
			return $this->load->view('module/promotions', $data);
		}
	}

    public function colorHeader($selectedColor)
    {
        $headerColor='';
        switch($selectedColor)
        {
            case('Rojo'):
            {
                $headerColor="Rojo";
                break;
            }
            case('Verde'):
            {
                $headerColor="Verde";
                break;
            }
            case('Azul'):
            {
                $headerColor="Azul";
                break;
            }
            case('Morado'):
            {
                $headerColor="Morado";
                break;
            }
            default:
            {
                $headerColor="Verde";
            }


        }

        return $headerColor;
    }

    public function obtenerPrecioFinal($precio, $special)
    {
        $precioFinal=0;
        if ($precio)
        {
            $precioFinal=0;

            if (!$special)
            {
                $precioFinal=$precio;
            }
            else
            {
                $precioFinal=$special;
            }
        }
        return $precioFinal;
    }
    public function reducirNombre($nombre)
    {
        $headerNombre="";

        if($nombre)
        {
            if(strlen($nombre)>40)
            {
                $temp = substr($nombre,0,40);
                $headerNombre=$temp.'...';
            }
            else{
                $headerNombre=$nombre;
            }
        }

        return $headerNombre;
    }
}