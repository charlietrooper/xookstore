<?php
class ControllerModuleWebocreationfeatured extends Controller {
	public function index($setting) {
		static $module = 0;
		$this->load->language('module/webocreationfeatured');

		$data['heading_title'] = $setting['name'];

		$data['text_tax'] = $this->language->get('text_tax');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.theme.css');
		$this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/module/webocreationfeatured.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (!$setting['webocreation_autoplay_speed']) {
			$data['webocreation_autoplay_speed'] = 0;
			$data['webocreation_autoplay_speed_status'] = "false";
		}else{
			$data['webocreation_autoplay_speed'] = $setting['webocreation_autoplay_speed'];
			$data['webocreation_autoplay_speed_status'] = "true";
		}
		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					$additional_image ="";
					if ($product_info['image']) {
                        if(substr($product_info['image'], 0,4) == 'http') {
                            $image = $product_info['image'];
                        } else {
                            $image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                        }
						$additional_images = $this->model_catalog_product->getProductImages($product_id);
						if($additional_images){
							$additional_image=$this->model_tool_image->resize($additional_images[0]['image'], $setting['width'], $setting['height']);
						}else{
							$additional_image =$this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
						}
					} else {
						$additional_image =$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}


					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'additional_image'       => $additional_image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}
		$data['module'] = $module++;
		if ($data['products']) {
			return $this->load->view('module/webocreationfeatured', $data);
		}
	}
}