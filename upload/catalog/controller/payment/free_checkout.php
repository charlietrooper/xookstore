<?php
class ControllerPaymentFreeCheckout extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		return $this->load->view('payment/free_checkout', $data);
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'free_checkout') {
			$this->load->model('checkout/order');

            $this->load->model('catalog/libro');

            $products = $this->cart->getProducts();
            $productos = array();
            foreach($products as $p) {
                $libro = $this->model_catalog_libro->obtenDatos($p['product_id']);
                $productos[] = array(
                    'COGS' => array(
                        'Amount' => $libro['precio'],
                        'CurrencyCode' => $libro['currency']
                    ),
                    'PriceCharged' => array(
                        'CurrencyCode' => $libro['currency'],
                        'DiscountApplied' => (int)$libro['precio'] - (int)$libro['prec'],
                        'PriceBeforeTax' => $libro['precio'],
                    ),
                    'ProductId' => $p['model']
                );
            }

            $direccion = array(
                'City' => $this->session->data['payment_address']['city'],
                'Country' => $this->session->data['payment_address']['iso_code_2'],
                'StateProvince' => $this->session->data['payment_address']['zone'],
                'ZipPostalCode' => $this->session->data['payment_address']['postcode']
            );

            //Se agregan a $purchase_files todos los datos de la compra
            $purchases_files = json_encode(array(
                'PurchaseId' => $this->session->data['order_id'],
                'user_id' => $this->session->data['kobo_id'],
                'Products' => $productos,
                'payment' => 'Tarjeta',
                'Address' => $direccion
            ));

            $this->load->model('checkout/api');
            $api_purchase = $this->model_checkout_api->compra($purchases_files);
            $this->session->data['api_purchase'] = $api_purchase;

            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('free_checkout_order_status_id'));
		}
	}
}