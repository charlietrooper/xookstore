<?php

/**
 * @version Opencart v2.0.1.1
 */
if (!defined('OWNER'))
    define('OWNER', 'Customer');

class ControllerPaymentOpenpayWebhook extends Controller {

    public function __construct($registry) {
        parent::__construct($registry);
    }

    public function index() {

    }

    public function webhook(){
        $objeto = file_get_contents('php://input');
        $json = json_decode($objeto);
        $jsonString=json_encode($objeto);

        if(!count($json)>0)
            return true;

        if($json)
        {
            $this->load->model('checkout/order');
            $this->model_checkout_order->getWebhookJSON($json->transaction->order_id,$jsonString);
        }

        if($json->type == 'verification' )
        {
            $this->load->model('payment/prueba');
            $this->model_payment_prueba->addVerify($json->verification_code);

        }

        if ($json->type == 'charge.succeeded' && $json->transaction->method == 'card') {
            $this->load->model('checkout/order');
            $this->model_checkout_order->addCommentJSON($json->transaction->order_id, "",$jsonString);
        }

        if ($json->type == 'charge.succeeded' && $json->transaction->method == 'store') {
            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($json->transaction->order_id, $this->config->get('openpay_new_status_id'), '', true);
            $this->model_checkout_order->addCommentJSON($json->transaction->order_id, $this->config->get('openpay_new_status_id'),$jsonString);


            //Se determina la fecha para nombrar el archivo de log
            $fecha_log = date('Y-m-d');
            $logger = new Log("purchases/purchases_on_$fecha_log.log");

            $orden = $this->model_checkout_order->getOrder($json->transaction->order_id);
            $productos = $this->model_checkout_order->getOrderProducts($json->transaction->order_id);

            $this->load->model('checkout/api');
            $api_aut = $this->model_checkout_api->autenticacion($orden['customer_id'], $orden['email']);
            $logger->write('Resultado de la autenticación: ' . json_encode($api_aut));

            if(!$api_aut) {
                $logger->write('Error de conexión en la autenticación');
            } elseif(isset($api_aut['error_autentication'])) {
                $logger->write('Error: ' . json_encode($api_aut['error']));
            } elseif(isset($api_aut["Result"])) {
                if($api_aut["Result"] == "Success") {
                    $direccion = array();
                    $direccion['City'] = $orden['payment_city'];
                    $direccion['Country'] = $orden['payment_iso_code_2'];
                    $direccion['StateProvince'] = $orden['payment_zone'];
                    $direccion['ZipPostalCode'] = $orden['payment_postcode'];

                    $product_ids = array();
                    $prods = array();
                    foreach ($productos as $product) {
                        $product_ids[] = $product['model'];
                        $prods[] = array(
                            'id' => $product['product_id'],
                            'active_id' => $product['model']
                        );
                    }

                    $post_fields = json_encode(array(
                        'user_id' => $api_aut['KoboUserId'],
                        'product_ids' => $product_ids,
                        'address' => $direccion
                    ));

                    $api_verif = $this->model_checkout_api->verificacion($post_fields,$prods);
                    $logger->write('Resultado de la verificación: ' . json_encode($api_verif));

                    if(!$api_verif) {
                        $logger->write('Error de conexión en la verificación');
                    } elseif(isset($api_verif['error_purchase'])) {
                        $logger->write(json_encode($api_verif['error']));
                    } elseif(isset($api_verif["resultado"])) {
                        if($api_verif["resultado"]["Result"] == "Success") {

                            $this->load->model('catalog/libro');

                            $productos_purchase = array();
                            foreach($productos as $p) {
                                $libro = $this->model_catalog_libro->obtenDatos($p['model']);
                                $productos_purchase[] = array(
                                    'COGS' => array(
                                        'Amount' => $libro['precio'],
                                        'CurrencyCode' => $libro['currency']
                                    ),
                                    'PriceCharged' => array(
                                        'CurrencyCode' => $libro['currency'],
                                        'DiscountApplied' => (int)$libro['precio'] - (int)$libro['prec'],
                                        'PriceBeforeTax' => $libro['precio'],
                                    ),
                                    'ProductId' => $p['model']
                                );
                            }

                            $direccion = array(
                                'City' => $orden['payment_city'],
                                'Country' => $orden['payment_iso_code_2'],
                                'StateProvince' => $orden['payment_zone'],
                                'ZipPostalCode' => $orden['payment_postcode']
                            );

                            //Se agregan a $purchase_files todos los datos de la compra
                            $purchases_files = json_encode(array(
                                'PurchaseId' => $json->transaction->order_id,
                                'user_id' => $api_aut['KoboUserId'],
                                'Products' => $productos_purchase,
                                'payment' => 'Pago en banco',
                                'Address' => $direccion
                            ));

                            $api_purchase = $this->model_checkout_api->compra($purchases_files);
                            $logger->write('Resultado de la compra: ' . json_encode($api_purchase));

                        }
                        else {
                            $logger->write('Error: ' . json_encode($api_verif));
                        }
                    } else {
                        $logger->write('Error: Error de verificacicación: intente de nuevo');
                    }
                } else {
                    $logger->write('Error: ' .  json_encode($api_aut));
                }
            }else {
                $logger->write('Error: Error de autenticación: intente de nuevo');
            }
        }

        if ($json->type == 'charge.succeeded' && $json->transaction->method == 'bank_account') {
            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($json->transaction->order_id, $this->config->get('openpay_bank_new_status_id'));
            $this->model_checkout_order->addCommentJSON($json->transaction->order_id, $this->config->get('openpay_bank_new_status_id'),$jsonString);

            //Se determina la fecha para nombrar el archivo de log
            $fecha_log = date('Y-m-d');
            $logger = new Log("purchases/purchases_on_$fecha_log.log");

            $orden = $this->model_checkout_order->getOrder($json->transaction->order_id);
            $productos = $this->model_checkout_order->getOrderProducts($json->transaction->order_id);

            $this->load->model('checkout/api');
            $api_aut = $this->model_checkout_api->autenticacion($orden['customer_id'], $orden['email']);
            $logger->write('Resultado de la autenticación: ' . json_encode($api_aut));

            if(!$api_aut) {
                $logger->write('Error de conexión en la autenticación');
            } elseif(isset($api_aut['error_autentication'])) {
                $logger->write('Error: ' . json_encode($api_aut['error']));
            } elseif(isset($api_aut["Result"])) {
                if($api_aut["Result"] == "Success") {
                    $direccion = array();
                    $direccion['City'] = $orden['payment_city'];
                    $direccion['Country'] = $orden['payment_iso_code_2'];
                    $direccion['StateProvince'] = $orden['payment_zone'];
                    $direccion['ZipPostalCode'] = $orden['payment_postcode'];

                    $product_ids = array();
                    $prods = array();
                    foreach ($productos as $product) {
                        $product_ids[] = $product['model'];
                        $prods[] = array(
                            'id' => $product['product_id'],
                            'active_id' => $product['model']
                        );
                    }

                    $post_fields = json_encode(array(
                        'user_id' => $api_aut['KoboUserId'],
                        'product_ids' => $product_ids,
                        'address' => $direccion
                    ));

                    $api_verif = $this->model_checkout_api->verificacion($post_fields,$prods);
                    $logger->write('Resultado de la verificación: ' . json_encode($api_verif));

                    if(!$api_verif) {
                        $logger->write('Error de conexión en la verificación');
                    } elseif(isset($api_verif['error_purchase'])) {
                        $logger->write(json_encode($api_verif['error']));
                    } elseif(isset($api_verif["resultado"])) {
                        if($api_verif["resultado"]["Result"] == "Success") {

                            $this->load->model('catalog/libro');

                            $productos_purchase = array();
                            foreach($productos as $p) {
                                $libro = $this->model_catalog_libro->obtenDatos($p['model']);
                                $productos_purchase[] = array(
                                    'COGS' => array(
                                        'Amount' => $libro['precio'],
                                        'CurrencyCode' => $libro['currency']
                                    ),
                                    'PriceCharged' => array(
                                        'CurrencyCode' => $libro['currency'],
                                        'DiscountApplied' => (int)$libro['precio'] - (int)$libro['prec'],
                                        'PriceBeforeTax' => $libro['precio'],
                                    ),
                                    'ProductId' => $p['model']
                                );
                            }

                            $direccion = array(
                                'City' => $orden['payment_city'],
                                'Country' => $orden['payment_iso_code_2'],
                                'StateProvince' => $orden['payment_zone'],
                                'ZipPostalCode' => $orden['payment_postcode']
                            );

                            //Se agregan a $purchase_files todos los datos de la compra
                            $purchases_files = json_encode(array(
                                'PurchaseId' => $json->transaction->order_id,
                                'user_id' => $api_aut['KoboUserId'],
                                'Products' => $productos_purchase,
                                'payment' => 'Pago en banco',
                                'Address' => $direccion
                            ));

                            $api_purchase = $this->model_checkout_api->compra($purchases_files);
                            $logger->write('Resultado de la compra: ' . json_encode($api_purchase));


                        }
                        else {
                            $logger->write('Error: ' . json_encode($api_verif));
                        }
                    } else {
                        $logger->write('Error: Error de verificacicación: intente de nuevo');
                    }
                } else {
                    $logger->write('Error: ' .  json_encode($api_aut));
                }
            }else {
                $logger->write('Error: Error de autenticación: intente de nuevo');
            }
        }
    }

}

?>
