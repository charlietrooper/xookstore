<?php
class ControllerProductSearch extends Controller {
	public function index() {
		$this->load->language('product/search');
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/product/search.css');
        $this->document->addScript('catalog/view/javascript/rangeslider/js/ion.rangeSlider.js');
        $this->document->addStyle('catalog/view/javascript/rangeslider/css/ion.rangeSlider.css');
        $this->document->addStyle('catalog/view/javascript/rangeslider/css/ion.rangeSlider.skinFlat.css');

        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/notification-popup/popup.css');
        $this->load->language('common/popup');
        $data['popup_code'] = sprintf($this->language->get('popup_code'), $this->language->get('text_continuar'),$this->url->link('account/wishlist', '', true),$this->language->get('text_wishlist'),$this->url->link('checkout/cart'),$this->language->get('text_shopping'),$this->url->link('checkout/checkout', '', true),$this->language->get('text_checkout'));

        //------------------- ELEMENTOS DE LA BÚSQUEDA--------------------
        $url = '';

		if (isset($this->request->get['search'])) {
			$search = $this->request->get['search'];
            $url .= '&search=' . urlencode(html_entity_decode($search, ENT_QUOTES, 'UTF-8'));
		} else {
			$search = '';
		}


        if (isset($this->request->get['searchText'])) {
            $searchText = $this->request->get['searchText'];
            $url .= '&searchText=' . urlencode(html_entity_decode($searchText, ENT_QUOTES, 'UTF-8'));

        } else {
            $searchText = 'texto';
        }

        if (isset($this->request->get['searchCat'])) {
            $searchCat = $this->request->get['searchCat'];
            $url .= '&searchCat=' . urlencode(html_entity_decode($searchCat, ENT_QUOTES, 'UTF-8'));

        } else {
            $searchCat = 'ALL';
        }


        if (isset($this->request->get['language']) && ($this->request->get['language']!=='all')) {
            $language = $this->request->get['language'];
            $url .= '&language=' . urlencode(html_entity_decode($language, ENT_QUOTES, 'UTF-8'));

        } else {
            $language = 'ALL';
        }

        if (isset($this->request->get['initialRange'])) {
            $initialRange = $this->request->get['initialRange'];
            $url .= '&initialRange=' . urlencode(html_entity_decode($initialRange, ENT_QUOTES, 'UTF-8'));

        } else {
            $initialRange = 0;
        }

        if (isset($this->request->get['finalRange'])) {
            $finalRange = $this->request->get['finalRange'];
            $url .= '&finalRange=' . urlencode(html_entity_decode($finalRange, ENT_QUOTES, 'UTF-8'));

        } else {
            $finalRange = 2000;
        }
        if (isset($this->request->get['available']) && ($this->request->get['available']!=='all')) {
            $available = $this->request->get['available'];
            $url .= '&available=' . urlencode(html_entity_decode($available, ENT_QUOTES, 'UTF-8'));

        } else {
            $available = 'all';
        }

		if (isset($this->request->get['tag'])) {
			$tag = $this->request->get['tag'];
		} elseif (isset($this->request->get['search'])) {
			$tag = $this->request->get['search'];
		} else {
			$tag = '';
		}

		if (isset($this->request->get['description'])) {
			$description = $this->request->get['description'];
		} else {
			$description = '';
		}

		if (isset($this->request->get['category_id'])) {
			$category_id = $this->request->get['category_id'];
		} else {
			$category_id = 0;
		}


       // $typeOfSearch=$this->determinarBusqueda($search,$searchText);
       // $products=$this->mandarURL();


		if (isset($this->request->get['sub_category'])) {
			$sub_category = $this->request->get['sub_category'];
		} else {
			$sub_category = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		if (isset($this->request->get['search'])) {
			$this->document->setTitle($this->language->get('heading_title') .  ' - ' . $this->request->get['search']);
		} elseif (isset($this->request->get['tag'])) {
			$this->document->setTitle($this->language->get('heading_title') .  ' - ' . $this->language->get('heading_tag') . $this->request->get['tag']);
		} else {
			$this->document->setTitle($this->language->get('heading_title'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
        //------------------- DEFINICIÓN DE ELEMENTOS PARA SELECT BOXES DE FILTROS --------------------
		//$url = '';

        $searchTextOptions=[
            array(
                'searchText_id'  => 'texto',
                'searchText_name'  => $this->language->get('entry_searchText1')
            ),
            array(
                'searchText_id'  => 'autor',
                'searchText_name'  => $this->language->get('entry_searchText2')
            ),

        ];
        $data['searchTextOptions']=$searchTextOptions;

        $languageOptions=[
            array(
                'language_id'  => 'ALL',
                'language_name'  => $this->language->get('entry_lang1')
            ),
            array(
                'language_id'  => 'es',
                'language_name'  => $this->language->get('entry_lang2')
            ),
            array(
                'language_id'  => 'en',
                'language_name'  => $this->language->get('entry_lang3')
            )
        ];

        $data['languageOptions']=$languageOptions;

        $availableOptions = [
            array(
                'available_id'  => 'all',
                'available_name'  => $this->language->get('entry_disp1')
            ),
            array(
                'available_id'  => 'ava',
                'available_name'  => $this->language->get('entry_disp2')
            ),
            array(
                'available_id'  => 'pre',
                'available_name'  => $this->language->get('entry_disp3')
            )
        ];

        $data['availableOptions']=$availableOptions;

        $priceRangeOptions = [
            array(
                'priceRange_id'  => 'all',
                'priceRange_name'  => $this->language->get('entry_price1')
            ),
            array(
                'priceRange_id'  => '0to50',
                'priceRange_name'  => $this->language->get('entry_price2')
            ),
            array(
                'priceRange_id'  => '50to100',
                'priceRange_name'  => $this->language->get('entry_price3')
            ),
            array(
                'priceRange_id'  => '100+',
                'priceRange_name'  => $this->language->get('entry_price4')
            )
        ];

        $data['priceRangeOptions']=$priceRangeOptions;

		if (isset($this->request->get['search'])) {
		//	$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['tag'])) {
			$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['description'])) {
			$url .= '&description=' . $this->request->get['description'];
		}

		if (isset($this->request->get['category_id'])) {
			$url .= '&category_id=' . $this->request->get['category_id'];
		}

		if (isset($this->request->get['sub_category'])) {
			$url .= '&sub_category=' . $this->request->get['sub_category'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
/*
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}*/

		if (isset($this->request->get['limit'])) {
		//	$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/search', $url)
		);

		if (isset($this->request->get['search'])) {
			$data['heading_title'] = $this->language->get('heading_title') .  ' - ' . $this->request->get['search'];
		} else {
			$data['heading_title'] = $this->language->get('heading_title');
		}


        //------------------- TEXTOS PARA PÁGINA --------------------


		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_search'] = $this->language->get('text_search');
		$data['text_keyword'] = $this->language->get('text_keyword');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_sub_category'] = $this->language->get('text_sub_category');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');

		$data['entry_search'] = $this->language->get('entry_search');
		$data['entry_searchText'] = $this->language->get('entry_searchText');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_language'] = $this->language->get('entry_language');
		$data['entry_availability'] = $this->language->get('entry_availability');
		$data['entry_priceRange'] = $this->language->get('entry_priceRange');
		$data['entry_Category'] = $this->language->get('entry_Category');

		$data['button_search'] = $this->language->get('button_search');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');

		$data['compare'] = $this->url->link('product/compare');

		$this->load->model('catalog/category');
        //------------------- LISTA CATEGORÍAS PARA FILTROS--------------------
		// 3 Level Category Search
		$data['categories'] = array();

		$categories_1 = $this->model_catalog_category->getCategories(0);

		foreach ($categories_1 as $category_1) {
			$level_2_data = array();

			$categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);

			foreach ($categories_2 as $category_2) {
				$level_3_data = array();

				$categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

				foreach ($categories_3 as $category_3) {
					$level_3_data[] = array(
						'category_id' => $category_3['category_id'],
						'name'        => $category_3['name'],
					);
				}

				$level_2_data[] = array(
					'category_id' => $category_2['category_id'],
					'name'        => $category_2['name'],
					'children'    => $level_3_data
				);
			}

			$data['categories'][] = array(
				'category_id' => $category_1['category_id'],
				'name'        => $category_1['name'],
				'children'    => $level_2_data
			);
		}


        $data['products'] = array();
       // $priceRangeURL=$this->getPriceRanges($priceRange);
        $resultados=$this->sendURL(urlencode($search),$searchText,$searchCat,$language,$initialRange,$finalRange,$available, $limit, $page);

        if(isset($resultados))
        {
            //$resultados=$this->jsonPrueba();
            $product_total=$resultados['total'];
            foreach ($resultados['hits'] as $libro) {
                $imageBeforeFormat=$libro['_source']['coverImage'];
                $isbn=$libro['_source']['isbn'];
                $category=$libro['_source']['category'];
                $lang=$libro['_source']['language'];
                $priceBeforeFormat=$libro['_source']['sellingprice'];
                $author=$libro['_source']['author'];
                //$author= $author=$this->obtenerAutor($libro['_source']['author']);;
                $title=$libro['_source']['title'];
                $model=$libro['_source']['active_revision_id'];
                if ($imageBeforeFormat) {
                    $image = $this->model_tool_image->resize($imageBeforeFormat, $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($priceBeforeFormat, 0, $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                $this->load->model('catalog/productdata');
                $nombreReducido=$this->model_catalog_productdata->reducirNombre($title);
                $data['products'][] = array(
                    'product_id'  => $model,
                    'model'       => $model,
                    'thumb'       => $image,
                    'name'        => $title,
                    'nombreReducido' =>$nombreReducido,
                    'author'      => $author,
                    'price'       => $price,
                    'precioReal'  => $price,
                    'href'        => $this->url->link('product/product', 'model=' . $model),
                    //'resize'      => $resize
                );
            }
        }


            $data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/search', 'sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/search', 'sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/search', 'sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/search', 'sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/search', 'sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/search', 'sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/search', 'sort=rating&order=ASC' . $url)
				);
			}

        $data['limits'] = array();

        $limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

        sort($limits);

        foreach($limits as $value) {
            $data['limits'][] = array(
                'text'  => $value,
                'value' => $value,
                'href'  => $this->url->link('product/search', $url . '&limit=' . $value)
            );
        }

        //$product_total=10;
       // $limit=3;
        if(!isset($product_total))
        {
            $product_total=0;
        }
        $pagination = new Pagination();
        //	$pagination->total = $product_total;
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('product/search', $url . '&page={page}&limit='.$limit);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total);

        // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
        if ($page == 1) {
            $this->document->addLink($this->url->link('product/search', '', true), 'canonical');
        } elseif ($page == 2) {
            $this->document->addLink($this->url->link('product/search', '', true), 'prev');
        } else {
            $this->document->addLink($this->url->link('product/search', $url . '&page='. ($page - 1), true), 'prev');
        }

        if ($limit && ceil($product_total / $limit) > $page) {
            $this->document->addLink($this->url->link('product/search', $url . '&page='. ($page + 1), true), 'next');
        }


		$data['search'] = $search;
		$data['language'] = $language;
		//$data['priceRange'] = $priceRange;
        $data['initialRange'] = $initialRange;
        $data['finalRange'] = $finalRange;
		$data['searchText'] = $searchText;
		$data['available'] = $available;
        $data['searchCat'] = $searchCat;
		$data['description'] = $description;
		$data['category_id'] = $category_id;
		$data['sub_category'] = $sub_category;

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('product/search', $data));
	}

    public function determinarBusqueda($search, $searchText)
    {
        $tipoBusqueda=0;
        //verifica que la búsqueda sean solo numeros, 1 significa que son solo números
        $searchISBN=preg_match('/^[0-9]*$/', $search);

        if(strlen($search)==13 && $searchISBN)
        {
            $tipoBusqueda=1;
        }
        //ActiveRevisionID debe de ser XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXXX   (8-4-4-4-12)
        else if(preg_match('/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/',$search))
        {
            $tipoBusqueda=2;
        }
        else{
            if($searchText=='autor')
            {
                $tipoBusqueda=3;
            }

            elseif($searchText=='titulo')
            {
                $tipoBusqueda=4;
            }
        }

        return $tipoBusqueda;
            //isbn debe tener exactamente 13 números
    }


    public function sendURL($search = "",$searchText,$searchCat,$language,$initialRange,$finalRange,$available,$limit,$page)
    {
        $page=$page-1;
     //   $url = ELASTIC_URL;
        $ruta="";

        if ($searchText=="autor")
        {
            $ruta=ELASTIC_URL."/author/$search/$searchCat/$initialRange/$finalRange/$language/$limit/$page";
        }

        else if($searchText=="texto")
        {
            $ruta=ELASTIC_URL."/title/$search/$searchCat/$initialRange/$finalRange/$language/$limit/$page";
        }

        //se realiza la conexión cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ruta);
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);
        $res = json_decode($resultado, true);
        //    $res2 = json_decode($res, true);
        if(isset($res['hits'])){
            $libros=$res['hits'];
            return $libros;
        }
        else{
            return null;
        }

    }


 /*   public function sendURL($search,$searchText,$searchCat,$language,$priceRange,$available)
    {
        $fields_string='';
        $searchFields = array(
            'search' => urlencode($search),
            'searchText' => urlencode($searchText),
            'searchCat' => urlencode($searchCat),
            'language' => urlencode($language),
            'priceRange' => urlencode($priceRange),
            'available' => urlencode($available),
        );

        foreach($searchFields as $key=>$value)
        {
            $fields_string .= $key.'='.$value.'&';
        }

        $fields_string= rtrim($fields_string, '&');
        $fields_string = implode('&', $searchFields);

        $thisURL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
       // $thisURL = str_replace("%20","-",$thisURL);
        //para prueba
        //$thisURL="http://store.xooklocal.com/index.php?route=product/search&search=ha&searchText=texto&searchCat=ALL&language=all&priceRange=all&available=all";
        $urlJSON = json_encode($thisURL);
        $ruta = 'http://catalog.orbiletest.com/search/elastic/find';
        $cadenaCURL="";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $ruta);
        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        //Especificamos el post
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POST, 1);
        //LE DECIMOS QUE PARAMETROS ENVIAMOS
        curl_setopt($ch, CURLOPT_POSTFIELDS, $urlJSON);
        //para solucionar el problema de "https"
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec($ch);
        if(curl_exec($ch) === false)
        {
            echo "Error: ".curl_error($ch);
        }

        else {

           // $resultado = stripslashes($resultado);
            $res = json_decode($resultado, true);
        //    $res2 = json_decode($res, true);
            if(isset($res['hits'])){
                $libros=$res['hits'];
                return $libros;
            }
            else{
                return null;
            }

        }

    }*/


    public function jsonPrueba(){
        $string = file_get_contents(DIR_APPLICATION . 'controller/product/json/prueba.json');
        $jsonPruebaArray = json_encode($string, true);
        $libros=$jsonPruebaArray['hits']['hits'];

        return $libros;
    }

    public function getPriceRanges($priceRange){

        if (isset($priceRange))
        {
            switch($priceRange)
            {
                case 'all':
                {
                    $priceRangeArray = [
                        'initialRange' => 'ALL',
                        'finalRange' => 'ALL'
                    ];
                    break;
                }
                case '0to50':
                {
                    $priceRangeArray = [
                        'initialRange' => 0,
                        'finalRange' => 50
                    ];
                    break;
                }
                case '50to100':
                {
                    $priceRangeArray = [
                        'initialRange' => 50,
                        'finalRange' => 100
                    ];
                    break;
                }
                case '100+':
                {
                    $priceRangeArray = [
                        'initialRange' => 100,
                        'finalRange' => 'ALL'
                    ];
                    break;
                }

                default:
                {
                    $priceRangeArray = [
                        'initialRange' => 'ALL',
                        'finalRange' => 'ALL'
                    ];
                }


            }
            return $priceRangeArray;
        }

        else{
            return ($priceRangeArray = [
                'initialRange' => 'ALL',
                'finalRange' => 'ALL'
            ]);
        }



    }

    public function obtenerAutor($infoLibro)
    {
        $aut = '';
        if ($infoLibro) {
            //$aut = ' / ';
            // $what=isset($infoLibro['activeRevision']['contibutors']['contributor']['@value']);
            if (isset($infoLibro['contributor']['@value'])) {
                if($infoLibro['contributor']['@attributes']['type']=='Author' || $infoLibro['contributor']['@attributes']['type']=='Editor')
                {
                    $holder = $infoLibro['contributor']['@value'];
                }
                //$aut .= $infoLibro['contributor']['@value'];
            } else {
                $autores = $infoLibro['contributor'];
                $count = count($autores);
                $n = 0;
                foreach ($autores as $autor) {
                    if (!($n > 0)) {
                        if($autor['@attributes']['type']=='Author' || $autor['@attributes']['type']=='Editor')
                        {
                            $holder = $autor['@value'];
                            $n++;
                        }
                    }



                }

            }


        }
        if(isset($holder))
        {
            return $holder;
        }

        else{
            return NULL;
        }

    }

}
