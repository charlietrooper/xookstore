<?php
class ControllerPromoPromocion02 extends Controller
{
    public function index()
    {

        $this->load->language('promo/promocion01');
        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');

        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/promo/promocion01.css');
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/notification-popup/popup.css');
        $this->load->language('common/popup');
        $data['popup_code'] = sprintf($this->language->get('popup_code'), $this->language->get('text_continuar'),$this->url->link('account/wishlist', '', true),$this->language->get('text_wishlist'),$this->url->link('checkout/cart'),$this->language->get('text_shopping'),$this->url->link('checkout/checkout', '', true),$this->language->get('text_checkout'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Promociones',
            'href' => $this->url->link('seccion/promociones')
        );

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('promo/promocion01', $data));


    }


}

?>