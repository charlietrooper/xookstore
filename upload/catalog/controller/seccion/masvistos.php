<?php
class ControllerSeccionMasvistos extends Controller {
    public function index() {
        $this->load->language('seccion/masvistos');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/seccion/top50.css');
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/notification-popup/popup.css');
        $this->load->language('common/popup');
        $data['popup_code'] = sprintf($this->language->get('popup_code'), $this->language->get('text_continuar'),$this->url->link('account/wishlist', '', true),$this->language->get('text_wishlist'),$this->url->link('checkout/cart'),$this->language->get('text_shopping'),$this->url->link('checkout/checkout', '', true),$this->language->get('text_checkout'));

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('seccion/masvistos')
        );

        $this->load->model('catalog/views');
        $this->load->model('tool/image');
        $masvistosLimit=20;

        $filter_data = array(
            'start' => ($page - 1) * $masvistosLimit,
            'limit' => $masvistosLimit
        );

        $data['products'] = array();

        $product_viewed_total = $this->model_catalog_views->getTotalProductViews();

        $product_total = $this->model_catalog_views->getTotalProductsViewed();

        if($product_total>50)
        {
            $limitePaginacion=50;
        }
        else{
            $limitePaginacion=$product_total;
        }

        $results = $this->model_catalog_views->getProductsViewed($filter_data);

        if ($results) {
            foreach ($results as $result) {

                if ($result['viewed']) {
                    $percent = round($result['viewed'] / $product_viewed_total * 100, 2);
                } else {
                    $percent = 0;
                }

                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }


                $this->load->model('catalog/productdata');
                $nombreReducido=$this->model_catalog_productdata->reducirNombre($result['name']);
                $precioReal=$this->model_catalog_productdata->obtenerPrecioReal($price, $special);

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'viewed'  => $result['viewed'],
                    'name' => $result['name'],
                    'nombreReducido' =>$nombreReducido,
                    'author'      =>$result['author'],
                    'price' => $price,
                    'special' => $special,
                    'precioReal'  => $precioReal,
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                    'percent' => $percent . '%'
                );
            }
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_viewed'] = $this->language->get('column_viewed');
        $data['column_percent'] = $this->language->get('column_percent');

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } elseif (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $url = '';
        $pagination = new Pagination();
        $pagination->total = $limitePaginacion;
        $pagination->page = $page;
        $pagination->limit = $masvistosLimit;
        $pagination->url = $this->url->link('seccion/masvistos', $url . '&page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($limitePaginacion) ? (($page - 1) * $masvistosLimit) + 1 : 0, ((($page - 1) * $masvistosLimit) > ($limitePaginacion - $masvistosLimit)) ? $limitePaginacion : ((($page - 1) * $masvistosLimit) + $masvistosLimit), $limitePaginacion);

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('seccion/masvistos', $data));
    }

}