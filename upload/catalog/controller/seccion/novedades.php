<?php
class ControllerSeccionNovedades extends Controller
{
    public function index()
    {

        $this->load->language('seccion/novedades');
        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/seccion/novedades.css');
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/notification-popup/popup.css');
        $this->load->language('common/popup');
        $data['popup_code'] = sprintf($this->language->get('popup_code'), $this->language->get('text_continuar'),$this->url->link('account/wishlist', '', true),$this->language->get('text_wishlist'),$this->url->link('checkout/cart'),$this->language->get('text_shopping'),$this->url->link('checkout/checkout', '', true),$this->language->get('text_checkout'));

        /*        if (isset($this->request->get['filter'])) {
                    $filter = $this->request->get['filter'];
                } else {
                    $filter = '';
                }

                if (isset($this->request->get['sort'])) {
                    $sort = $this->request->get['sort'];
                } else {
                    $sort = 'p.sort_order';
                }

                if (isset($this->request->get['order'])) {
                    $order = $this->request->get['order'];
                } else {
                    $order = 'ASC';
                }*/

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            // $limit = (int)$this->request->get['limit'];
            $limit=20;
        } else {
            // $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
            $limit=20;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('seccion/novedades')
        );






        /*----------------------------------------------------------NOVEDADES------------------------------------------------------*/
        $data['products'] = array();
        $start = ($page - 1) * $limit;
        $length = $limit;
        $totalABuscar=50;

        $filter_data = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => $start,
            'limit' => $totalABuscar
        );

        $queryNovedades = $this->model_catalog_product->getLatest($filter_data, $length);
        $results=$queryNovedades[0];
        $totalCount=$queryNovedades[1];

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }
                $this->load->model('catalog/productdata');
                $nombreReducido=$this->model_catalog_productdata->reducirNombre($result['name']);
                $precioReal=$this->model_catalog_productdata->obtenerPrecioReal($price, $special);

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'nombreReducido' =>$nombreReducido,
                    'author'      =>$result['author'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price' => $price,
                    'special' => $special,
                    'precioReal'  => $precioReal,
                    'tax' => $tax,
                    'rating' => $rating,
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                    // 'resize' => $resize
                );
            }

            $url = '';
            $product_total = $totalCount;
            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('seccion/novedades', $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total);

            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
            if ($page == 1) {
                $this->document->addLink($this->url->link('seccion/novedades', true), 'canonical');
            } elseif ($page == 2) {
                $this->document->addLink($this->url->link('seccion/novedades', true), 'prev');
            } else {
                $this->document->addLink($this->url->link('seccion/novedades', '&page=' . ($page - 1), true), 'prev');
            }

            if ($limit && ceil($product_total / $limit) > $page) {
                $this->document->addLink($this->url->link('seccion/novedades', '&page=' . ($page + 1), true), 'next');
            }

           // $data['sort'] = $sort;
           // $data['order'] = $order;
            $data['limit'] = $limit;
            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['shopping_cart'] = $this->url->link('checkout/cart');
            $data['checkout'] = $this->url->link('checkout/checkout', '', true);

            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('seccion/novedades', $data));
        }

    }


}

?>