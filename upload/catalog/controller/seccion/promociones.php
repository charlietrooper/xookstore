<?php
class ControllerSeccionPromociones extends Controller
{
    public function index()
    {
        $this->load->language('seccion/promociones');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');
        $this->load->model('design/banner');

        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/seccion/promociones.css');
        $this->document->addStyle('catalog/view/theme/temaOrbile01/stylesheet/plugins/smoothzoom.css');
        $this->load->model('catalog/information');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['information_id'])) {
            $information_id = (int)$this->request->get['information_id'];
        } else {
            $information_id = 0;
        }
        //El bannerID de Banner Promocionales es 9
        $bannerID=9;
        $imagenes = $this->model_design_banner->getBanner($bannerID);

        if ($imagenes) {
            $data['promotionsQuantity']=count($imagenes);
            $data['heading_title'] = $this->language->get('heading_title');
            $data['continue'] = $this->url->link('common/home');

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('seccion/promociones')
            );

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $width=800;
            $height=320;

            foreach ($imagenes as $imagen) {
                if (is_file(DIR_IMAGE . $imagen['image'])) {
                    $data['banners'][] = array(
                        'title' => $imagen['title'],
                        'link'  => $imagen['link'],
                        'image' => $this->model_tool_image->resize($imagen['image'], $width, $height)
                    );
                }
            }

            $this->response->setOutput($this->load->view('seccion/promociones', $data));
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('seccion/promociones')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }
}
?>