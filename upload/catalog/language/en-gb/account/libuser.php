<?php
// Heading
$_['heading_title']     = 'User Library';

// Text
$_['text_account']      = 'Account';
$_['text_libuser']    = 'User library';
$_['text_empty']        = 'You have not made any previous orders!';

// Column
$_['column_order_id']   = 'Order ID';
$_['column_name']       = 'Name';
$_['column_size']       = 'Size';
$_['column_date_added'] = 'Date Added';