<?php
// Heading
$_['heading_title']     = 'Search';
$_['heading_tag']		= 'Tag - ';

// Text
$_['text_search']       = 'Products meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';
$_['text_sub_category'] = 'Search in subcategories';
$_['text_empty']        = 'There is no product that matches the search criteria.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'Ex Tax:';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (Low &gt; High)';
$_['text_price_desc']   = 'Price (High &gt; Low)';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';

// Entry
$_['entry_search']      = 'Search Criteria';
$_['entry_searchText'] = 'Search by';
$_['entry_description'] = 'Search in product descriptions';
$_['entry_language'] = 'Language';
$_['entry_availability'] = 'Availability';
$_['entry_priceRange'] = 'Price range';
$_['entry_Category'] = 'Category';
$_['entry_searchText1'] = 'Text';
$_['entry_searchText2'] = 'Author';
$_['entry_lang1'] = 'All';
$_['entry_lang2'] = 'Spanish';
$_['entry_lang3'] = 'English';
$_['entry_disp1'] = 'All';
$_['entry_disp2'] = 'Available';
$_['entry_disp3'] = 'Preorder';
$_['entry_price1'] = 'All';
$_['entry_price2'] = 'Less than $50';
$_['entry_price3'] = '$50 - $100';
$_['entry_price4'] = '$100 +';