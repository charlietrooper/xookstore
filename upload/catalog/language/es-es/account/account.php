<?php
// Heading
$_['heading_title']      = 'Mi cuenta';

// Text
$_['text_account']       = 'Cuenta';
$_['text_my_account']    = 'Mi cuenta';
$_['text_my_orders']     = 'Mis compras';
$_['text_my_newsletter'] = 'Bolet&iacute;n';
$_['text_edit']          = 'Modificar informaci&oacute;n';
$_['text_password']      = 'Cambia tu contrase&ntilde;a';
$_['text_address']       = 'Modificar direcci&oacute;n';
$_['text_wishlist']      = 'Modifica tu Lista de deseos';
$_['text_order']         = 'Historial de pedidos';
$_['text_download']      = 'Descargas';
$_['text_reward']        = 'Tus puntos';
$_['text_return']        = 'Ver sus Peticiones de devoluci&oacute;n';
$_['text_transaction']   = 'Tus transacciones';
$_['text_newsletter']    = 'Subscribirse / desubscribirse del bolet&iacute;n';
$_['text_recurring']     = 'Pagos recurrentes';
$_['text_transactions']  = 'Transactiones';
$_['text_libreria']  = 'Mi biblioteca';
$_['text_aviso']  = 'Aviso';
$_['text_bienvenida']  = 'Bienvenido, ';
