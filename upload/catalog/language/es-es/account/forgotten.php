<?php
// Heading
$_['heading_title']   = '¿Olvid&oacute; su contrase&ntilde;a?';

// Text
$_['text_account']    = 'Cuenta';
$_['text_forgotten']  = 'Olvid&oacute; su contrase&ntilde;a';
$_['text_your_email'] = 'Su correo';
$_['text_email']      = 'Escriba la direcci&oacute;n de correo electr&oacute;nico asociada a su cuenta de Orbile. Le enviaremos las instrucciones para recuperar su contrase&ntilde;a.';
$_['text_success']    = 'Un correo con instrucciones para restablecer su ontrase&ntilde;a ha sido enviado al correo proporcionado.';

// Entry
$_['entry_email']     = 'Correo';

// Entry
$_['btn_password']     = 'Recuperar contrase&ntilde;a';

// Error
$_['error_email']     = 'Advertencia: No tenemos registrada ninguna cuenta con la direcci&oacute;n de correo proporcionada, por favor, int&eacute;ntelo de nuevo.';