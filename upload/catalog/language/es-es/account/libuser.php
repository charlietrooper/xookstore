<?php
// Heading
$_['heading_title']     = 'Librer&iacute;a del usuario';

// Text
$_['text_account']      = 'Cuenta';
$_['text_libuser']    = 'Librería del Usuario';
$_['text_empty']        = 'No ha hecho ning&uacute;n pedido anteriormente!';

// Column
$_['column_order_id']   = 'Pedido ID';
$_['column_name']       = 'Nombre';
$_['column_size']       = 'Peso';
$_['column_date_added'] = 'A&ntilde;adido el';