<?php
// Heading
$_['heading_title']                = 'Inicio de Sesi&oacute;n';

// Text
$_['text_account']                 = 'Cuenta';
$_['text_login']                   = 'Iniciar sesi&oacute;n';
$_['text_new_customer']            = 'Nuevo cliente';
$_['text_register']                = '&Uacute;nete a Orbile';
$_['text_register_account']        = 'Al crear una cuenta podr&aacute; realizar sus descargas gratuitas o compras rapidamente, revisar los estado \' s  de sus pedido \' s , y realizar un seguimiento de sus operaciones anteriores. Estar al d&iacute;a de las &uacute;ltimas actualizaciones. Recuerde introducir sus datos de contacto correctamente, para poder dirigirnos a usted de la forma m&aacute;s apropiada, gracias por su confianza en nuestros servicios. Las cuentas con los datos incompletos ser&aacute;n bloqueadas.';
$_['text_returning_customer']      = 'Iniciar sesi&oacute;n en Orbile';
$_['text_i_am_returning_customer'] = 'Yo soy cliente';
$_['text_forgotten']               = '¿Olvidaste tu contrase&ntilde;a?';
$_['button_registro']               = 'Reg&iacute;trate en Orbile';


// Entry
$_['entry_email']                  = 'Correo';
$_['entry_password']               = 'Contrase&ntilde;a';

// Error
$_['error_login']                  = 'Advertencia: No hay resultados para la direcci&oacute;n de correo electr&oacute;nico y / o contrase&ntilde;a.';
$_['error_approved']               = 'Advertencia: Tu cuenta requiere aprobaci&oacute;n antes de que pueda iniciar sesi&oacute;n.';