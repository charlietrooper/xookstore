<?php
// Heading
$_['heading_title'] = 'Salir de su cuenta';

// Text
$_['text_shortSuccess']  = 'Usted ha cerrado sesi&oacute;n con &eacute;xito';
$_['text_message']  = '';
$_['text_account']  = 'Cuenta';
$_['text_logout']   = 'Salir';