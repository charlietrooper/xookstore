<?php
// header
$_['heading_title']  = 'Restablecer contrase&ntilde;a';

// Text
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Escriba una nueva contrase&ntilde;a:';
$_['text_success']   = 'Su contrase&ntilde;a ha sido actualizada.';

// Entry
$_['entry_password'] = 'Contrase&ntilde;a';
$_['entry_confirm']  = 'Confirmar contrase&ntilde;a';

// Error
$_['error_password'] = 'La contrase&ntilde;a debe tener entre 4 y 20 caracteres.';
$_['error_confirm']  = 'La confirmaci&oacute;n de contrase&ntilde;a no concide';
$_['error_code']     = 'El c&oacute;digo para restablecer su contras&ntilde;a es inv&aacute;lido o ya ha sido usado anteriormente';