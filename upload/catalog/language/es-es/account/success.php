<?php
// Heading
$_['heading_title'] = '¡Tu cuenta ha sido creada con &eacute;xito!';

// Text
$_['text_shortSuccess']  = '¡Felicidades! Tu nueva cuenta Orbile ha sido creada con &eacute;xito.';

$_['text_message']  = '<p> Si tiene dudas acerca de alguna transacci&oacute;n, por favor p&oacute;ngase en contacto con nosotros.</p> <p> Una confirmaci&oacute;n ha sido enviada a la direcci&oacute;n de correo electr&oacute;nico proporcionada. Si usted no lo ha recibido en menos de una hora, <a href="%s"> cont&aacute;ctenos </a>. </p>';
$_['text_approval'] = '<p>Gracias por registrarse en % s! </p> <p> Se le notificar&aacute; por correo electr&oacute;nico una vez que su cuenta ha sido activada por el due&ntilde;o de la tienda. </p> <p> Si usted tiene alguna pregunta acerca del funcionamiento de esta tienda en l&iacute;nea, por favor <a href="%s"> en contacto con el propietario de la tienda </a>.</p>';

$_['text_account']  = 'Cuenta';
$_['text_success']  = '&Eacute;xito';
