<?php
// Text
$_['text_information']  = 'Sobre Orbile';
$_['text_service']      = 'Servicio al Cliente';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contacto';
$_['text_return']       = 'Devoluciones';
$_['text_sitemap']      = 'Mapa del sitio';
$_['text_manufacturer'] = 'Marca';
$_['text_voucher']      = 'Vales Regalo';
$_['text_affiliate']    = 'Afiliados';
$_['text_special']      = 'Ofertas';
$_['text_account']      = 'Mi Cuenta';
$_['text_order']        = 'Historial de Pedidos';
$_['text_wishlist']     = 'Lista de Deseos';
$_['text_newsletter']   = 'Bolet&iacute;n Noticias';
$_['text_social']        = 'Redes sociales';
$_['text_powered']      = 'Creado con Opencart';