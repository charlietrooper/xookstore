<?php
// Text
$_['text_home']          = 'Inicio';
$_['text_wishlist']      = 'Lista de deseo (%s)';
$_['text_shopping_cart'] = 'Carrito de compras';
$_['text_category']      = 'Categorias';
$_['text_account']       = 'Mi Cuenta';
$_['text_register']      = 'Registro';
$_['text_login']         = 'Iniciar sesi&oacute;n';
$_['text_order']         = 'Historial de pedidos';
$_['text_transaction']   = 'Transacciones';
$_['text_download']      = 'Descargas';
$_['text_libuser']      = 'Mi biblioteca';
$_['text_logout']        = 'Salir';
$_['text_checkout']      = 'Caja';
$_['text_search']        = 'Buscar';
$_['text_all']           = 'Ver todas las categor&iacute;as';
$_['text_seccion_Nov']           = 'Novedades';
$_['text_seccion_Top']           = 'Top 50';
$_['text_seccion_Promo']           = 'Promociones';
$_['text_seccion_Vis']           = 'M&aacute;s vistos';
$_['text_seccion_Pre']           = 'Preventas';
//$_['text_all']           = 'Ver Todo';