<?php
// Text
$_['text_wishlist'] = 'Lista de deseos';
$_['text_continuar'] = 'Continuar';
$_['text_shopping'] = 'Ver carrito';
$_['text_checkout'] = 'Caja';
$_['popup_code'] = ' <div id="notification-popup" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4></h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> %s <i class="fa fa-check"></i></button>
                    <a href="%s" class="btn btn-wishlist"> %s <i class="fa fa-heart"></i></a>
                    <a href="%s" class="btn btn-warning"> %s <i class="fa fa-shopping-cart"></i></a>
                    <a href="%s" class="btn btn-success"> %s <i class="fa fa-credit-card"></i></a>
                </div>
            </div>
        </div>
    </div>';
