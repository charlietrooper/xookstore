<?php
// Heading
$_['heading_title']  = 'Contacto';

// Text
$_['text_location']  = 'Nuestra dirección';
$_['text_store']     = 'Nuetras Tiendas';
$_['text_contact']   = 'Cont&aacute;ctanos';
$_['text_address']   = 'Direcci&oacute;n';
$_['text_telephone'] = 'Tel&eacute;fono';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Horario';
$_['text_comment']   = 'Comentario';
$_['text_shortSuccess']   = 'Su mensaje ha sido enviado con &eacute;xito ';
$_['text_success']   = '';

// Entry
$_['entry_name']     = 'Su Nombre';
$_['entry_email']    = 'Correo';
$_['entry_enquiry']  = 'Comentario';
$_['entry_captcha']  = 'Introduzca el C&oacute;digo de la caja de abajo';

// Email
$_['email_subject']  = 'Comentario';

// Errors
$_['error_name']     = 'El nombre debe tener entre 3 y 32 caracteres!';
$_['error_email']    = 'E-Mail no parece ser v&aacute;lido!';
$_['error_enquiry']  = 'Solicitud debe estar entre 10 y 3000 caracteres!';
$_['error_captcha']  = 'C&oacute;digo de verificaci&oacute;n no coincide con la imagen!';
