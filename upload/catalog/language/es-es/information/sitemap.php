<?php
// Heading
$_['heading_title']    = 'Mapa de sitio';

// Text
$_['text_special']     = 'Ofertas Especiales';
$_['text_promo']         = 'Promociones';
$_['text_top50']         = 'Top 50';
$_['text_novedades']         = 'Novedades';
$_['text_masvistos']         = 'M&aacute;s vistos';
$_['text_account']     = 'Mi Cuenta';
$_['text_edit']        = 'Informaci&oacute;n de la Cuenta';
$_['text_password']    = 'Contrase&ntilde;a';
$_['text_address']     = 'Libro de Direcciones';
$_['text_history']     = 'Historial de Pedidos';
$_['text_libuser']     = 'Librer&iacute;a de usuario';
$_['text_download']    = 'Descargas';
$_['text_cart']        = 'Carrito de Compras';
$_['text_checkout']    = 'Caja';
$_['text_search']      = 'Buscar';
$_['text_information'] = 'Informaci&oacute;n';
$_['text_contact']     = 'Contacto';