<?php
// Text
$_['text_subject']  = '%s - Nueva Contrase&ntilde;a';
$_['text_greeting'] = 'Usted ha solicitado restablecer su contrase&ntilde;a de %s.';
$_['text_password'] = 'La nueva contrase&ntilde;a es:';
$_['text_change']   = 'Para restablecer su contrase&ntilde;a, por favor, de click en el siguiente enlace:';
