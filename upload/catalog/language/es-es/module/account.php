<?php
// Heading
$_['heading_title']    = 'Cuenta';

// Text
$_['text_register']    = 'Registro';
$_['text_login']       = 'Iniciar sesi&oacute;n';
$_['text_logout']      = 'Salir';
$_['text_forgotten']   = 'Contrase&ntilde;a olvidada';
$_['text_account']     = 'Mi cuenta';
$_['text_edit']        = 'Editar cuenta';
$_['text_password']    = 'Contrase&ntilde;a';
$_['text_address']     = 'Lista de direcciones';
$_['text_wishlist']    = 'Lista de deseos';
$_['text_order']       = 'Historial de pedidos';
$_['text_download']    = 'Descargas';
$_['text_reward']      = 'Puntos de recompensa';
$_['text_return']      = 'Devoluciones';
$_['text_transaction'] = 'Transaciones';
$_['text_newsletter']  = 'Bolet&iacute;n de noticias';
$_['text_recurring']   = 'Pagos recurrentes';