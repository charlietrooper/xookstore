<?php
// Heading
$_['heading_title']    = 'Afiliado';

// Text
$_['text_register']    = 'Registro';
$_['text_login']       = 'Iniciar sesi&oacute;n';
$_['text_logout']      = 'Salir';
$_['text_forgotten']   = 'Contrase&ntilde;a olvidada';
$_['text_account']     = 'Mi cuenta';
$_['text_edit']        = 'Editar cuenta';
$_['text_password']    = 'Contrase&ntilde;a';
$_['text_payment']     = 'Opciones de pago';
$_['text_tracking']    = 'Programa de afiliados';
$_['text_transaction'] = 'Transacciones';
