<?php
// Heading
$_['heading_title']     = 'Resultados de b&uacute;squeda para';
$_['heading_tag']		= 'Etiqueta - ';

// Text
$_['text_search']       = 'Productos que satisfacen los criterios de b&uacute;squeda ';
$_['text_keyword']      = 'Palabras Clave';
$_['text_category']     = 'Todas las categor&iacute;as';
$_['text_sub_category'] = 'Buscar en las Subcategor&iacute;as';
$_['text_empty']        = 'No hay productos que corresponden con los criterios de b&uacute;squeda.';
$_['text_quantity']     = 'Cantidad:';
$_['text_manufacturer'] = 'Marca';
$_['text_model']        = 'C&oacute;digo de Producto:';
$_['text_points']       = 'Puntos Recompensa:';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin Iva:';
$_['text_reviews']      = 'Basado en %s Opiniones.';
$_['text_compare']      = 'Comprar Producto (%s)';
$_['text_sort']         = 'Ordenar por:';
$_['text_default']      = 'Defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Bajo &gt; Alto)';
$_['text_price_desc']   = 'Precio (Alto &gt; Bajo)';
$_['text_rating_asc']   = 'Clasificar (M&aacute;s Bajo)';
$_['text_rating_desc']  = 'Clasificar (M&aacute;s Alto)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Ver:';

// Entry
$_['entry_search']      = 'Palabras clave';
$_['entry_searchText'] = 'Buscar por';
$_['entry_description'] = 'Buscar Productos en las descripciones';
$_['entry_language'] = 'Idioma';
$_['entry_availability'] = 'Disponibilidad';
$_['entry_priceRange'] = 'Rango de precio';
$_['entry_Category'] = 'Categor&iacute;a';
$_['entry_searchText1'] = 'Texto';
$_['entry_searchText2'] = 'Autor';
$_['entry_lang1'] = 'Todos';
$_['entry_lang2'] = 'Espa&ntilde;ol';
$_['entry_lang3'] = 'Ingl&eacute;s';
$_['entry_disp1'] = 'Todos';
$_['entry_disp2'] = 'Disponible';
$_['entry_disp3'] = 'Preventa';
$_['entry_price1'] = 'Todos';
$_['entry_price2'] = 'Menos de $50';
$_['entry_price3'] = '$50 - $100';
$_['entry_price4'] = '$100 +';