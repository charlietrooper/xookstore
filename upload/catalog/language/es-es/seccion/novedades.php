<?php
// Heading
$_['heading_title']  = 'Novedades';

$_['text_sort']         = 'Ordenar Por:';
$_['text_default']      = 'Defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Bajo &gt; Alto)';
$_['text_price_desc']   = 'Precio (Alto &gt; Bajo)';
$_['text_rating_asc']   = 'Clasificaci&oacute;n (M&aacute;s Bajo)';
$_['text_rating_desc']  = 'Clasificaci&oacute;n (M&aacute;s Alto)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_views']         = 'M&aacute;s vistos';
?>