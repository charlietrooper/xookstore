<?php
class ModelAccountLibuser extends Model {

    public function autenticacion($customer_id, $email) {


        //Se obtienen los datos del config(url del API, usuario y cliente "orbile")
        $url_api = $this->config->get('orbile_api_base_url');
        $api_user = $this->config->get('x-api-user');
        $api_key = $this->config->get('x-api-key');


        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url_api/authservice/authenticate_or_register/$email/$customer_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_user\r\nx-api-key:$api_key"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);

        //Código http en la respuesta
        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            //Si el código de error es mayor a 400, se devuelve el error obtenido
            $error = $httpcode['http_code'].": ".$this->http_status_code_string('400'). "=>" .$resultado;
            curl_close($ch);
            $error_aut = array(
                'error_autentication'   => true,
                'error'                 => $error
            );
            return $error_aut;
            $logger->write('SE devuelve: ' . json_encode($error_aut));
        } elseif($resultado == false) {
            //Si la conexión es false, devuelve el curl_error
            $error_aut = array(
                'error_autentication'   => true,
                'error'                 => curl_error($ch)
            );
            return $error_aut;
            $logger->write('SE devuelve: ' . json_encode($error_aut));
        } else {
            //Se decodifica el JSON que envió la respuesta de la API
            $result = json_decode($resultado, true);

            //Se guarda el kobo_id en sessión
            $this->session->data['kobo_id'] = $result['KoboUserId'];

            //Se cierra la conexión cURL
            curl_close($ch);

            //se devuelve lo obtenido
            return $result;
            $logger->write('SE devuelve: ' . json_encode($result));
        }
    }

    public function libreria($user_id) {
        $url_api = $this->config->get('orbile_api_base_url');
        $api_user = $this->config->get('x-api-user');
        $api_key = $this->config->get('x-api-key');

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url_api/library_service/get_library/100/1/$user_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_user\r\nx-api-key:$api_key"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);

        //CODIGO DE ERROR MAYOR A 400
        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            $error = $httpcode['http_code'].": ".$this->http_status_code_string('400'). "=>" .$resultado;
            curl_close($ch);
            $respuesta = array(
                'error_purchase'    => true,
                'error'             => $error
            );
        } else{
            curl_close($ch);
            $respuesta = json_decode($resultado);
        }
        return $respuesta;
    }

    public function downloadbook($product_id,$user_id) {
        $url_api = $this->config->get('orbile_api_base_url');
        $api_user = $this->config->get('x-api-user');
        $api_key = $this->config->get('x-api-key');

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
        //Enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url_api/library_service/get_download_urls/$product_id/$user_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_user\r\nx-api-key:$api_key"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);

        //CODIGO DE ERROR MAYOR A 400
        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            $error = $httpcode['http_code'].": ".$this->http_status_code_string('400'). "=>" .$resultado;
            curl_close($ch);
            $respuesta = array(
                'error_purchase'    => true,
                'error'             => $error
            );
        } else{
            curl_close($ch);
            $respuesta = json_decode($resultado);
        }
        return $respuesta;
    }


    public function http_status_code_string($code, $include_code = false)
    {
        // Source: http://en.wikipedia.org/wiki/List_of_HTTP_status_codes

        switch($code)
        {
            // 1xx Informational
            case 100: $string = 'Continue'; break;
            case 101: $string = 'Switching Protocols'; break;
            case 102: $string = 'Processing'; break; // WebDAV
            case 122: $string = 'Request-URI too long'; break; // Microsoft

            // 2xx Success
            case 200: $string = 'OK'; break;
            case 201: $string = 'Created'; break;
            case 202: $string = 'Accepted'; break;
            case 203: $string = 'Non-Authoritative Information'; break; // HTTP/1.1
            case 204: $string = 'No Content'; break;
            case 205: $string = 'Reset Content'; break;
            case 206: $string = 'Partial Content'; break;
            case 207: $string = 'Multi-Status'; break; // WebDAV

            // 3xx Redirection
            case 300: $string = 'Multiple Choices'; break;
            case 301: $string = 'Moved Permanently'; break;
            case 302: $string = 'Found'; break;
            case 303: $string = 'See Other'; break; //HTTP/1.1
            case 304: $string = 'Not Modified'; break;
            case 305: $string = 'Use Proxy'; break; // HTTP/1.1
            case 306: $string = 'Switch Proxy'; break; // Depreciated
            case 307: $string = 'Temporary Redirect'; break; // HTTP/1.1

            // 4xx Client Error
            case 400: $string = 'Bad Request'; break;
            case 401: $string = 'Unauthorized'; break;
            case 402: $string = 'Payment Required'; break;
            case 403: $string = 'Forbidden'; break;
            case 404: $string = 'Not Found'; break;
            case 405: $string = 'Method Not Allowed'; break;
            case 406: $string = 'Not Acceptable'; break;
            case 407: $string = 'Proxy Authentication Required'; break;
            case 408: $string = 'Request Timeout'; break;
            case 409: $string = 'Conflict'; break;
            case 410: $string = 'Gone'; break;
            case 411: $string = 'Length Required'; break;
            case 412: $string = 'Precondition Failed'; break;
            case 413: $string = 'Request Entity Too Large'; break;
            case 414: $string = 'Request-URI Too Long'; break;
            case 415: $string = 'Unsupported Media Type'; break;
            case 416: $string = 'Requested Range Not Satisfiable'; break;
            case 417: $string = 'Expectation Failed'; break;
            case 422: $string = 'Unprocessable Entity'; break; // WebDAV
            case 423: $string = 'Locked'; break; // WebDAV
            case 424: $string = 'Failed Dependency'; break; // WebDAV
            case 425: $string = 'Unordered Collection'; break; // WebDAV
            case 426: $string = 'Upgrade Required'; break;
            case 449: $string = 'Retry With'; break; // Microsoft
            case 450: $string = 'Blocked'; break; // Microsoft

            // 5xx Server Error
            case 500: $string = 'Internal Server Error'; break;
            case 501: $string = 'Not Implemented'; break;
            case 502: $string = 'Bad Gateway'; break;
            case 503: $string = 'Service Unavailable'; break;
            case 504: $string = 'Gateway Timeout'; break;
            case 505: $string = 'HTTP Version Not Supported'; break;
            case 506: $string = 'Variant Also Negotiates'; break;
            case 507: $string = 'Insufficient Storage'; break; // WebDAV
            case 509: $string = 'Bandwidth Limit Exceeded'; break; // Apache
            case 510: $string = 'Not Extended'; break;

            // Unknown code:
            default: $string = 'Unknown';  break;
        }
        if($include_code)
            return $code . ' '.$string;
        return $string;
    }


}