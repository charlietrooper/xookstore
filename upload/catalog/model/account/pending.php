<?php
class ModelAccountPending extends Model {
    public function cancelPendingOrders($customerID)
    {
        //obtenemos todas las órdenes con Status de Pendiente (order_status_id=1)
        $order_query = $this->db->query("SELECT order_id, firstname, payment_code, order_status_id, date_added FROM `" . DB_PREFIX . "order` WHERE order_status_id = 1 AND customer_id = '" . (int)$customerID . "'");
        $arrExpired=array();
        if (count($order_query->rows)>0)
        {
            //recorremos las órdenes
            foreach($order_query->rows as $order)
            {
                //la orden pendiente puede haberse generado al usar Openpay Stores u Openpay Banks
                //De acuerdo a la correspondiente, sacamos $deadline de la configuración que tenemos en el admin de ambos métodos
               if($order['payment_code']=='openpay_stores') {
                   $deadline = $this->config->get('openpay_deadline');
               }

               else if($order['payment_code']=='openpay_banks') {
                   $deadline = $this->config->get('openpay_bank_deadline');
               }
               else {
                   $deadline=null;
               }
                //¿Cuándo se generó este pedido?
               $dateCreated=$order['date_added'];
               $dateToday = date('Y-m-d\TH:i:s');

               if($deadline > 0){
                   //Al tiempo que se generó la orden, le sumamamos las horas que establecimos como DEADLINE
                   $due_datePendingNormal = date('Y-m-d H:i:s',strtotime('+' . $deadline . ' hour',strtotime($dateCreated)));
                   $due_datePending=strtotime('+' . $deadline . ' hour',strtotime($dateCreated));

                   //Aquí comparamos para saber si el día de hoy ya pasó el datePending
                   if(strtotime($dateToday)>$due_datePending)
                   {
                       //Si ya expiró, entonces metemos el id de orden a un arreglo. Al final del foreach, vamos a mandar
                       //Un query con este array que actualize la base de datos de PENDING a CANCELADO
                       array_push($arrExpired,$order['order_id']);
                   }


               }else{
                   //$due_datePending = date('Y-m-d H:i:s',strtotime('+720 hour',strtotime($dateCreated)));
                   $due_datePending = strtotime('+720 hour',strtotime($dateCreated));
                   if(strtotime($dateToday)>$due_datePending)
                   {
                       array_push($arrExpired,$order['order_id']);
                   }
               }

            }

            if(!empty($arrExpired))
            {
                if($this->cancelarOrdenes($arrExpired))
                {
                    return $arrExpired;
                }
                else{
                    return false;
                }
            }

        }

    }

    public function cancelarOrdenes($arrExpired)
    {
        $arrExpQuery = join(", ", $arrExpired);
        $update=$this->db->query("UPDATE " . DB_PREFIX . "order SET order_status_id = 7 WHERE order_id IN ($arrExpQuery)");
        //$update=$this->db->query("SELECT order_id, firstname, order_status_id, date_added FROM " . DB_PREFIX . "order WHERE order_id IN ($arrExpQuery)");
        foreach($arrExpQuery as $orderID)
        {
            $this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$orderID . "', order_status_id = 7, notify = 0, comment = 'Expir&oacute; periodo de pago', date_added = NOW()");
        }


        if($update)
        {
            return true;
        }

        else{
            return false;
        }
       // $quer="SELECT order_id, firstname, order_status_id, date_added FROM " . DB_PREFIX . "order WHERE order_id IN ($arrExpQuery)";
        //$query=$this->db->query("SELECT order_id, firstname, order_status_id, date_added FROM " . DB_PREFIX . "order WHERE order_id IN ($arrExpQuery)");
    }
}