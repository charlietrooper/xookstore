<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 15/02/2016
 * Time: 11:10 AM
 */
class ModelCatalogLibro extends Model {
    public function obtenDatos($product_id)
    {
        $fecha_log = date('Y-m-d');
        $logger = new Log("update/update$fecha_log.log");

        $url = $this->config->get('orbile_catalog_url');
        if(strlen($product_id)<32) {
            $product = $this->db->query("SELECT p.model FROM " . DB_PREFIX . "product p WHERE p.product_id = '" . $product_id . "'");
            $ActiveRevisionID = $product->row['model'];
        } else {
            $ActiveRevisionID = $product_id;
        }


        if(strlen($ActiveRevisionID) != 36){
            return 'Falso';
        } else {

            //Se asigna el cURL a la variable $ch
            $ch = curl_init();

            //enviamos la url con los parámetros ingresados por el aprtner
            curl_setopt($ch, CURLOPT_URL, "$url/books/getBook/rev/$ActiveRevisionID");

            //Se asigna el tiempo de espera
            curl_setopt($ch, CURLOPT_TIMEOUT, 100);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $resultado = curl_exec ($ch);

            if($resultado === false)
            {
                $error = 'Curl error: ' . curl_error($ch);
                $logger->write('Error curl: ' . json_encode($error));
            } elseif ($resultado == null || $resultado == 'null') {
                return null;

                $logger->write('Resultado null: ' . $resultado);
            }
            else {
                //$error = curl_error($ch);
                $libro_completo = json_decode($resultado, true);

                $libro = array();
                $libro['completo'] = $libro_completo;

                $price = $libro_completo['activeRevision']['prices']['price'];

                //Definir el precio
                $hoy = strtotime(date('Y-m-d H:i:s'));
                $from = strtotime($price['@attributes']['from']);

                //Se verifica si en la colección de precios que esta en raíz de price, coincide con el rango de fechas y si es preorder en caso de que coincida
                $rangofecha = (isset($price['@attributes']['to']) ? ($from <= $hoy && $hoy <= strtotime($price['@attributes']['to'])) : $from <= $hoy) ? true : false;

                //Si se cumplen las condiciones anteriores, el libro es pre orden, sino, se revizarán las siguientes colecciones de precios en caso de que existan
                if($rangofecha == true)
                {
                    if(isset($price['sellingprice']['@value'])) {
                        $libro['precio'] = $price['sellingprice']['@value'];
                    } else {
                        $libro['precio'] = $price['sellingprice'];
                    }
                    if(isset($price['listprice']['@value'])) {
                        $libro['prec'] = $price['listprice']['@value'];
                    } else {
                        $libro['prec'] = $price['listprice'];
                    }
                }
                else {
                    foreach ($price as $p => $val) {
                        if (isset($val['@attributes']['currency'])) {
                            //Se define la fecha
                            $From = $val['@attributes']['from'];

                            //Se verifica si la coleccion de precios de los arrays 0,1...n, coincide con el rango de fechas
                            $RangoFecha = (isset($val['@attributes']['to']) ? ($From <= $hoy && $hoy <= $val['@attributes']['to']) : $From <= $hoy) ? true : false;

                            //Si se cumple la condicion anteriorse toma el valor de sellingprice
                            if ($RangoFecha == true) {
                                if(isset($val['sellingprice']['@value']))
                                {
                                    $libro['precio'] = $val['sellingprice']['@value'];
                                } else {
                                    $libro['precio'] = $val['sellingprice'];
                                }
                                if(isset($price['listprice']['@value'])) {
                                    $libro['prec'] = $price['listprice']['@value'];
                                } else {
                                    $libro['prec'] = $price['listprice'];
                                }

                                break;
                            }
                        }
                    }
                    if(!isset($libro['precio'])){
                        $libro['precio'] = null;
                    }
                    if(!isset($libro['prec'])){
                        $libro['prec'] = null;
                    }
                }

                if($libro['precio'] != null) {
                    $query = $this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . (int)$libro['precio'] . "' WHERE product_id = '" . (int)$product_id . "'");
                }

                if(is_array($libro_completo['activeRevision']['publisher']))
                {
                    $libro['publisher'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['publisher']['name']);
                }

                else
                {
                    $libro['publisher']="No disponible";
                }

                $libro['titulo'] = $libro_completo['activeRevision']['title'];
                $libro['isbn'] = $libro_completo['isbn'];
                $libro['descripcion'] = $libro_completo['activeRevision']['description'];
                $libro['accountHolderId'] = $libro_completo['accountHolderId'];
                //$libro['LeerEn'] = $libro_completo['activeRevision']['readThisOn'];
                $libro['readThisOn'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['readThisOn']);
                $libro['LeerEn'] = $this->obtenerDatos($libro_completo['activeRevision']['readThisOn']);
                $libro['Formatos'] = $this->obtenerFormatos($libro_completo['activeRevision']['readThisOn']);
                $libro['autor'] = $libro_completo['activeRevision']['contibutors'];
                $libro['contibutors'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['contibutors']);
                $libro['imprint'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['imprint']);
                $libro['isKids'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['isKids']);
                $libro['hasPreview'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['hasPreview']);
                $libro['subtitle'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['subtitle']);
                $libro['isAdultMaterial'] = $this->obtenerTablaAnidada($libro_completo['activeRevision']['isAdultMaterial']);
                $libro['preOrden'] = $this->esPreOrden($libro_completo['activeRevision']);
                $libro['imagen'] = $libro_completo['activeRevision']['links']['coverImage'];
                $libro['currency'] = $libro_completo['activeRevision']['prices']['price']['@attributes']['currency'];
                $libro['language'] = $libro_completo['activeRevision']['language'];
                $libro['related'] = $libro_completo['activeRevision']['relatedProducts'];

                return $libro;
            }
        }
    }

    public function obtenerTablaAnidada($array) {
        $ini = '<table style="border:1px solid #ddd; border-collapse: separate; border-spacing: 1px; vertical-align: top; font-size: small;">';
        $fin = '</table>';
        $tabla = '';

        if(is_array($array))
        {
            foreach ($array as $id => $valor) {
                if (is_array($valor)) {
                    $tabla .= '<tr><td valign="top">' . $id .': </td><td>' . $this->obtenerTablaAnidada($valor) . '</td></tr>';
                } else {
                    if(empty($valor)) {
                        $tabla .= '<tr><td valign="top">' . $id .': </td><td> (vacío) </td></tr>';
                    } else {
                        $tabla .= '<tr><td valign="top">' . $id . ': </td><td>' . $valor . '</td></tr>';
                    }
                }
            }
            return $ini . $tabla . $fin;
        } else {
            if(empty($array)) {
                return '(vacío)';
            } else {
                return $array;
            }
            return $array;
        }

    }

    public function obtenerDatos($array) {
        $arrDatos=[];
        $datos="";
        if(is_array($array))
        {
            foreach ($array as $id => $valor) {
                if (is_array($valor)) {
                    $datos .=  $this->obtenerDatos($valor);
                } else {
                    if(empty($valor)) {
                        $datos .= '';
                    } else if(($id=="name")) {
                        if($valor=="Windows")
                        {
                            $datos .= "Windows Phone, ";
                        }
                        else{
                            $datos .= $valor .", ";
                        }

                    }
                }
            }
            return $datos;
        } else {
            if(empty($array)) {
                return '';
            } else {
                return $array;
            }
            return $array;
        }

    }

    public function obtenerFormatos($array) {
        $arrDatos=[];
        $datos="";
        if(is_array($array))
        {
            foreach ($array as $id => $valor) {
                if (is_array($valor)) {
                    $datos .=  $this->obtenerFormatos($valor);
                } else {
                    if(empty($valor)) {
                        $datos .= '';
                    } else if(($id=="format")) {
                        if($valor=="FixedLayoutEpub")
                        {
                            $datos .= "Epub, ";
                        }
                        else if($valor=="Epub"){
                            $datos .= $valor .", ";
                        }

                    }
                }
            }
            return $datos;
        } else {
            if(empty($array)) {
                return '';
            } else {
                return $array;
            }
            return $array;
        }

    }


    public function esPreOrden($Libro) {
        //Se obtiene la fecha actual
        $hoy = strtotime(date('Y-m-d H:i:s'));

        //Se obtiene el valor de la fecha de available(Lanzamiento para venta del libro)
        $available = strtotime($Libro['available']['from']['@value']);

        if($available <= $hoy){
            $esPosiblePreOrden = false;
        }
        else{
            if(isset($Libro['availableForPreorder']['from']['@value'])){
                $esPosiblePreOrden = true;
            }
            else{
                $esPosiblePreOrden = false;
            }
        }
        //si se cuplen las condiciones anteriores, se pasa al siguiente filtro, sino el libro es falsa pre orden
        if($esPosiblePreOrden == true)
        {
            unset($esPosiblePreOrden);

            //Se obtienen los datos de las fechas y del preorden
            $from = strtotime($Libro['prices']['price']['@attributes']['from']);
            $preOrden = $Libro['prices']['price']['@attributes']['isPreOrder'];

            //Se verifica si en la colección de precios que esta en raíz de price, coincide con el rango de fechas y si es preorder en caso de que coincida
            $esPosiblePreOrden = ((isset($Libro['prices']['price']['@attributes']['to']) ? ($from <= $hoy && $hoy <= strtotime($Libro['prices']['price']['@attributes']['to'])) : $from <= $hoy) ? ($preOrden == 'true') : false) ? true : false;

            //Si se cumplen las condiciones anteriores, el libro es pre orden, sino, se revizarán las siguientes colecciones de precios en caso de que existan
            if($esPosiblePreOrden == true)
            {
                $esPreOrden = 'Si';
            }
            else
            {
                $precio = $Libro['prices']['price'];
                foreach($precio as $p => $val)
                {
                    if (isset($val['@attributes']['currency'])){
                        //Se definen las fechas y el preorden
                        $From = strtotime($val['@attributes']['from']);
                        $PreOrden = $val['@attributes']['isPreOrder'];

                        //Se verifica si la coleccion de precios de los arrays 0,1...n, coincide con el rango de fechas y si es preorder en caso de que coincida
                        $PosiblePreOrden = ((isset($val['@attributes']['to']) ? ($From <= $hoy && $hoy <= strtotime($val['@attributes']['to'])) : $From <= $hoy) ? ($PreOrden == 'true') : false) ? true : false;

                        //Si se cumplen las condiciones anteriores, el libro es preorden y se rompe el ciclo
                        if($PosiblePreOrden == true)
                        {
                            $esPreOrden = 'Si';
                            break;
                        }
                    }
                }
                //Si no se cumplió ninguno de las condiciones del foreach, entonces el libro será falsa preorden
                if(!isset($PosiblePreOrden) || $PosiblePreOrden == false){
                    $esPreOrden = 'No';
                }
            }
        } else
        {
            $esPreOrden = 'No';
        }
        return $esPreOrden;
    }
}