<?php
class ModelCatalogProductdata extends Model {

    public function getProductByActiveRevision($model) {
        $product_id=0;

        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE model = '" . $model . "'");


        $product_id=$query->row['product_id'];
        return $product_id;
    }


    public function obtenerPrecioReal($precio, $special)
    {
        $precioReal = '';
        if ($precio) {
            $precioReal = 0;

            if (!$special) {
                $precioReal = $precio;
            } else {
                $precioReal = $special;
            }
        }
        return $precioReal;

    }

    public function reducirNombre($nombre)
    {
        $headerNombre = "";

        if ($nombre) {
            if (strlen($nombre) > 40) {
                $temp = substr($nombre, 0, 40);
                $headerNombre = $temp . '...';
            } else {
                $headerNombre = $nombre;
            }
        }

        return $headerNombre;
    }


    public function obtenerAutor($infoLibro)
    {
        $aut='';
        if ($infoLibro)
        {
            $aut=' / ';
            // $what=isset($infoLibro['activeRevision']['contibutors']['contributor']['@value']);
            if(isset($infoLibro['contributor']['@value']))
            {
                $aut.=$infoLibro['contributor']['@value'];
            }
            else
            {
                $autores=$infoLibro['contributor'];
                $count=count($autores);
                $n=1;
                foreach($autores as $autor)
                {
                    if (!($n>2))
                    {
                        $holder=$autor['@value'];
                        if (!($n==$count))
                        {
                            $aut.=$holder.", ";
                        }
                        else{
                            $aut.=$holder;
                        }
                    }

                    $n++;

                }
                if($count>2)
                {
                    $temp=$aut;
                    $aut=substr($temp, 0, -2);
                }

            }
        }


        return $aut;
    }

    public function obtenerHeight($img)
    {
        $height=0;
        if($img)
        {
            $imageSize=getimagesize($img);
            $height=$imageSize[1];
        }

        return $height;
    }

    public function obtenerDescripcion($infoLibro)
    {
        //$check=substr($infoLibro,0,3);

        //A veces el JSON viene con un montón de tags de HTML, con esto las quitamos
        $infoLibro=strip_tags($infoLibro);

        //$descripcion='Descripción no disponible';
        $descripcion=array();
        $descripcionBig='Descripción no disponible';
        $descripcionSmall='Descripción no disponible';
        if($infoLibro)
        {
            $desc=$infoLibro;

            if(strlen($desc)>300)
            {
                $temp2 = substr($desc,0,300);
                $temp3 = substr($desc,0,200);
                $descripcionBig=$temp2.'...';
                $descripcionSmall=$temp3.'...';
            }
            else{
                $descripcionBig=$desc;
                $temp3 = substr($desc,0,200);
                $descripcionSmall=$temp3.'...';
            }

        }
        array_push($descripcion,$descripcionBig);
        array_push($descripcion,$descripcionSmall);
        array_push($descripcion,$infoLibro);

        return $descripcion;
    }


    public function obtenerPrecioFinal($precio, $special)
    {
        $precioFinal=0;
        if ($precio)
        {
            $precioFinal=0;

            if (!$special)
            {
                $precioFinal=$precio;
            }
            else
            {
                $precioFinal=$special;
            }
        }
        return $precioFinal;
    }

    public function obtenerLenguajeCompleto($leng)
    {
        $language="";
        if (strtoupper($leng)=="EN")
        {
            $language="Inglés";
        }

        else if(strtoupper($leng)=="ES")
        {
            $language="Español";
        }

        else{
            $language="Información No Disponible";
        }

        return $language;

    }

    public function obtenerReadThisOn($info)
    {
        $readThis="";
        if ($info)
        {
            $temp=substr_replace($info,".",-2);
            $readThis=$temp;
        }
        else
        {
            $readThis="Información no disponible";
        }

        return $readThis;

    }

    public function obtenerFormato($info)
    {
        $formatos="";
        if ($info)
        {
            // $temp=substr_replace($info,".",-2);
            // $readThis=$temp;
            $prueba=preg_match('/EPUB/',strtoupper($info));

            if (preg_match('/EPUB/',strtoupper($info))) {
                $formatos.='Epub, ';
            }

            if (preg_match('/PDF/',strtoupper($info))) {
                $formatos.='Pdf, ';
            }

            if($formatos)
            {
                $formatos=substr_replace($formatos,".",-2);
            }

            else{
                $formatos="Información no disponible";
            }



        }
        else
        {
            $formatos="Información no disponible";
        }

        return $formatos;

    }


}