<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 29/07/2016
 * Time: 07:47 AM
 */

/**
 * Class ModelCheckoutApi, para mandar llamar al API y poder realizar las compras de los libros electónicos
 */
class ModelCheckoutApi extends Model {
    /*
     * Función para mandar llevar a cabo la utenticación del usuario en kobo
     * @param $customer_id
     * @param $email
     * @return array|mixed|null
     */
    public function autenticacion($customer_id, $email) {
        //Se determina la fecha para nombrar el archivo de log
        $fecha_log = date('Y-m-d');
        $logger = new Log("purchases/purchases_on_$fecha_log.log");

        //Se obtienen los datos del config(url del API, usuario y cliente "orbile")
        $url_api = $this->config->get('orbile_api_base_url');
        $api_user = $this->config->get('x-api-user');
        $api_key = $this->config->get('x-api-key');
        $logger->write('Archivos del config: url - ' . json_encode($url_api) . 'userOrbile - ' . json_encode($api_user) . 'keyOrbile - ' . json_encode($api_key));

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url_api/authservice/authenticate_or_register/$email/$customer_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_user\r\nx-api-key:$api_key"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);

        //Código http en la respuesta
        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            //Si el código de error es mayor a 400, se devuelve el error obtenido
            $error = $httpcode['http_code'].": ".$this->http_status_code_string('400'). "=>" .$resultado;
            curl_close($ch);
            $error_aut = array(
                'error_autentication'   => true,
                'error'                 => $error
            );
            return $error_aut;
            $logger->write('SE devuelve: ' . json_encode($error_aut));
        } elseif($resultado == false) {
            //Si la conexión es false, devuelve el curl_error
            $error_aut = array(
                'error_autentication'   => true,
                'error'                 => curl_error($ch)
            );
            return $error_aut;
            $logger->write('SE devuelve: ' . json_encode($error_aut));
        } else {
            //Se decodifica el JSON que envió la respuesta de la API
            $result = json_decode($resultado, true);

            //Se guarda el kobo_id en sessión
            $this->session->data['kobo_id'] = $result['KoboUserId'];

            //Se cierra la conexión cURL
            curl_close($ch);

            //se devuelve lo obtenido
            return $result;
            $logger->write('SE devuelve: ' . json_encode($result));
        }
    }

    /*
     * Función que ejecuta la verificacion del cliente y sus productos que va a comprar con
     * su biblioteca en kobo
     * @param $post_fields
     * @param $prods
     * @return array
     */
    public function verificacion($post_fields, $prods){
        //Se determina la fecha para nombrar el archivo de log
        $fecha_log = date('Y-m-d');
        $logger = new Log("purchases/purchases_on_$fecha_log.log");

        //Se obtienen los datos del config(url del API, usuario y cliente "orbile")
        $url_api = $this->config->get('orbile_api_base_url');
        $api_user = $this->config->get('x-api-user');
        $api_key = $this->config->get('x-api-key');
        $logger->write('Archivos del config: url - ' . json_encode($url_api) . 'userOrbile - ' . json_encode($api_user) . 'keyOrbile - ' . json_encode($api_key));

        //iniciamos el curl
        $ch = curl_init();

        //pasamos la url
        curl_setopt($ch, CURLOPT_URL, "$url_api/purchase_service/verify_purchase");

        //TIEMPO DE RESPUESTA
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //LE DECIMOS QUE QUEREMOS RECOGER UNA RESPUESTA
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);

        //LE DECIMOS QUE PARAMETROS ENVIAMOS
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

        $headers = array();
        $headers[] = "x-api-user:$api_user";
        $headers[] = "x-api-key:$api_key";

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //Se obtiene la respuesta
        $resultado = curl_exec($ch);

        //Se obtiene el codigo http de respuesta
        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            //si el codigo http es mayor a 400, se devuelve el error obtenido
            $error = $httpcode['http_code'].": ".$this->http_status_code_string('400'). "=>" .$resultado;
            curl_close($ch);
            $error_aut = array(
                'error_purchase'    => true,
                'error'             => $error
            );
            return $error_aut;
            $logger->write('SE devuelve: ' . json_encode($error_aut));
        } elseif($resultado == false) {
            //Si el resultado es flaso, se devuelve el curl_error
            $error_aut = array(
                'error_purchase'   => true,
                'error'                 => curl_error($ch)
            );
            return $error_aut;
            $logger->write('SE devuelve: ' . json_encode($error_aut));
        } else{
            //Se cierra el curl
            curl_close($ch);

            //Se decodifica la respuesta
            $result = json_decode($resultado, true);

            $respuesta = array();

            //Se carga el modelo "libro" para aobtener los datos reuqeridos de(l) libro(s) a comprar
            $this->load->model('catalog/libro');
            $this->load->model('catalog/product');

            //Se genera el array de datos para cada libros a comprar
            foreach($result['Items']['Item'] as $producto) {
                foreach($prods as $producto2) {
                    if($producto['Id'] == $producto2['active_id']) {

                        if($this->verificarPendinte($producto2))
                        {
                            $producto['Result']='ContentPending';
                            $result['Result'] = "InternalError";
                        }

                        $libro = $this->model_catalog_libro->obtenDatos($producto2['id']);
                        //Si no se encontró en el catalogo entnonces mandamos que el resultado es INTERNALERROR, para que el cliente no pueda comprar el libro,
                        //pues no está disponible realmente en el catálogo.
                        if(!$libro)
                        {
                            $result['Result'] = "InternalError";
                            $datosAdicionales=$this->model_catalog_product->getProduct($producto2['id']);
                            $respuesta[] = array(
                                'id'            => $producto['Id'],
                                'isPreOrder'    => $producto['isPreOrder'],
                                'Result'        => "InternalError",
                                'Titulo'        => $datosAdicionales['name'],
                                'Isbn'          => null,
                                'Precio'        => null,
                                'Imagen'        => null,
                            );
                        }

                        else{
                            $respuesta[] = array(
                                'id'            => $producto['Id'],
                                'isPreOrder'    => $producto['isPreOrder'],
                                'Result'        => $producto['Result'],
                                'Titulo'        => $libro['titulo'],
                                'Isbn'          => $libro['isbn'],
                                'Precio'        => $libro['precio'],
                                'Imagen'        => $libro['imagen'],
                            );

                        }

                    }
                }
            }



            //Se arma la respuesta
            $result['Items']['Item'] = $respuesta;
            $regreso = array(
                'resultado' => $result,
                'datos'     => json_decode($post_fields)
            );

            //Se retorna lo obtenido
            return $regreso;
            $logger->write('SE devuelve: ' . json_encode($regreso));
        }
    }

    public function verificarPendinte($producto2){
        $this->load->model('checkout/order');
        $email = $this->customer->getEmail();
        $ordenExistente = $this->model_checkout_order->getOrderPending($producto2['id'],$email);
        if ($ordenExistente)
        {
            return true;
        }

        else{
            return false;
        }
    }

    /*
     * Función que ndevuelve el texto de los codigos de respuesta http
     * @param $code
     * @param bool $include_code
     * @return string
     */
    public function http_status_code_string($code, $include_code = false)
    {
        // Source: http://en.wikipedia.org/wiki/List_of_HTTP_status_codes

        switch($code)
        {
            // 1xx Informational
            case 100: $string = 'Continue'; break;
            case 101: $string = 'Switching Protocols'; break;
            case 102: $string = 'Processing'; break; // WebDAV
            case 122: $string = 'Request-URI too long'; break; // Microsoft

            // 2xx Success
            case 200: $string = 'OK'; break;
            case 201: $string = 'Created'; break;
            case 202: $string = 'Accepted'; break;
            case 203: $string = 'Non-Authoritative Information'; break; // HTTP/1.1
            case 204: $string = 'No Content'; break;
            case 205: $string = 'Reset Content'; break;
            case 206: $string = 'Partial Content'; break;
            case 207: $string = 'Multi-Status'; break; // WebDAV

            // 3xx Redirection
            case 300: $string = 'Multiple Choices'; break;
            case 301: $string = 'Moved Permanently'; break;
            case 302: $string = 'Found'; break;
            case 303: $string = 'See Other'; break; //HTTP/1.1
            case 304: $string = 'Not Modified'; break;
            case 305: $string = 'Use Proxy'; break; // HTTP/1.1
            case 306: $string = 'Switch Proxy'; break; // Depreciated
            case 307: $string = 'Temporary Redirect'; break; // HTTP/1.1

            // 4xx Client Error
            case 400: $string = 'Bad Request'; break;
            case 401: $string = 'Unauthorized'; break;
            case 402: $string = 'Payment Required'; break;
            case 403: $string = 'Forbidden'; break;
            case 404: $string = 'Not Found'; break;
            case 405: $string = 'Method Not Allowed'; break;
            case 406: $string = 'Not Acceptable'; break;
            case 407: $string = 'Proxy Authentication Required'; break;
            case 408: $string = 'Request Timeout'; break;
            case 409: $string = 'Conflict'; break;
            case 410: $string = 'Gone'; break;
            case 411: $string = 'Length Required'; break;
            case 412: $string = 'Precondition Failed'; break;
            case 413: $string = 'Request Entity Too Large'; break;
            case 414: $string = 'Request-URI Too Long'; break;
            case 415: $string = 'Unsupported Media Type'; break;
            case 416: $string = 'Requested Range Not Satisfiable'; break;
            case 417: $string = 'Expectation Failed'; break;
            case 422: $string = 'Unprocessable Entity'; break; // WebDAV
            case 423: $string = 'Locked'; break; // WebDAV
            case 424: $string = 'Failed Dependency'; break; // WebDAV
            case 425: $string = 'Unordered Collection'; break; // WebDAV
            case 426: $string = 'Upgrade Required'; break;
            case 449: $string = 'Retry With'; break; // Microsoft
            case 450: $string = 'Blocked'; break; // Microsoft

            // 5xx Server Error
            case 500: $string = 'Internal Server Error'; break;
            case 501: $string = 'Not Implemented'; break;
            case 502: $string = 'Bad Gateway'; break;
            case 503: $string = 'Service Unavailable'; break;
            case 504: $string = 'Gateway Timeout'; break;
            case 505: $string = 'HTTP Version Not Supported'; break;
            case 506: $string = 'Variant Also Negotiates'; break;
            case 507: $string = 'Insufficient Storage'; break; // WebDAV
            case 509: $string = 'Bandwidth Limit Exceeded'; break; // Apache
            case 510: $string = 'Not Extended'; break;

            // Unknown code:
            default: $string = 'Unknown';  break;
        }
        if($include_code)
            return $code . ' '.$string;
        return $string;
    }

    /*
     * Función que completa la compra agregando el(los) libro(s) a la biblioteca del usuario en kobo,
     * y devolviendo la liga de descarga de(los) producto(s)
     * @param $purchase_files
     * @return array|mixed|null
     */
    public function compra($purchase_files) {
        //Se determina la fecha para nombrar el archivo de log
        $fecha_log = date('Y-m-d');
        $logger = new Log("purchases/purchases_on_$fecha_log.log");

        //Se obtienen los datos del config(url del API, usuario y cliente "orbile")
        $url_api = $this->config->get('orbile_api_base_url');
        $api_user = $this->config->get('x-api-user');
        $api_key = $this->config->get('x-api-key');
        $logger->write('Archivos del config: url - ' . json_encode($url_api) . 'userOrbile - ' . json_encode($api_user) . 'keyOrbile - ' . json_encode($api_key));

        //iniciamos el curl
        $ch = curl_init();

        //pasamos la url
        curl_setopt($ch, CURLOPT_URL, "$url_api/purchase_service/purchase");

        //TIEMPO DE RESPUESTA
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //LE DECIMOS QUE QUEREMOS RECOGER UNA RESPUESTA
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Especificamos el post
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);

        //LE DECIMOS QUE PARAMETROS ENVIAMOS
        curl_setopt($ch, CURLOPT_POSTFIELDS, $purchase_files);

        //Lo que hace la funcion http_build_query es convertir un array(a=>1,b=>2) en una cadena de texto tipo &a=1&b=2

        $headers = array();
        $headers[] = "x-api-user:$api_user";
        $headers[] = "x-api-key:$api_key";


        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //Se obtiene la respuesta
        $resultado = curl_exec($ch);

        //Se obtiene el codigo http de respuesta
        $httpcode = curl_getinfo($ch);
//Aquí escribir chequeo, si no existe la url, devolver error.
        if($httpcode['http_code'] > 400)
        {
            //Si el codigo http es mayor a 400, se devuelve el texto del error
            $error = $httpcode['http_code'].": ".$this->http_status_code_string('400'). "=>" .$resultado;
            curl_close($ch);
            $respuesta = array(
                'error_purchase'    => true,
                'error'             => $error
            );
            $logger->write('SE devuelve: ' . json_encode($regreso));
        } elseif($resultado == false) {
            //Si el resultado es falso, se retorna el Curl_error
            $respuesta = array(
                'error_purchase'   => true,
                'error'            => curl_error($ch)
            );
        } else{
            //Se cierra la conexión cURL
            curl_close($ch);

            //se decodifica la respuesta
            $respuesta = json_decode($resultado);
        }

        //Se devuelva la respuesta
        return $respuesta;
        $logger->write('SE devuelve: ' . json_encode($respuesta));
    }

    /*
     * Función que añade los datos del cliente a una tabla para tener registro de éste al comprar
     * como invitado
     * @param $data
     * @return mixed
     */
    public function guest($data) {
        //Se determina la fecha para nombrar el archivo de log
        $fecha_log = date('Y-m-d');
        $logger = new Log("purchases/purchases_on_$fecha_log.log");

        //Se consulta si ya esta registrado el cliente_invitado
        $guest = $this->db->query("SELECT * FROM " . DB_PREFIX . "guest WHERE email = '" . $this->db->escape($data['email']) . "'");

        if($guest->num_rows == 0) {
            //Si no esta, se agrega y se devuelve el id
            $agregacion = $this->db->query("INSERT INTO " . DB_PREFIX . "guest SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', address = '" . $this->db->escape($data['address']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = " . (int)$data['country_id'] . ", iso_code_2 = '" . $this->db->escape($data['iso_code_2']) . "', zone = '" . $this->db->escape($data['zone']) . "', date_added = NOW()");
            $guest_id = $this->db->getLastId();
            $logger->write('SE Seagregó nuevo cliente comprando como invitado: ' . json_encode($agregacion));
        } else {
            //Si ya esta, se consulta y devuelva el id
            $actualizacion = $this->db->query("UPDATE " . DB_PREFIX . "guest SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', address = '" . $this->db->escape($data['address']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = " . (int)$data['country_id'] . ", iso_code_2 = '" . $this->db->escape($data['iso_code_2']) . "', zone = '" . $this->db->escape($data['zone']) . "', date_added = NOW()");
            $guest_id = $guest->rows[0]['guest_id'];
            $logger->write('SE actualizó cliente existente comprando como invitado: ' . json_encode($actualizacion));
        }
        $logger->write('Id de cliente (invitado) a retornar: ' . json_encode($guest_id));
        return $guest_id;
    }
}