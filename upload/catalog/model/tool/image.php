<?php
class ModelToolImage extends Model {
	public function resize($filename, $width, $height) {
        if(substr($filename, 0,4) == 'http') {
            $imagenKobo = $this->imagenKobo($filename, $width);
            return $imagenKobo;
        } else {
            if (!is_file(DIR_IMAGE . $filename)) {
                return;
            }

            $extension = pathinfo($filename, PATHINFO_EXTENSION);

            $old_image = $filename;
            $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

            if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
                $path = '';

                $directories = explode('/', dirname(str_replace('../', '', $new_image)));

                foreach ($directories as $directory) {
                    $path = $path . '/' . $directory;

                    if (!is_dir(DIR_IMAGE . $path)) {
                        @mkdir(DIR_IMAGE . $path, 0777);
                    }
                }

                list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

                if ($width_orig != $width || $height_orig != $height) {
                    $image = new Image(DIR_IMAGE . $old_image);
                    $image->resize($width, $height);
                    $image->save(DIR_IMAGE . $new_image);
                } else {
                    copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
                }
            }

            if ($this->request->server['HTTPS']) {
                return $this->config->get('config_ssl') . 'image/' . $new_image;
            } else {
                return $this->config->get('config_url') . 'image/' . $new_image;
            }
        }
	}

    public function imagenKobo($archivo, $Ancho) {
        $division_url = explode('/', $archivo);
        //division_url[0] siempre regresa http...
        //hacemos el chequeo para saber si la página está en HTTPS
        //para saber si pasar https o http al url de la imagen
        if ($this->request->server['HTTPS']) {
            $division_url[0]='https:';
        } else {
            $division_url[0]='http:';
        }
        $url_img = $division_url[0] . "/" . $division_url[1] . "/" . $division_url[2] . "/" .$division_url[3] . "/" .$division_url[4] . "/$Ancho/$Ancho/false/Imagen.jpg";
        return $url_img;
    }

}
