<?php
class ModelToolSizecheck extends Model {
    public function imageDimensions($img)
    {

        $resize=false;

        //$dimensions=getimagesize($img);
        //checamos si es más grande el width que el height

        $start = microtime(true);

        $url = $img;

        $raw = $this->ranger($url);
        $im = @imagecreatefromstring($raw);

        $width = imagesx($im);
        $height = imagesy($im);

        //$stop = round(microtime(true) - $start, 5);

        $dimensions = array($width,$height);
        //  echo $width." x ".$height." ({$stop}s)";
        if ($dimensions)
        {
            if($dimensions[0]>$dimensions[1])
            {
                $resize=true;
            }
        }

        return $resize;

    }

    function ranger($url){
        $headers = array(
            "Range: bytes=0-32768"
        );

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }
}