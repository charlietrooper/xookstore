<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

        <div class="row">
            <div class="stripedHeader">
                <h2 class="stripes"><?php echo $text_bienvenida; ?></h2>
            </div>
            <div class="col-md-6 col-md-offset-3">

                <?php if ($ordenesCanceladas !== '') { ?>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $text_aviso; ?></h3>
                    </div>
                    <div class="panel-body">
                            <?php echo $ordenesCanceladas; ?>
                    </div>
                </div>

                <?php } ?>
                <!--MI CUENTA-->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $text_my_account; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></p>
                                <a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></p>
                            </div>
                            <div class="col-md-6">
                                <p><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></p>
                                <p><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <!--MI CUENTA-->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $text_my_orders; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></p>
                            </div>
                            <div class="col-md-6">
                                <p><a href="<?php echo $libuser; ?>"><?php echo $text_libreria; ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 