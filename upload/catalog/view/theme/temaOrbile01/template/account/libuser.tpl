<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row">
        <div id="content"><?php echo $content_top; ?>
            <div class="stripedHeader">
                <h2 class="stripes"><?php echo $heading_title; ?></h2>
            </div>
            <div class="msgKobo">
                <div class="alert alert-warning" role="alert">
                    <strong>¡Atención!</strong> Para poder leer sus libros, debe descargar la aplicaci&oacute;n de Kobo by Orbile. Ah&iacute; encontrar&aacute; todas sus compras.
                </div>
            </div>

            <?php if ($libuser) { ?>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead class="libHeader">
                    <tr>
                        <td class="text-left"><?php echo $column_order_id; ?></td>
                        <td class="text-left"><?php echo $column_name; ?></td>
                        <td class="text-left"><?php echo $column_size; ?></td>
                        <td class="text-left"><?php echo $column_date_added; ?></td>
                       <!-- <td class="text-left"><?php echo $column_url; ?></td>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($libuser as $book) { ?>
                    <tr>
                        <td class="text-left"><?php echo $book['title']; ?></td>
                        <td class="text-left"><?php echo $book['isbn']; ?></td>
                        <td class="text-left"><?php echo $book['imprint']; ?></td>
                        <td class="text-left"><?php echo $book['purchase_date']; ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <?php } ?>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>

<?php echo $footer; ?>