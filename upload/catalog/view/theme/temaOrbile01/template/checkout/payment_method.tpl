<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>

<?php if ($mostrarPanel) { ?>
<div id="verificacion" class="panel">
    <?php if ($librosError) { ?>
        <?php foreach ($librosError as $libro) { ?>
            <div class="alert alert-warning" role="alert">
                <?php echo $libro; ?>
            </div>
         <?php } ?>
    <?php } ?>

</div>
<?php } else { ?>

    <?php if ($payment_methods) { ?>
        <p><?php echo $text_payment_method; ?></p>
        <div class="radioPayment">
        <?php foreach ($payment_methods as $payment_method) { ?>

                <?php if ($payment_method['code'] == $code || !$code) { ?>
                <?php $code = $payment_method['code']; ?>
                <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
                <?php } ?>
                <div class="radioLabel">
                  <label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?>
                      <img src="<?php echo $payment_method['src']; ?>" alt="Tiendas" class="tiendas pull-right" style="display: inline;" height="30px">
                  </label>
                <!--<?php echo $payment_method['title']; ?>-->

                </div>
                <?php if ($payment_method['terms']) { ?>
                (<?php echo $payment_method['terms']; ?>)
                <?php } ?>

        <?php } ?>
        </div>
    <?php } ?>
<?php } ?>


<?php if ($text_agree && !$mostrarPanel) { ?>
<div class="buttons">
  <div class="pull-right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    &nbsp;
      <?php echo $btninfo; ?>
  </div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="pull-right">
      <?php echo $btninfo; ?>
  </div>
</div>
<?php } ?>

