<div id="search" class="input-group">
    <form class="form-inline" name="search" id="myform">
        <div class="form-group form1">
            <div class="nav-left">
                <div class="nav-search-select">
                    <select name="categoriesSearch" id="input-categories" class="form-control">
                        <?php if($searchCat == 'ALL') { ?>
                        <option value="ALL" selected="selected"><?php echo $text_searchCategories; ?></option>
                        <?php } else { ?>
                        <option value="ALL"><?php echo $text_searchCategories; ?></option>
                        <?php } ?>
                        <?php foreach ($categories as $cat) { ?>
                        <?php if($cat['name'] == $searchCat) { ?>
                        <option value="<?php echo $cat['name']; ?>" selected="selected"><?php echo $cat['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $cat['name']; ?>"><?php echo $cat['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group form2">
            <div class="nav-fill">
                <div class="nav-search-field">
                    <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
                </div>
            </div>
        </div>
        <div class="form-group form3">
            <div class="nav-right">
                <div class="nav-search-submit">
                   <span class="input-group-btn">
                    <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
                  </span>
                </div>
            </div>
        </div>
        <div class="radioSearch">
            <?php if($searchText == 'texto' || $searchText == '') { ?>
                <input type="radio" value="autor" id="radioAutor" name="autor-texto" />
                <label for="radioAutor"><?php echo $text_cbAutor; ?></label>
                <input type="radio" value="texto" id="radioTexto" name="autor-texto" checked />
                <label for="radioTexto"><?php echo $text_cbTexto; ?></label>
            <?php } else if ($searchText == 'autor') { ?>
                <input type="radio" value="autor" id="radioAutor" name="autor-texto" checked/>
                <label for="radioAutor"><?php echo $text_cbAutor; ?></label>
                <input type="radio" value="texto" id="radioTexto" name="autor-texto" />
                <label for="radioTexto"><?php echo $text_cbTexto; ?></label>

            <?php } ?>
        </div>


    </form>
</div>

<script>
    $("input[name='autor-texto']").change(function(){
       var checked = $("input[name='autor-texto']:checked").val();
        console.log(checked);
    });

</script>