<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <?php if(isset($api_purchase)) { ?>

            <h1><?php echo $respuesta_final; ?></h1>

            <table class="table table-bordered table-striped">
                <tr class="headerSuccess">
                    <th>Título de libro</th>
                    <th>Resultado de compra</th>
                </tr>
                <?php foreach($libros as $lib) { ?>
                <tr>
                    <td><?php echo $lib['titulo']; ?> </td>
                    <td><?php echo $lib['Result']; ?> </td>
                </tr>
                <?php } ?>
            </table>

            <?php } else { ?>
            <div id="display-success">
                <?php if($text_shortSuccess!=='Error en la compra') { ?>
                     <p class="successMSG"><b><i class="fa fa-check" aria-hidden="true"></i><?php echo $text_shortSuccess; ?></b></p>
                <?php } else { ?>
                      <p class="successMSG"><b><i class="fa fa-times" aria-hidden="true"></i><?php echo $text_shortSuccess; ?></b></p>
                <?php } ?>
                <div class="message-minor">
                    <?php if ($text_message!=='') { ?>
                       <?php echo $text_message; ?>
                    <?php } ?>
                </div>


            </div>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>"
                                           class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>


            <?php } ?>



            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>