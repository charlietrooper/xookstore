<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    <div class="stripedHeader">
        <h2 class="stripes"><?php echo $heading_title; ?></h2>
    </div>
      <div class="row">
        <div class="col-sm-12">
          <ul>
            <?php foreach ($categories as $category_1) { ?>
            <li class="headerList"><a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></a>
              <?php if ($category_1['children']) { ?>
                <div class="list-design">
                  <ul>
                    <?php foreach ($category_1['children'] as $category_2) { ?>
                    <li><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                      <?php if ($category_2['children']) { ?>
                      <ul>
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                        <?php } ?>
                      </ul>
                      <?php } ?>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
          <div class="row sitemapBottom">
              <div class="col-sm-3">
                  <ul>
                      <li class="headerList"><a href="<?php echo $promo; ?>"><?php echo $text_promo; ?></a></li>
                      <li class="headerList"><a href="<?php echo $top50; ?>"><?php echo $text_top50; ?></a></li>
                      <li class="headerList"><a href="<?php echo $novedades; ?>"><?php echo $text_novedades; ?></a></li>
                      <li class="headerList"><a href="<?php echo $masvistos; ?>"><?php echo $text_masvistos; ?></a></li>
                  </ul>
              </div>
              <div class="col-sm-3">
                  <ul>
                      <li class="headerList"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
                          <div class="list-design2">
                              <ul>
                                  <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
                                  <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
                                  <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
                                  <li><a href="<?php echo $history; ?>"><?php echo $text_history; ?></a></li>
                                  <li><a href="<?php echo $libuser; ?>"><?php echo $text_libuser; ?></a></li>
                              </ul>
                          </div>
                      </li>
                  </ul>
              </div>
              <div class="col-sm-3">
                  <ul>
                      <li class="headerList"><a href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a></li>
                      <li class="headerList"><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
                      <li class="headerList"><a href="<?php echo $search; ?>"><?php echo $text_search; ?></a></li>
                  </ul>
              </div>
              <div class="col-sm-3">
                  <ul>
                      <li class="headerList"><?php echo $text_information; ?>
                          <div class="list-design2">
                              <ul>
                                  <?php foreach ($informations as $information) { ?>
                                  <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                  <?php } ?>
                                  <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                              </ul>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>
        <!---->
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>