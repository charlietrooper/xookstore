
<div class="favs">
    <div class="wrapFavoritos">
        <div class="headerSlider">
            <p><b>TOP</b><span> Favoritos </span> <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
            </p>

        </div>
    </div>
    <div class="fcontainer">
        <div class="fitem">
            <div class="fcontainer">
                <div class="fitemsub">
                    <a href="<?php echo $products[0]['href']; ?>">
                       <img src="<?php echo $products[0]['thumb']; ?>" alt="<?php echo $products[0]['name']; ?>" title="<?php echo $products[0]['name']; ?>" />
                    </a>
                </div>
                <div class="fitemsub">
                    <div class="textoLibros">
                        <h4 class="sp_header">
                            <a href="<?php echo $products[0]['href']; ?>"><?php echo $products[0]['headerName']; ?></a>
                        </h4>
                        <p class="autor_header" href="<?php echo $products[0]['href']; ?>"><?php echo $products[0]['author']; ?></p>
                    </div>
                </div>
            </div>

        </div>
        <div class="fitem">
            <div class="fcontainer">
                <div class="fitemsub">
                    <a href="<?php echo $products[1]['href']; ?>">
                    <img src="<?php echo $products[1]['thumb']; ?>" alt="<?php echo $products[1]['name']; ?>" title="<?php echo $products[1]['name']; ?>" />
                    </a>
                </div>
                <div class="fitemsub">
                    <div class="textoLibros">
                        <h4 class="sp_header">
                            <a href="<?php echo $products[1]['href']; ?>"><?php echo $products[1]['headerName']; ?></a>
                        </h4>
                        <p class="autor_header" href="<?php echo $products[1]['href']; ?>"><?php echo $products[1]['author']; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="fitem">
            <div class="fcontainer">
                <div class="fitemsub">
                    <a href="<?php echo $products[2]['href']; ?>">
                    <img src="<?php echo $products[2]['thumb']; ?>" alt="<?php echo $products[2]['name']; ?>" title="<?php echo $products[2]['name']; ?>" />
                    </a>
                </div>
                <div class="fitemsub">
                    <div class="textoLibros">
                        <h4 class="sp_header">
                            <a href="<?php echo $products[2]['href']; ?>"><?php echo $products[2]['headerName']; ?></a>
                        </h4>
                        <p class="autor_header" href="<?php echo $products[2]['href']; ?>"><?php echo $products[2]['author']; ?></p>

                    </div>
                </div>
            </div>
        </div>
        <div class="fitemBoton">
            <a href="<?php echo $linkFavoritos; ?>">
                <div class="fitemBotonsub">
                    <div class="estrellas1">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </div>

                    <span>
                        <p>Ver todos</p>
                    </span>
                    <div class="estrellas2">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </div>

                </div>
            </a>
        </div>

    </div>

</div>
