<div class="recomendaciones">
    <div class="stripedHeader">
        <h2 class="stripes"><?php echo $heading_title; ?></h2>
    </div>

    <?php if ($products) { ?>
    <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-thumb">
                <div class="sp_titulo">
                    <h4 class="sp_header">
                        <a href="<?php echo $product['href']; ?>"><?php echo $product['headerName']; ?></a>
                    </h4>
                </div>
                <div class="autor_titulo">
                    <p class="autor_header" href="<?php echo $product['href']; ?>"><?php echo $product['author']; ?></p>
                </div>

                <div class="image" >
                    <a href="<?php echo $product['href']; ?>">
                        <img class="img-responsive cropped" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                    </a>
                </div>

                <div>

                    <div class="button-group">
                        <button class="buttonAzul btnPrecio disabled">
                            <span style="color:#eee;" class="btnPrecio"><?php echo $product['precioFinal']; ?></span>
                        </button>
                        <button class="buttonAzul" type="button" data-toggle="tooltip"
                                title="<?php echo $button_wishlist; ?>"
                                onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                                    class="fa fa-heart" style="color:#eee;"></i></button>
                        <button class="buttonAzul" type="button"
                                onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                                    class="fa fa-shopping-cart" style="color:#eee;"></i>
                            <!--  <span class="hidden-xs hidden-sm hidden-md carro" style="color:#eee;"><?php echo $button_cart; ?></span>-->
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-7 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-5 text-right"><?php echo $results; ?></div>
    </div>
</div>

