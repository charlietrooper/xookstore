<div class="favoritos">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-9">
            <div id="owl_new<?php echo 15; ?>">

                <?php foreach ($products as $product) { ?>
                <div class="item product-layout ">
                    <div class="product-thumb">
                        <div class="image"><a href="<?php echo $product['href']; ?>">
                        <span class="product-image">
                            <img class="img-responsive cropped" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                        </span>
                            </a>

                        </div>
                    </div>
                    <h4 class="sp_header">
                        <a href="<?php echo $product['href']; ?>"><?php echo $product['headerName']; ?></a>
                    </h4>
                    <p class="autor_header" href="<?php echo $product['href']; ?>"><?php echo $product['author']; ?></p>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>





    <script type="text/javascript">
        $('#owl_new<?php echo 15; ?>').owlCarousel({
            items: 3,
            autoPlay: <?php echo $webocreation_autoplay_speed_status; ?>,
        autoplayTimeout:<?php echo $webocreation_autoplay_speed; ?>,
        stopOnHover: true,
                pagination: false,
                navigation: true,
                navigationText: ['<i class="fa fa-arrow-left fa-1x"></i>', '<i class="fa fa-arrow-right fa-1x"></i>'],
                pagination: true,
                rewindNav: true,
                scrollPerPage: false
        });
    </script>
</div>

