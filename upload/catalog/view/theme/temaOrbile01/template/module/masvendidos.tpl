
<div class="masvendidos">
    <!--<h3><?php echo $heading_title; ?></h3>-->
    <div class="wrap">
        <div class="headerSliderMV">
            <p><span>B</span><b>e</b><span>st s</span><b>e</b><span>llers</span></p>
        </div>
    </div>


    <div id="owl_new<?php echo 10 ?>">

        <?php foreach ($products as $product) { ?>
        <div class="item product-layout ">
            <div class="sp_titulo">
                <h4 class="sp_header">
                    <a href="<?php echo $product['href']; ?>"><?php echo $product['headerName']; ?></a>
                </h4>
            </div>
            <div class="autor_titulo">

                <p class="autor_header" href="<?php echo $product['href']; ?>"><?php echo $product['author']; ?></p>

            </div>
            <div class="product-thumb">

                <div class="image"><a href="<?php echo $product['href']; ?>">
                    <span class="product-image">
                        <img class="img-responsive cropped" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                    </span>

                    </a>

                </div>

                <div class="button-group">
                    <button class="buttonMV btnPrecio disabled">
                        <span style="color:#eee;" class="btnPrecio"><?php echo $product['precioFinal']; ?></span>
                    </button>
                    <button class="buttonMV" type="button" data-toggle="tooltip"
                            title="<?php echo $button_wishlist; ?>"
                            onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                                class="fa fa-heart" style="color:#eee;"></i></button>
                    <button class="buttonMV" type="button"
                            onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                                class="fa fa-shopping-cart" style="color:#eee;"></i>
                        <!--  <span class="hidden-xs hidden-sm hidden-md carro" style="color:#eee;"><?php echo $button_cart; ?></span>-->
                    </button>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>



    <script type="text/javascript">
        $('#owl_new<?php echo 10; ?>').owlCarousel({
            items: 4,
            autoPlay: <?php echo $masvendidos_autoplay_speed_status; ?>,
        autoplayTimeout:<?php echo $masvendidos_autoplay_speed; ?>,
        stopOnHover: true,
                pagination: false,
                navigation: true,
                navigationText: ['<i class="fa fa-arrow-left fa-1x"></i>', '<i class="fa fa-arrow-right fa-1x"></i>'],
                pagination: true,
                rewindNav: true,
                scrollPerPage: false
        });
    </script>
</div>
