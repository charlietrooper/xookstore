<div class="webocreation">
    <!--<h3><?php echo $heading_title; ?></h3>-->
   <div class="wrap">
       <div class="headerSlider">
           <p><span>Nov</span><b>e</b><span>dades</span></p>
       </div>
   </div>


    <div id="owl_new<?php echo $module; ?>">

        <?php foreach ($products as $product) { ?>
        <div class="item product-layout ">
            <?php
            if(strlen($product['name'])>60)
            {
                $temp = substr($product['name'],0,60);
                $headerNombre=$temp.'...';
            }
            else{
              $headerNombre=$product['name'];
            }

            ?>
            <div class="sp_titulo">
                <h4 class="sp_header">
                    <a href="<?php echo $product['href']; ?>"><?php echo $headerNombre; ?></a>
                </h4>
            </div>
            <div class="product-thumb">

                <div class="image"><a href="<?php echo $product['href']; ?>">
                                            <span class="product-image">
                                            <img
                                                    src="<?php echo $product['thumb']; ?>"
                                                    alt="<?php echo $product['name']; ?>"
                                                    title="<?php echo $product['name']; ?>" class="img-responsive"/>
                                            </span>
                        <!--  <span class="product-image-hover">
                              <img
                                      src="<?php echo $product['additional_image']; ?>"
                                      alt="<?php echo $product['name']; ?>"
                                      title="<?php echo $product['name']; ?>" class="img-responsive"/>
                          </span>-->


                    </a>

                </div>

                <?php if ($product['price'])
                                         { 
                                            $precioReal=0;
                                     
                                           if (!$product['special']) 
                                             { 
                                                $precioReal=$product['price'];
                                             }
                                             else 
                                             {
                                                $precioReal=$product['special'];
                                             } 
                                         }
                                    ?>

                <div class="button-group">
                    <button class="button btnPrecio disabled">
                        <span style="color:#eee;" class="btnPrecio"><?php echo $precioReal; ?></span>
                    </button>
                    <button class="button" type="button" data-toggle="tooltip"
                            title="<?php echo $button_wishlist; ?>"
                            onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                                class="fa fa-heart" style="color:#eee;"></i></button>
                    <button class="button" type="button"
                            onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                                class="fa fa-shopping-cart" style="color:#eee;"></i>
                      <!--  <span class="hidden-xs hidden-sm hidden-md carro" style="color:#eee;"><?php echo $button_cart; ?></span>-->
                    </button>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>



    <script type="text/javascript">
        $('#owl_new<?php echo $module; ?>').owlCarousel({
            items: 3,
            autoPlay: <?php echo $webocreation_autoplay_speed_status; ?>,
        autoplayTimeout:<?php echo $webocreation_autoplay_speed; ?>,
        stopOnHover: true,
                pagination: false,
                navigation: true,
                navigationText: ['<i class="fa fa-arrow-left fa-1x"></i>', '<i class="fa fa-arrow-right fa-1x"></i>'],
                pagination: true,
                rewindNav: true,
                scrollPerPage: false
        });
    </script>
</div>

