<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <!--Popup al agregar producto al carrito-->
    <?php echo $popup_code ?>
    <!--Termina Popup al agregar producto al carrito-->

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="row">
                <?php if ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } else { ?>
                <?php $class = 'col-sm-8'; ?>
                <?php } ?>
                <div class="col-md-9">
                    <div class="flex">
                        <div class="left">
                            <?php if($thumbHeight > 270) { ?>
                            <img class="book" height="260px" src="<?php echo $thumb; ?>"  alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" />
                            <?php } else { ?>
                            <img class="book" height="260px" src="<?php echo $thumb; ?>"  alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" />

                            <?php }  ?>
                            <div class="thumbnails">
                                <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                    <img class="kobo" src="catalog/view/theme/temaOrbile01/image/Kobo_PNG.png" />
                                </a>
                            </div>
                        </div>
                        <div class="right">
                            <div class="datos">
                                <span id="responsive_headline" class="titulo"><?php echo $heading_title; ?></span>
                                <span id="responsive_headline" class="autor"><?php echo $autor; ?></span>
                            </div>
                            <p class="descripcion" id="responsive_headline"><?php echo $descriptionPrint[0]; ?>
                                <?php if(substr($descriptionPrint[0], -3)=='...') { ?>
                                  <a href="#readMore">Leer más</a></p>
                                <?php } ?>
                            <div class="stuff">
                                <button disabled><b><?php echo $preciofinal; ?></b></button>
                                <button id="wish" onclick="wishlist.add('<?php echo $product_id; ?>');"><i
                                            class="fa fa-heart" style="color:#eee;"></i></button>
                                <button type="button"
                                        onclick="cart.add('<?php echo $product_id; ?>');"><i
                                            class="fa fa-shopping-cart" style="color:#eee;"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="infoAdicional">
                        <h3>Detalles</h3>
                        <!--<p><strong><?php echo $text_imprint; ?> </strong> <?php echo $imprint; ?></p>-->
                        <!--<p><strong><?php echo $text_imprint; ?> </strong> <?php echo $imprint; ?></p>-->
                        <p><strong><?php echo $text_publisher; ?> </strong> <?php echo $publisher; ?></p>
                        <p><strong><?php echo $text_isbn; ?> </strong> <?php echo $isbn; ?></p>
                        <p><strong><?php echo $text_idioma; ?> </strong> <?php echo $language; ?></p>
                        <strong><?php echo $text_opcDescarga; ?></strong>
                        <p><?php echo $readThisOn; ?></p>
                        <strong><?php echo $text_forDescarga; ?></strong>
                        <p><?php echo $formatos; ?></p>
                    </div>

                </div>
            </div>

            <div class="row relatedDiv">

                <div class="relatedHeader">
                        <h2 class="stripes">Títulos similares</h2>
                </div>
                <!--
                <div class="wrap">
                    <div class="r1"><p></p></div>
                    <div class="r2"> <h2>Títulos similares</h2></div>
                    <div class="r3"><p></p></div>
                </div>-->

                <?php foreach ($relatedProducts as $related) { ?>
                <div class="col-md-3 col-xs-12">
                    <div class="product-thumb">
                        <div class="sp_titulo">
                            <h4 class="sp_header">
                                <a href="<?php echo $related['href']; ?>"><?php echo $related['nombreReducido']; ?></a>
                            </h4>
                        </div>
                        <div class="autor_titulo">

                            <p class="autor_header" href="<?php echo $related['href']; ?>"><?php echo $related['author']; ?></p>

                        </div>

                        <div class="image" >
                            <a href="<?php echo $related['href']; ?>">
                                <img class="img-responsive cropped" src="<?php echo $related['thumb']; ?>" alt="<?php echo $related['name']; ?>" title="<?php echo $related['name']; ?>" />
                            </a>
                        </div>

                        <div>
                            <div class="button-group">
                                <button class="buttonProduct btnPrecio disabled">
                                    <span style="color:#eee;" class="btnPrecio"><?php echo $related['precioFinal']; ?></span>
                                </button>
                                <button class="buttonProduct" type="button" data-toggle="tooltip"
                                        title="<?php echo $button_wishlist; ?>"
                                        onclick="wishlist.add('<?php echo $related['product_id']; ?>');"><i
                                            class="fa fa-heart" style="color:#eee;"></i></button>
                                <button class="buttonProduct" type="button"
                                        onclick="cart.add('<?php echo $related['product_id']; ?>');"><i
                                            class="fa fa-shopping-cart" style="color:#eee;"></i>
                                    <!--  <span class="hidden-xs hidden-sm hidden-md carro" style="color:#eee;"><?php echo $button_cart; ?></span>-->
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div id="readMore">
                <div class="row">
                    <div class="descriptionHeader">
                        <h2 class="stripes">Descripción del libro</h2>
                    </div>
                    <div class="descriptionDiv">
                        <p><?php echo $descriptionFull; ?></p>
                    </div>

                </div>
            </div>


            <?php if ($products) { ?>
            <h3><?php echo $text_related; ?></h3>

            <div class="row">
                <?php $i = 0; ?>
                <?php foreach ($products as $product) { ?>
                <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
                <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
                <?php } else { ?>
                <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">
                    <div class="product-thumb transition">
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img
                                        src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                        title="<?php echo $product['name']; ?>" class="img-responsive"/></a></div>
                        <div class="caption">
                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>

                            <p><?php echo $product['description']; ?></p>
                            <?php if ($product['rating']) { ?>
                            <div class="rating">
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                <?php } else { ?>
                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i
                                            class="fa fa-star-o fa-stack-1x"></i></span>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <?php if ($product['price']) { ?>
                            <p class="price">
                                <?php if (!$product['special']) { ?>
                                <?php echo $product['price']; ?>
                                <?php } else { ?>
                                <span class="price-new"><?php echo $product['special']; ?></span> <span
                                        class="price-old"><?php echo $product['price']; ?></span>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                <?php } ?>
                            </p>
                            <?php } ?>
                        </div>
                        <div class="button-group">
                            <button type="button"
                                    onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
                                <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i
                                        class="fa fa-shopping-cart"></i></button>
                            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>"
                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                                        class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>"
                                    onclick="compare.add('<?php echo $product['product_id']; ?>');"><i
                                        class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
                <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
                <div class="clearfix visible-md visible-sm"></div>
                <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
                <div class="clearfix visible-md"></div>
                <?php } elseif ($i % 4 == 0) { ?>
                <div class="clearfix visible-md"></div>
                <?php } ?>
                <?php $i++; ?>
                <?php } ?>
            </div>
            <?php } ?>
            <?php if ($tags) { ?>
            <p><?php echo $text_tags; ?>
                <?php for ($i = 0; $i < count($tags); $i++) { ?>
                <?php if ($i < (count($tags) - 1)) { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                <?php } else { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                <?php } ?>
                <?php } ?>
            </p>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript">
    $(document).ready(function(){
        //PARTE PARA OCULTAR O MOSTRAR EL JSON
        $('#ocultar').click(function(){
            $('#mostrarReadThis').hide();
            $('#mostrarContributors').hide();
            $('#mostrarImprint').hide();
            $('#mostrarIsKids').hide();
            $('#mostrarHasPreview').hide();
            $('#mostrarSubtitle').hide();
            $('#mostrarIsAdultMaterial').hide();
            $('#ocultar').hide();
        });
        $('#verReadThis').click(function(){
            $('#mostrarReadThis').show();
            $('#mostrarContributors').hide();
            $('#mostrarImprint').hide();
            $('#mostrarIsKids').hide();
            $('#mostrarHasPreview').hide();
            $('#mostrarSubtitle').hide();
            $('#mostrarIsAdultMaterial').hide();
            $('#ocultar').show();
        });
        $('#verContributors').click(function(){
            $('#mostrarReadThis').hide();
            $('#mostrarContributors').show();
            $('#mostrarImprint').hide();
            $('#mostrarIsKids').hide();
            $('#mostrarHasPreview').hide();
            $('#mostrarSubtitle').hide();
            $('#mostrarIsAdultMaterial').hide();
            $('#ocultar').show();
        });
        $('#verImprint').click(function(){
            $('#mostrarReadThis').hide();
            $('#mostrarContributors').hide();
            $('#mostrarImprint').show();
            $('#mostrarIsKids').hide();
            $('#mostrarHasPreview').hide();
            $('#mostrarSubtitle').hide();
            $('#mostrarIsAdultMaterial').hide();
            $('#ocultar').show();
        });
        $('#verIsKids').click(function(){
            $('#mostrarReadThis').hide();
            $('#mostrarContributors').hide();
            $('#mostrarImprint').hide();
            $('#mostrarIsKids').show();
            $('#mostrarHasPreview').hide();
            $('#mostrarSubtitle').hide();
            $('#mostrarIsAdultMaterial').hide();
            $('#ocultar').show();
        });
        $('#verHasPreview').click(function(){
            $('#mostrarReadThis').hide();
            $('#mostrarContributors').hide();
            $('#mostrarImprint').hide();
            $('#mostrarIsKids').hide();
            $('#mostrarHasPreview').show();
            $('#mostrarSubtitle').hide();
            $('#mostrarIsAdultMaterial').hide();
            $('#ocultar').show();
        });
        $('#verSubtitle').click(function(){
            $('#mostrarReadThis').hide();
            $('#mostrarContributors').hide();
            $('#mostrarImprint').hide();
            $('#mostrarIsKids').hide();
            $('#mostrarHasPreview').hide();
            $('#mostrarSubtitle').show();
            $('#mostrarIsAdultMaterial').hide();
            $('#ocultar').show();
        });
        $('#verIsAdultMaterial').click(function(){
            $('#mostrarReadThis').hide();
            $('#mostrarContributors').hide();
            $('#mostrarImprint').hide();
            $('#mostrarIsKids').hide();
            $('#mostrarHasPreview').hide();
            $('#mostrarSubtitle').hide();
            $('#mostrarIsAdultMaterial').show();
            $('#ocultar').show();
        });
    });
</script>

<script type="text/javascript"><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }
                    if (json['error']['repeated']) {
                        $('.breadcrumb').after('<div class="alert alert-danger">' + json['error']['repeated'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });
    //--></script>
<?php echo $footer; ?>
