<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
    <!--Popup al agregar producto al carrito-->
    <?php echo $popup_code ?>
    <!--Termina Popup al agregar producto al carrito-->
    <div class="stripedHeader">
        <h2 class="stripes"><?php echo $heading_title; ?></h2>
    </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } ?>
      <div class="col-sm-3" id="filtros">
          <p class="headerFiltro">Filtrar búsqueda</p>
          <hr>
          <div class="filtrosBusqueda">
              <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
              <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
          </div>
          <div class="filtrosBusqueda">
              <label class="control-label" for="searchCat"><?php echo $entry_Category; ?></label>
              <select name="searchCat" class="form-control">
                  <option value="ALL"><?php echo $text_category; ?></option>
                  <?php foreach ($categories as $category_1) { ?>
                  <?php foreach ($category_1['children'] as $category_2) { ?>
                  <?php if ($category_2['name'] == $searchCat) { ?>
                  <option value="<?php echo $category_2['name']; ?>" selected="selected"><?php echo $category_2['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $category_2['name']; ?>"><?php echo $category_2['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                  <?php } ?>
              </select>
          </div>
          <div class="filtrosBusqueda">
              <label class="control-label" for="input-searchText"><?php echo $entry_searchText; ?></label>
              <select name="searchText" id="input-searchText" class="form-control">
                  <?php foreach ($searchTextOptions as $st) { ?>
                  <?php if ($st['searchText_id'] == $searchText) { ?>
                  <option value="<?php echo $st['searchText_id']; ?>" selected="selected"><?php echo $st['searchText_name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $st['searchText_id']; ?>"><?php echo $st['searchText_name']; ?></option>
                  <?php } ?>
                  <?php } ?>
              </select>
          </div>
          <div class="filtrosBusqueda">
               <div class="filtrosBusqueda">
                   <label class="control-label" for="input-price"><?php echo $entry_priceRange; ?></label>

                   <div class="rangeSlider">
                       <input type="text" id="range" value="" name="range" />
                   </div>
               </div>
</div>
<div class="filtrosBusqueda">
   <label class="control-label" for="input-language"><?php echo $entry_language; ?></label>
   <select name="language" id="input-language" class="form-control">
       <?php foreach ($languageOptions as $lang) { ?>
       <?php if ($lang['language_id'] == $language) { ?>
       <option value="<?php echo $lang['language_id']; ?>" selected="selected"><?php echo $lang['language_name']; ?></option>
       <?php } else { ?>
       <option value="<?php echo $lang['language_id']; ?>"><?php echo $lang['language_name']; ?></option>
       <?php } ?>
       <?php } ?>
   </select>
</div>
<div class="filtrosBusqueda">
   <label class="control-label" for="input-available"><?php echo $entry_availability; ?></label>
   <select name="available" id="input-available" class="form-control">
       <?php foreach ($availableOptions as $ava) { ?>
       <?php if ($ava['available_id'] == $available) { ?>
       <option value="<?php echo $ava['available_id']; ?>" selected="selected"><?php echo $ava['available_name']; ?></option>
       <?php } else { ?>
       <option value="<?php echo $ava['available_id']; ?>"><?php echo $ava['available_name']; ?></option>
       <?php } ?>
       <?php } ?>
   </select>
</div>

<div class="filtrosBusqueda">
   <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
</div>

</div>
<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
<div class="row">
<div class="row">
   <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
   <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
<h2><?php echo $text_search; ?></h2>
<?php if ($products) { ?>
<!--<p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>-->
      <div class="row">

          <!--
        <div class="col-sm-3 hidden-xs">
          <div class="btn-group">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>-->
        <div class="col-sm-2 text-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="col-sm-3 text-right">
          <select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-1 text-right">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-sm-2 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>
      <br />
      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="product-thumb">
              <div class="sp_titulo">
                  <h4 class="sp_header">
                      <a href="<?php echo $product['href']; ?>"><?php echo $product['nombreReducido']; ?></a>
                  </h4>
              </div>
              <div class="autor_titulo">
                  <p class="autor_header" href="<?php echo $product['href']; ?>"><?php echo $product['author']; ?></p>
              </div>
              <div class="image" >
                  <a href="<?php echo $product['href']; ?>">
                      <img class="img-responsive cropped" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                  </a>
              </div>

              <div>
                  <div class="button-group">
                      <button class="buttonAzul btnPrecio disabled">
                          <span style="color:#eee;" class="btnPrecio"><?php echo $product['precioReal']; ?></span>
                      </button>
                      <button class="buttonAzul" type="button" data-toggle="tooltip"
                              title="<?php echo $button_wishlist; ?>"
                              onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                                  class="fa fa-heart" style="color:#eee;"></i></button>
                      <button class="buttonAzul" type="button"
                              onclick="cart.add('<?php echo $product['model']; ?>');"><i
                                  class="fa fa-shopping-cart" style="color:#eee;"></i>
                          <!--  <span class="hidden-xs hidden-sm hidden-md carro" style="color:#eee;"><?php echo $button_cart; ?></span>-->
                      </button>
                  </div>

              </div>
          </div>
        </div>
        <?php } ?>
      </div>

      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
    </div>
</div>
<script type="text/javascript"><!--
    $(function () {

        var initialRange="<?php echo $initialRange ?>";
        var finalRange="<?php echo $finalRange ?>";
        if(initialRange=='ALL')
        {
            initialRange = 300;
        }

        if(finalRange=='ALL')
        {
            finalRange = 800;
        }



        $("#range").ionRangeSlider({
            type: "double",
            min: 0,
            max: 2000,
            from: initialRange,
            to: finalRange,
            prefix: '$',
            max_postfix: '+'
        });

    });


$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#filtros input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

    var searchText = $('#filtros select[name=\'searchText\']').prop('value');

    if (searchText) {
        url += '&searchText=' + encodeURIComponent(searchText);
    }

    var searchCat = $('#filtros select[name=\'searchCat\']').prop('value');

    if (searchCat) {
        url += '&searchCat=' + encodeURIComponent(searchCat);
    }

    var language = $('#filtros select[name=\'language\']').prop('value');

    if (language && language!=='all') {
        url += '&language=' + encodeURIComponent(language);
    }

    var priceRange = $("#range").data("ionRangeSlider");
    if (priceRange) {
        url += '&initialRange=' + encodeURIComponent(priceRange.result.from);
        url += '&finalRange=' + encodeURIComponent(priceRange.result.to);
    }

    var available = $('#filtros select[name=\'available\']').prop('value');

    if (available && available!=='all') {
        url += '&available=' + encodeURIComponent(available);
    }

	var sub_category = $('#filtros input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#filtros input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});
/*
$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});*/

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>