<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <!--Popup al agregar producto al carrito-->
    <?php echo $popup_code ?>
    <!--Termina Popup al agregar producto al carrito-->
    <div class="row">
        <div id="content"><?php echo $content_top; ?>

        <?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $footer; ?>