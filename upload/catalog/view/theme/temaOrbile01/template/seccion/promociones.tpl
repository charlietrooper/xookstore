
<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="stripedHeader">
                <h2 class="stripes"><?php echo $heading_title; ?></h2>
            </div>
            <div class="accResponsive">
                <div class="accordian_<?php echo $promotionsQuantity; ?>">
                    <ul>
                        <?php foreach($banners as $banner) { ?>
                            <li>
                                 <div class="image_title">
                                    <a href="<?php echo $banner['image']; ?>"><?php echo $banner['title']; ?></a>
                                </div>
                              <!--  <a href="<?php echo $banner['link']; ?>" data-smoothzoom="group1">
                                    <img src="<?php echo $banner['image']; ?>"/>
                                </a>-->
                                <a href="<?php echo $banner['link']; ?>">
                                    <img src="<?php echo $banner['image']; ?>"/>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

            </div>

            <?php echo $column_right; ?></div>
    </div>
</div>
<script src="catalog/view/javascript/smoothzoom/jquery.js" type="text/javascript"></script>
<script src="catalog/view/javascript/smoothzoom/easing.js" type="text/javascript"></script>
<script src="catalog/view/javascript/smoothzoom/smoothzoom.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(window).load( function() {
        $('img').smoothZoom({
            // Options go here
        });
    });
</script>
<?php echo $footer; ?>