<?php
// HTTP
define('HTTP_SERVER', 'http://store.xooklocal.com/xookpanel16/');
define('HTTP_CATALOG', 'http://store.xooklocal.com/');

// HTTPS
define('HTTPS_SERVER', 'http://store.xooklocal.com/xookpanel16/');
define('HTTPS_CATALOG', 'http://store.xooklocal.com/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/XookStore/upload/xookpanel16/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/XookStore/upload/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/XookStore/upload/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/XookStore/upload/xookpanel16/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/XookStore/upload/xookpanel16/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/XookStore/upload/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/XookStore/upload/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/XookStore/upload/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/XookStore/upload/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/XookStore/upload/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/XookStore/upload/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/XookStore/upload/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'xookstore');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
