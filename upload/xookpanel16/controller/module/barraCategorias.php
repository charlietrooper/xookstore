<?php
class ControllerModuleBarraCategorias extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/barraCategorias');
       // $this->document->addStyle('xookpanel16/view/stylesheet/module/promotions.css');


        $this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('barraCategorias', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], true));
		}

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategoriesEmpty(0);

        foreach ($categories as $category) {
            $children_data = array();


                $children = $this->model_catalog_category->getCategoriesEmpty($category['category_id']);

                foreach($children as $child) {
                    $filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }


            $filter_data = array(
                'filter_category_id'  => $category['category_id'],
                'filter_sub_category' => true
            );

            $data['allCategories'][] = array(
                'category_id' => $category['category_id'],
                'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                'children'    => $children_data,
                'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
            );
        }

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_model'] = $this->language->get('text_entry_model');
		$data['text_isbn'] = $this->language->get('text_isbn');
		$data['header_add'] = $this->language->get('header_add');
		$data['text_paginas'] = $this->language->get('text_paginas');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_container'] = $this->language->get('entry_container');
		$data['entry_categories'] = $this->language->get('entry_categories');
		$data['entry_product'] = $this->language->get('entry_product');
        $data['entry_color'] = $this->language->get('entry_color');
        $data['entry_texto'] = $this->language->get('entry_texto');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_autoplay'] = $this->language->get('entry_autoplay');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['help_product'] = $this->language->get('help_product');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

        //Array con las opciones de colores
        $data['colors'] = ['Rojo','Azul','Verde','Morado', 'Gris'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

        if (isset($this->error['text1'])) {
            $data['error_text1'] = $this->error['text1'];
        } else {
            $data['error_text1'] = '';
        }

        if (isset($this->error['text2'])) {
            $data['error_text2'] = $this->error['text2'];
        } else {
            $data['error_text2'] = '';
        }

        if (isset($this->error['text3'])) {
            $data['error_text3'] = $this->error['text3'];
        } else {
            $data['error_text3'] = '';
        }

        if (isset($this->error['text4'])) {
            $data['error_text4'] = $this->error['text4'];
        } else {
            $data['error_text4'] = '';
        }


        $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/barraCategorias', 'token=' . $this->session->data['token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/barraCategorias', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true)
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('module/barraCategorias', 'token=' . $this->session->data['token'], true);
		} else {
			$data['action'] = $this->url->link('module/barraCategorias', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true);
		}

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true);

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

        if (isset($this->request->post['categories1'])) {
            $data['categories1'] = $this->request->post['categories1'];
        } elseif (!empty($module_info)) {
            $data['categories1'] = $module_info['categories1'];
        } else {
            $data['categories1'] = '';
        }

        if (isset($this->request->post['categories2'])) {
            $data['categories2'] = $this->request->post['categories2'];
        } elseif (!empty($module_info)) {
            $data['categories2'] = $module_info['categories2'];
        } else {
            $data['categories2'] = '';
        }

        if (isset($this->request->post['categories3'])) {
            $data['categories3'] = $this->request->post['categories3'];
        } elseif (!empty($module_info)) {
            $data['categories3'] = $module_info['categories3'];
        } else {
            $data['categories3'] = '';
        }

        if (isset($this->request->post['categories4'])) {
            $data['categories4'] = $this->request->post['categories4'];
        } elseif (!empty($module_info)) {
            $data['categories4'] = $module_info['categories4'];
        } else {
            $data['categories4'] = '';
        }

        if (isset($this->request->post['color1'])) {
            $data['color1'] = $this->request->post['color1'];
        } elseif (!empty($module_info)) {
            $data['color1'] = $module_info['color1'];
        } else {
            $data['color1'] = '';
        }

        if (isset($this->request->post['color2'])) {
            $data['color2'] = $this->request->post['color2'];
        } elseif (!empty($module_info)) {
            $data['color2'] = $module_info['color2'];
        } else {
            $data['color2'] = '';
        }

        if (isset($this->request->post['color3'])) {
            $data['color3'] = $this->request->post['color3'];
        } elseif (!empty($module_info)) {
            $data['color3'] = $module_info['color3'];
        } else {
            $data['color3'] = '';
        }

        if (isset($this->request->post['color4'])) {
            $data['color4'] = $this->request->post['color4'];
        } elseif (!empty($module_info)) {
            $data['color4'] = $module_info['color4'];
        } else {
            $data['color4'] = '';
        }

        if (isset($this->request->post['alias1'])) {
            $data['alias1'] = $this->request->post['alias1'];
        } elseif (!empty($module_info)) {
            $data['alias1'] = $module_info['alias1'];
        } else {
            $data['alias1'] = '';
        }

        if (isset($this->request->post['alias2'])) {
            $data['alias2'] = $this->request->post['alias2'];
        } elseif (!empty($module_info)) {
            $data['alias2'] = $module_info['alias2'];
        } else {
            $data['alias2'] = '';
        }

        if (isset($this->request->post['alias3'])) {
            $data['alias3'] = $this->request->post['alias3'];
        } elseif (!empty($module_info)) {
            $data['alias3'] = $module_info['alias3'];
        } else {
            $data['alias3'] = '';
        }

        if (isset($this->request->post['alias4'])) {
            $data['alias4'] = $this->request->post['alias4'];
        } elseif (!empty($module_info)) {
            $data['alias4'] = $module_info['alias4'];
        } else {
            $data['alias4'] = '';
        }

		$this->load->model('catalog/product');

		$data['products'] = array();

		if (!empty($this->request->post['product'])) {
			$products = $this->request->post['product'];
		} elseif (!empty($module_info['product'])) {
			$products = $module_info['product'];
		} else {
			$products = array();
		}

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				$data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'name'       => $product_info['name']
				);
			}
		}


		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/barraCategorias', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/barraCategorias')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

        if ((utf8_strlen($this->request->post['alias1']) < 3) || (utf8_strlen($this->request->post['alias1']) > 12)) {
            $this->error['text1'] = $this->language->get('error_text');
        }

        if ((utf8_strlen($this->request->post['alias2']) < 3) || (utf8_strlen($this->request->post['alias2']) > 12)) {
            $this->error['text2'] = $this->language->get('error_text');
        }

        if ((utf8_strlen($this->request->post['alias3']) < 3) || (utf8_strlen($this->request->post['alias3']) > 12)) {
            $this->error['text3'] = $this->language->get('error_text');
        }

        if ((utf8_strlen($this->request->post['alias4']) < 3) || (utf8_strlen($this->request->post['alias4']) > 12)) {
            $this->error['text4'] = $this->language->get('error_text');
        }
		return !$this->error;
	}
}
