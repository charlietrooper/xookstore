<?php
class ControllerScriptUpdate extends Controller {
    private $error = array();
    public function index() {
        //Se determina la fecha para nombrar el archivo de log
        $fecha_log = date('Y-m-d');
        $logger = new Log("update/update$fecha_log.log");
        //Se declara un tiempo indeterminado de espera
        set_time_limit(0);
        $data['agregados'] = 0;
        $data['actualizados'] = 0;
        $data['desactivados'] = 0;
        $fechaLimite = date('Y-m-d', strtotime('-1 month'));
        //Se consulta la tabla de las actualizaciones
        $query = $this->db->query("SELECT nombre_archivo AS archivo FROM " . DB_PREFIX . "actualizacion_libros WHERE fecha_actualizacion >='" . $fechaLimite . "'");
        $archBaseDatos = array();
        //Se obtienen los archivos disponibles en la base de datos
        foreach($query->rows as $q) {
            $archBaseDatos[] = $q['archivo'];
        }
        $logger->write('Archivos registrados en la base de datos: ' . json_encode($archBaseDatos));
        //Se define el directorio en donde se buscarán los archivos par aactualizar la base de datos
        $directorio = DIR_APPLICATION . 'controller/script';
        //Se escánea la carpeta
        $archivosCarpeta = scandir($directorio);
        $logger->write('Archivos encontrados en la carpeta "upload/admin/controller/script": ' . json_encode($archivosCarpeta));
        //Se obtienen los archivos que no se han usado para actualizar la base de datos
        $archivos = array();
        foreach($archivosCarpeta as $arch) {
            if(!in_array($arch, $archBaseDatos)) {
                $archivos[] = $arch ;
            }
        }
        $logger->write('Archivos que se usarán para actualizar los productos: ' . json_encode($archivos));
        //Si hay archivos nuevos, se comienza a ejecutar la examinación del(os) archivos para la adición de libros
        if(count($archivos) > 0) {
            foreach($archivos as $a) {
                $logger->write('Archivo a iniciar actualización: ' . json_encode($a));
                $hora_inicio = date('H:i:s');
                $archivo = DIR_APPLICATION . 'controller/script/' . $a;
                $fecha_actualizacion = date('Y-m-d');
                $archivoAbierto = fopen($archivo,"r") or exit ("Unable to open file!");
                if($archivoAbierto){
                    $lineas = 0;
                    $actualizados = 0;
                    $agregados = 0;
                    $desactivados= 0;
                    while(!feof($archivoAbierto)){
                        $linea4 = json_decode(fgets($archivoAbierto), true);
                        if(isset($linea4['activeRevision'])) {
                            $id_ =  $linea4['@attributes']['id'];
                            $isbn = $linea4['isbn'];
                            $ActiveRevisionID = $linea4['activeRevision']['@attributes']['id'];
                            $titulo = $linea4['activeRevision']['title'];
                            $urlImagen = $linea4['activeRevision']['links']['coverImage'];
                            $itemPage = $linea4['activeRevision']['links']['itemPage'];
                            $seoUrl = substr(strrchr($itemPage, '/'), 1);
                            $this->load->model('catalog/update');
                            //S obtiene el precio
                            $precio = $this->model_catalog_update->obtenerPrecio($linea4['activeRevision']['prices']['price']);
                            //Se obtiene el género
                            if(isset($linea4['activeRevision']['categories']['category'])) {
                                $genero = $this->model_catalog_update->obtenerGenero($linea4['activeRevision']['categories']['category']);
                            } else {
                                $genero = 144;
                            }
                            if (is_array($linea4['activeRevision']['contibutors']))
                            {
                                $author=$this->obtenerAutor($linea4['activeRevision']['contibutors']);
                            }
                            $producto = array(
                                'product_description' => array(
                                    1 => array(
                                        'name'              => $titulo,
                                        'description'       => '',
                                        'meta_title'        => $titulo,
                                        'meta_description'  => '',
                                        'meta_keyword'      => '',
                                        'tag'               => ''
                                    ),
                                    2 => array(
                                        'name'              => $titulo,
                                        'description'       => '',
                                        'meta_title'        => $titulo,
                                        'meta_description'  => '',
                                        'meta_keyword'      => '',
                                        'tag'               => ''
                                    )
                                ),
                                'image'             => $urlImagen,
                                'model'             => $ActiveRevisionID,
                                'sku'               => $id_,
                                'upc'               => '',
                                'ean'               => $ActiveRevisionID,
                                'jan'               => '',
                                'isbn'              => $isbn,
                                'author'            => $author,
                                'mpn'               => '',
                                'location'          => 'locationPrueba',
                                'price'             => $precio,
                                'tax_class_id'      => '0',
                                'quantity'          => '999',
                                'minimum'           => '1',
                                'subtract'          => '0',
                                'stock_status_id'   => '7',
                                'shipping'          => '0',
                                'keyword'           => $seoUrl,
                                'date_available'    => '',
                                'length'            => '',
                                'width'             => '',
                                'height'            => '',
                                'length_class_id'   => '1',
                                'weight'            => '',
                                'weight_class_id'   => '1',
                                'status'            => '1',
                                'sort_order'        => '1',
                                'manufacturer'      => '',
                                'manufacturer_id'   => '0',
                                'category'          => '',
                                'product_category' => array (
                                    0 =>  $genero
                                ),
                                'filter'            => '',
                                'product_store'     => array (
                                    0 => '0'
                                ),
                                'download'          => '',
                                'related'           => '',
                                'option'            => '',
                                'points'            => '',
                                'product_reward'    => array(
                                    1 => array(
                                        'points' => ''
                                    )
                                ),
                                'product_layout'    => array(
                                    0 => ''
                                )
                            );
                            $this->load->model('catalog/product');
                            $query = $this->db->query("SELECT product_id AS product_id FROM " . DB_PREFIX . "product WHERE model = '" . $ActiveRevisionID . "'");
                            if(isset($query->rows[0]['product_id'])){
                                $this->model_catalog_product->editProduct($query->rows[0]['product_id'],$producto);
                                $logger->write('Se actualizó el libro: ' . $ActiveRevisionID . ' en la base de datos');
                                $actualizados ++;
                            }
                            else{
                                $this->model_catalog_product->addProduct($producto);
                                $logger->write('Se guardo el libro: ' . $ActiveRevisionID . ' en la base de datos');
                                $agregados ++;
                            }
                        } else {
                            $this->load->model('catalog/product');
                            $isbn = $linea4['isbn'];
                            if (isset($isbn))
                            {
                                $this->model_catalog_product->disableProduct($isbn);
                                $logger->write('El libro con id: ' . $linea4['@attributes']['id'] . ' no cuenta con "active revision. Desactivado en Opencart."');
                                $desactivados++;
                            }
                        }
                        $lineas ++;
                    }
                    fclose($archivoAbierto);
                    $logger->write('Libros agregados: ' . $agregados . ' /libros actuaizados: ' . $actualizados . ' /libros desactivados: ' . $desactivados);
                    $data['resultados'][] = array(
                        'agregados'  => $agregados,
                        'actualizados'       => $actualizados,
                        'desactivados'       => $desactivados,
                    );
                    $data['agregados'] = $agregados;
                    $data['actualizados'] = $actualizados;
                    $data['desactivados'] = $desactivados;
                    $estatus = 'exitoso';
                } else {
                    $logger->write('El Archivo no se puedo Abrir');
                    $estatus = 'fallido';
                }
                $hora_fin = date('H:i:s');
                $this->db->query("INSERT INTO " . DB_PREFIX . "actualizacion_libros SET fecha_actualizacion = '" . $fecha_actualizacion . "', nombre_archivo = '" . $a . "', total_entradas = '" . (int)$lineas . "', libros_agregados = '" . (int)$agregados . "', libros_actualizados = '" . (int)$actualizados . "', hora_inicio = '" . $hora_inicio . "', hora_fin = '" . $hora_fin . "', estatus = '" . $estatus . "'");
            }
        }
        $this->load->language('error/permission');
        $this->document->setTitle('Actualización de deltas');
        $data['heading_title'] = 'Actualización de deltas';
        $data['text_permission'] = 'Actualización de deltas';
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => 'Actualización de deltas',
            'href' => $this->url->link('script/update', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('script/update', $data));
    }
    public function obtenerAutor($infoLibro)
    {
        $aut = '';
        if ($infoLibro) {
            //$aut = ' / ';
            // $what=isset($infoLibro['activeRevision']['contibutors']['contributor']['@value']);
            if (!isset($infoLibro['contributor']))
            {
                $holder = "Autor Indefinido";
            }
            else{
                if (isset($infoLibro['contributor']['@value'])) {
                    if($infoLibro['contributor']['@attributes']['type']=='Author' || $infoLibro['contributor']['@attributes']['type']=='Editor')
                    {
                        $holder = $infoLibro['contributor']['@value'];
                    }
                    else{
                        $holder = "Autor Indefinido";
                    }
                    //$aut .= $infoLibro['contributor']['@value'];
                } else {
                    $autores = $infoLibro['contributor'];
                    $count = count($autores);
                    $n = 0;
                    foreach ($autores as $autor) {
                        if (!($n > 0)) {
                            if($autor['@attributes']['type']=='Author' || $autor['@attributes']['type']=='Editor')
                            {
                                $holder = $autor['@value'];
                                $n++;
                            }
                        }
                    }
                }
            }
        }
        if(isset($holder))
        {
            return $holder;
        }
        else{
            return "Autor Indefinido";
        }
    }
}