<?php
// Heading
$_['heading_title']    = 'Favoritos - Contenido de p&aacute;gina';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Favoritos (Contenido) module!';
$_['text_edit']        = 'Edit Favoritos (Contenido) Module';

// Entry
$_['header_add']       = 'Agregar productos';
$_['entry_name']       = 'Module Name';
$_['entry_product']    = 'T&iacute;tulo';;
$_['entry_color']      = 'Color';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_autoplay']   = 'AutoPlay Speed';
$_['entry_status']     = 'Status';
$_['text_entry_model']     = 'Active Revision ID';
$_['text_isbn']     = 'ISBN';
$_['text_paginas']     = 'N&uacute;mero de promoci&oacute;n';


// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Promotions module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';