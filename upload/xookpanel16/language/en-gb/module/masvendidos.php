<?php
// Heading
$_['heading_title']    = 'Más Vendidos';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Más Vendidos module!';
$_['text_edit']        = 'Edit Más Vendidos Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_image']      = 'Image (W x H) and Resize Type';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_autoplay']   = 'AutoPlay Speed';
$_['entry_status']     = 'Status';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify más vendidos module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';