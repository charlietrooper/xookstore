<?php
// Heading
$_['heading_title']    = 'Barra de categor&iacute;as';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Barra de categor&iacute;as module!';
$_['text_edit']        = 'Edit Barra de categor&iacute;as Module';

// Entry
$_['header_add']       = 'Agregar productos';
$_['entry_name']       = 'Module Name';
$_['entry_categories']       = 'Categor&iacute;a';
$_['entry_container']       = 'Contenedor';
$_['entry_product']    = 'T&iacute;tulo';;
$_['entry_color']      = 'Color';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_autoplay']   = 'AutoPlay Speed';
$_['entry_status']     = 'Status';
$_['text_entry_model']     = 'Active Revision ID';
$_['entry_texto']     = 'Texto';



// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Promociones module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_text']       = 'El texto que aparecer&aacute; en el m&oacute;dulo no puede tener m&aacute;s de 12 caracteres!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';