<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 06/07/2016
 * Time: 09:54 AM
 */

class ModelCatalogUpdate extends Model {

    /*
     * Función para obtener el precio
     *
     * @param array $price
     * @return $precio
     */
    public function obtenerPrecio($price = array()) {
        //Se obtiene la fecha actual
        $hoy = strtotime(date('Y-m-d H:i:s'));
        //Se obtienen los datos de las fechas y del preorden
        $from = strtotime($price['@attributes']['from']);

        //Se verifica si en la colección de precios que esta en raíz de price, coincide con el rango de fechas y si es preorder en caso de que coincida
        $rangofecha = (isset($price['@attributes']['to']) ? ($from <= $hoy && $hoy <= strtotime($price['@attributes']['to'])) : $from <= $hoy) ? true : false;

        //Si se cumplen las condiciones anteriores, el libro es pre orden, sino, se revizarán las siguientes colecciones de precios en caso de que existan
        if($rangofecha == true)
        {
            if(isset($price['sellingprice']['@value'])) {
                $precio = $price['sellingprice']['@value'];
            } else {
                $precio = $price['sellingprice'];
            }
        }
        else {
            foreach ($price as $p => $val) {
                if (isset($val['@attributes']['currency'])) {
                    //Se define la fecha
                    $From = $val['@attributes']['from'];

                    //Se verifica si la coleccion de precios de los arrays 0,1...n, coincide con el rango de fechas
                    $RangoFecha = (isset($val['@attributes']['to']) ? ($From <= $hoy && $hoy <= $val['@attributes']['to']) : $From <= $hoy) ? true : false;

                    //Si se cumple la condicion anteriorse toma el valor de cogs
                    if ($RangoFecha == true) {
                        if(isset($val['sellingprice']['@value']))
                        {
                            $precio = $val['sellingprice']['@value'];
                        } else {
                            $precio = $val['sellingprice'];
                        }

                        break;
                    }
                }
            }
            if(!isset($precio)){
                $precio = null;
            }
        }

        return $precio;
    }

    /*
     * Función que obriene el género
     *
     * @param array $category
     * @return genero =  $genero['tipo_genero']['nombre_genero'];
     */
    public function obtenerGeneroNuevo($category)
    {
        if(isset($category['$undefined']))
        {
            $genero_final = 144;
        }

        else{
            $generos = array(
                'Actividades y dibujo'	=>	60,//'Actividades y dibujo',
                'Agricultura y ganadería'	=>	61,//'Agricultura y ganadería',
                'Antigüedades y coleccionables'	=>	62,//'Antigüedades y coleccionables',
                'Antologías'	=>	63,//'Antologías',
                'Arqueología'	=>	64,//'Arqueología',
                'Arquitectura'	=>	65,//'Arquitectura',
                'Arte'	=>	66,//'Arte',
                'Artes escénicas'	=>	67,//'Artes escénicas',
                'Artículos de escritorio, infantiles y varios'	=>	68,//'Artículos de escritorio, infantiles y varios',
                'Astronomía, espacio y tiempo'	=>	69,//'Astronomía, espacio y tiempo',
                'Asuntos personales y sociales'	=>	70,//'Asuntos personales y sociales',
                'Autoayuda y desarrollo personal'	=>	71,//'Autoayuda y desarrollo personal',
                'Auxiliares de estudio'	=>	72,//'Auxiliares de estudio',
                'Aventura'	=>	73,//'Aventura',
                'Bases de datos'	=>	74,//'Bases de datos',
                'Biografía y autobiografía'	=>	75,//'Biografía y autobiografía',
                'Biología'	=>	76,//'Biología',
                'Ciencia ficción'	=>	77,//'Ciencia ficción',
                'Ciencias'	=>	78,//'Ciencias',
                'Ciencias sociales'	=>	79,//'Ciencias sociales',
                'Cine, TV y radio'	=>	80,//'Cine, TV y radio',
                'Clásicos'	=>	81,//'Clásicos',
                'Cocina'	=>	82,//'Cocina',
                'Colecciónes literarias'	=>	83,//'Colecciónes literarias',
                'Comics y novelas gráficas'	=>	84,//'Comics y novelas gráficas',
                'Computación'	=>	85,//'Computación',
                'Crimen y misterio'	=>	86,//'Crimen y misterio',
                'Crítica e historia literaria'	=>	87,//'Crítica e historia literaria',
                'Danza'	=>	88,//'Danza',
                'Diseño'	=>	89,//'Diseño',
                'Educación'	=>	90,//'Educación',
                'Electrónica y comunicaciones'	=>	91,//'Electrónica y comunicaciones',
                'Enfermería'	=>	92,//'Enfermería',
                'Erótica'	=>	93,//'Erótica',
                'Estilo de vida'	=>	94,//'Estilo de vida',
                'Estudios interdisciplinarios'	=>	95,//'Estudios interdisciplinarios',
                'Familia'	=>	96,//'Familia',
                'Fantasía'	=>	97,//'Fantasía',
                'Ficción'	=>	98,//'Ficción',
                'Filosofía'	=>	99,//'Filosofía',
                'Finanzas y contabilidad'	=>	100,//'Finanzas y contabilidad',
                'Física'	=>	101,//'Física',
                'Fotografía'	=>	102,//'Fotografía',
                'Geografía'	=>	103,//'Geografía',
                'Guerra'	=>	104,//'Guerra',
                'Historia'	=>	105,//'Historia',
                'Historia del arte'	=>	106,//'Historia del arte',
                'Historía natural'	=>	107,//'Historía natural',
                'Hogar y mantenimiento'	=>	108,//'Hogar y mantenimiento',
                'Horror y fantasmas'	=>	109,//'Horror y fantasmas',
                'Humor'	=>	110,//'Humor',
                'Idiomas'	=>	111,//'Idiomas',
                'Industria'	=>	112,//'Industria',
                'Infantil: Ficción'	=>	113,//'Infantil: Ficción',
                'Infantil: General'	=>	114,//'Infantil: General',
                'Infantil: Poesía'	=>	115,//'Infantil: Poesía',
                'Ingeniería ambiental'	=>	117,//'Ingeniería ambiental',
                'Ingeniería bioquímica'	=>	118,//'Ingeniería bioquímica',
                'Ingeniería civil'	=>	119,//'Ingeniería civil',
                'Ingeniería energética'	=>	120,//'Ingeniería energética',
                'Ingeniería mecánica'	=>	121,//'Ingeniería mecánica',
                'Interés local'	=>	122,//'Interés local',
                'Investigación'	=>	123,//'Investigación',
                'Jardinería'	=>	124,//'Jardinería',
                'Juegos y videojuegos'	=>	125,//'Juegos y videojuegos',
                'Juvenil: Ficción'	=>	126,//'Juvenil: Ficción',
                'Juvenil: No ficción'	=>	127,//'Juvenil: No ficción',
                'Leyes'	=>	128,//'Leyes',
                'Lingüística'	=>	129,//'Lingüística',
                'Manualidades y pasatiempos'	=>	130,//'Manualidades y pasatiempos',
                'Mascotas'	=>	131,//'Mascotas',
                'Matemáticas'	=>	132,//'Matemáticas',
                'Medicina'	=>	133,//'Medicina',
                'Medio ambiente'	=>	134,//'Medio ambiente',
                'Medios gráficos y digitales'	=>	135,//'Medios gráficos y digitales',
                'Mitos y leyendas'	=>	136,//'Mitos y leyendas',
                'Museología'	=>	137,//'Museología',
                'Música'	=>	138,//'Música',
                'Naturaleza'	=>	139,//'Naturaleza',
                'Negocios y economía'	=>	140,//'Negocios y economía',
                'No ficción'	=>	141,//'No ficción',
                'Novela histórica'	=>	142,//'Novela histórica',
                'Novela religiosa'	=>	143,//'Novela religiosa',
                'Otros'	=>	144,//'Otros',
                'Planeación de areas y regiones'	=>	145,//'Planeación de areas y regiones',
                'Poesía'	=>	146,//'Poesía',
                'Política y gobierno'	=>	147,//'Politica y gobierno',
                'Psicología'	=>	148,//'Psicología',
                'Química'	=>	149,//'Química',
                'Redes y comunicaciones'	=>	150,//'Redes y comunicaciones',
                'Referencia'	=>	151,//'Referencia',
                'Referencia: infantil'	=>	152,//'Referencia: infantil',
                'Religión'	=>	153,//'Religión',
                'Romance'	=>	154,//'Romance',
                'Salud y deportes'	=>	155,//'Salud y deportes',
                'Servicio social'	=>	156,//'Servicio social',
                'Sociedad y cultura'	=>	157,//'Sociedad y cultura',
                'Sociología y antropología'	=>	158,//'Sociología y antropología',
                'Teatro'	=>	159,//'Teatro',
                'Tecnología'	=>	160,//'Tecnología',
                'Terror y suspenso'	=>	161,//'Terror y suspenso',
                'Transporte y logística'	=>	162,//'Transporte y logística',
                'Veterinaria'	=>	163,//'Veterinaria',
                'Viajes'	=>	164,//'Viajes'
            );

            if(isset($generos["$category"])) {
                $genero_final = $generos["$category"];
            } else {
                $genero_final = 144;
            }
        }

        return $genero_final;
    }

    public function obtenerGenero($category = array()) {
        $tipo_genero = '';
        $genero = '';

        if(!isset($category)){
            $tipo_genero = 'No definido';
            $genero = 'No definido';
        }
        elseif(isset($category['@attributes'])){
            $tipo_genero = $category['@attributes']['system'];
            $genero = $category['@value'];
        }
        else{
            if($category['0']['@attributes']['system'] == 'BIC' || $category['0']['@attributes']['system'] == 'BISAC') {
                $tipo_genero = $category['0']['@attributes']['system'];
                $genero = $category['0']['@value'];
            } else {
                foreach ($category as $gen) {
                    if ($gen['@attributes']['system'] == 'BIC' || $gen['@attributes']['system'] == 'BISAC') {
                        $tipo_genero = $gen['@attributes']['system'];
                        $genero = $gen['@value'];
                        break;
                    }
                }
            }
        }

        if ($tipo_genero == 'BIC') {
            $id_genero = substr($genero, 0, 2);
        } elseif ($tipo_genero == 'BISAC') {
            $id_genero = substr($genero, 0, 3);
        } else {
            $id_genero = '';
        }

        $generos = array(
            'BIC'   => array(
                'YB'	=>	60,//'Actividades y dibujo',
                'TV'	=>	61,//'Agricultura y ganadería',
                'WC'	=>	62,//'Antigüedades y coleccionables',
                'DQ'	=>	68,//'Antologías',
                'HD'	=>	64,//'Arqueología',
                'AM'	=>	65,//'Arquitectura',
                'AB'	=>	66,//'Arte',
                'AF'	=>	66,//'Arte',
                'AG'	=>	66,//'Arte',
                'AK'	=>	66,//'Arte',
                'YZ'	=>	68,//'Artículos de escritorio, infantiles y varios',
                'PG'	=>	69,//'Astronomía, espacio y tiempo',
                'YX'	=>	70,//'Asuntos personales y sociales',
                'VS'	=>	71,//'Autoayuda y desarrollo personal',
                'FJ'	=>	73,//'Aventura',
                'UN'	=>	74,//'Bases de datos',
                'BG'	=>	75,//'Biografía y autobiografía',
                'BJ'	=>	75,//'Biografía y autobiografía',
                'BK'	=>	75,//'Biografía y autobiografía',
                'BM'	=>	75,//'Biografía y autobiografía',
                'PS'	=>	76,//'Biología',
                'FL'	=>	77,//'Ciencia ficción',
                'PD'	=>	78,//'Ciencias',
                'RB'	=>	78,//'Ciencias',
                'AP'	=>	80,//'Cine, TV y radio',
                'DB'	=>	81,//'Clásicos',
                'WB'	=>	82,//'Cocina',
                'FX'	=>	84,//'Comics y novelas gráficas',
                'UK'	=>	85,//'Computación',
                'UL'	=>	85,//'Computación',
                'UM'	=>	85,//'Computación',
                'UQ'	=>	85,//'Computación',
                'UR'	=>	85,//'Computación',
                'UY'	=>	85,//'Computación',
                'FF'	=>	86,//'Crimen y misterio',
                'DS'	=>	87,//'Crítica e historia literaria',
                'AS'	=>	88,//'Danza',
                'EB'	=>	90,//'Educación',
                'EL'	=>	90,//'Educación',
                'ES'	=>	90,//'Educación',
                'JN'	=>	90,//'Educación',
                'YQ'	=>	90,//'Educación',
                'TJ'	=>	91,//'Electrónica y comunicaciones',
                'MQ'	=>	92,//'Enfermería',
                'FP'	=>	93,//'Erótica',
                'UD'	=>	94,//'Estilo de vida',
                'WJ'	=>	94,//'Estilo de vida',
                'GT'	=>	95,//'Estudios interdisciplinarios',
                'FM'	=>	97,//'Fantasía',
                'BT'	=>	98,//'Ficción',
                'FA'	=>	98,//'Ficción',
                'FC'	=>	98,//'Ficción',
                'FY'	=>	98,//'Ficción',
                'FZ'	=>	98,//'Ficción',
                'HP'	=>	99,//'Filosofía',
                'KF'	=>	100,//'Finanzas y contabilidad',
                'PH'	=>	101,//'Física',
                'AJ'	=>	102,//'Fotografía',
                'RG'	=>	103,//'Geografía',
                'JW'	=>	104,//'Guerra',
                'AC'	=>	106,//'Historia del arte',
                'WN'	=>	107,//'Historía natural',
                'HB'	=>	105,//'Historia',
                'WK'	=>	108,//'Hogar y mantenimiento',
                'FK'	=>	109,//'Horror y fantasmas',
                'WH'	=>	110,//'Humor',
                'CB'	=>	111,//'Idiomas',
                'CJ'	=>	111,//'Idiomas',
                'KN'	=>	112,//'Industria',
                'YF'	=>	113,//'Infantil: Ficción',
                'YN'	=>	114,//'Infantil: General',
                'YD'	=>	115,//'Infantil: Poesía',
                'TQ'	=>	117,//'Ingeniería ambiental',
                'TC'	=>	118,//'Ingeniería bioquímica',
                'TN'	=>	119,//'Ingeniería civil',
                'TH'	=>	120,//'Ingeniería energética',
                'TG'	=>	121,//'Ingeniería mecáncia',
                'WQ'	=>	122,//'Interés local',
                'GP'	=>	123,//'Investigación',
                'WM'	=>	124,//'Jardinería',
                'LA'	=>	128,//'Leyes',
                'LB'	=>	128,//'Leyes',
                'LN'	=>	128,//'Leyes',
                'LR'	=>	128,//'Leyes',
                'CF'	=>	129,//'Lingüística',
                'WD'	=>	130,//'Manualidades y pasatiempos',
                'WF'	=>	130,//'Manualidades y pasatiempos',
                'PB'	=>	132,//'Matemáticas',
                'MB'	=>	133,//'Medicina',
                'MF'	=>	133,//'Medicina',
                'MJ'	=>	133,//'Medicina',
                'MM'	=>	133,//'Medicina',
                'MN'	=>	133,//'Medicina',
                'MR'	=>	133,//'Medicina',
                'MX'	=>	133,//'Medicina',
                'RN'	=>	134,//'Medio ambiente',
                'UG'	=>	135,//'Medios gráficos y digitales',
                'FQ'	=>	136,//'Mitos y leyendas',
                'FT'	=>	136,//'Mitos y leyendas',
                'GM'	=>	137,//'Museología',
                'AV'	=>	138,//'Música',
                'KC'	=>	140,//'Negocios y economía',
                'KJ'	=>	140,//'Negocios y economía',
                'UF'	=>	140,//'Negocios y economía',
                'DN'	=>	141,//'No ficción',
                'FV'	=>	142,//'Novela histórica',
                'FW'	=>	142,//'Novela religiosa',
                'WZ'	=>	144,//'Otros',
                'RP'	=>	145,//'Planeación de areas y regiones',
                'DC'	=>	146,//'Poesía',
                'JP'	=>	147,//'Politica y gobierno',
                'JM'	=>	148,//'Psicología',
                'PN'	=>	149,//'Química',
                'TD'	=>	149,//'Química',
                'UT'	=>	150,//'Redes y comunicaciones',
                'GB'	=>	151,//'Referencia',
                'GL'	=>	151,//'Referencia',
                'YR'	=>	152,//'Referencia: infantil',
                'HR'	=>	153,//'Religión',
                'FR'	=>	154,//'Romance',
                'VF'	=>	155,//'Salud y deportes',
                'VX'	=>	155,//'Salud y deportes',
                'WS'	=>	155,//'Salud y deportes',
                'JK'	=>	156,//'Servicio social',
                'JF'	=>	157,//'Sociedad y cultura',
                'JH'	=>	158,//'Sociología y antropología',
                'AN'	=>	159,//'Teatro',
                'DD'	=>	159,//'Teatro',
                'TB'	=>	160,//'Tecnología',
                'TT'	=>	160,//'Tecnología',
                'UB'	=>	160,//'Tecnología',
                'FH'	=>	161,//'Terror y suspenso',
                'TR'	=>	162,//'Transporte y logística',
                'WG'	=>	162,//'Transporte y logística',
                'MZ'	=>	163,//'Veterinaria',
                'WT'	=>	164,//'Viajes'

            ),
            'BISAC' => array(
                'ANT'	=>	62,//'Antigüedades y coleccionables',
                'ARC'	=>	65,//'Arquitectura',
                'ART'	=>	66,//'Arte',
                'PER'	=>	67,//'Artes escénicas',
                'SEL'	=>	71,//'Autoayuda y desarrollo personal',
                'STU'	=>	72,//'Auxiliares de estudio',
                'BIO'	=>	75,//'Biografía y autobiografía',
                'SOC'	=>	79,//'Ciencias sociales',
                'SCI'	=>	78,//'Ciencias',
                'CKB'	=>	82,//'Cocina',
                'LCO'	=>	83,//'Colecciónes literarias',
                'CGN'	=>	84,//'Comics y novelas gráficas',
                'COM'	=>	85,//'Computación',
                'TRU'	=>	86,//'Crimen y misterio',
                'LIT'	=>	87,//'Crítica e historia literaria',
                'DES'	=>	89,//'Diseño',
                'EDU'	=>	90,//'Educación',
                'FAM'	=>	96,//'Familia',
                'FIC'	=>	98,//'Ficción',
                'PHI'	=>	99,//'Filosofía',
                'PHO'	=>	102,//'Fotografía',
                'HIS'	=>	105,//'Historia',
                'HOM'	=>	108,//'Hogar y mantenimiento',
                'HUM'	=>	110,//'Humor',
                'FOR'	=>	111,//'Idiomas',
                'LAN'	=>	111,//'Idiomas',
                'GAR'	=>	124,//'Jardinería',
                'GAM'	=>	125,//'Juegos y videojuegos',
                'JUV'	=>	126,//'Juvenil: Ficción',
                'YAF'	=>	126,//'Juvenil: Ficción',
                'YAN'	=>	127,//'Juvenil: No ficción',
                'JNF'	=>	127,//'Juvenil: No ficción',
                'LAW'	=>	128,//'Leyes',
                'CRA'	=>	130,//'Manualidades y pasatiempos',
                'PET'	=>	131,//'Mascotas',
                'MAT'	=>	132,//'Matemáticas',
                'MED'	=>	133,//'Medicina',
                'MUS'	=>	138,//'Música',
                'NAT'	=>	139,//'Naturaleza',
                'BUS'	=>	140,//'Negocios y economía',
                'POE'	=>	146,//'Poesía',
                'POL'	=>	147,//'Política y gobierno',
                'PSY'	=>	148,//'Psicología',
                'REF'	=>	151,//'Referencia',
                'BIB'	=>	153,//'Religión',
                'REL'	=>	153,//'Religión',
                'HEA'	=>	155,//'Salud y deportes',
                'OCC'	=>	155,//'Salud y deportes',
                'SPO'	=>	155,//'Salud y deportes',
                'DRA'	=>	159,//'Teatro',
                'TEC'	=>	160,//'Tecnología',
                'TRA'	=>	162,//'Transporte y logística',
                'TRV'	=>	164,//'Viajes'

            )
        );

        if(isset($generos["$tipo_genero"]["$id_genero"])) {
            $genero_final = $generos["$tipo_genero"]["$id_genero"];
        } else {
            $genero_final = 144;
        }
        return $genero_final;
    }
}