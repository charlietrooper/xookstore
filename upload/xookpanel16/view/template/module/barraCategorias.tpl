<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-featured" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
          <!--Se manda a llamar text_edit que en el php de admin, se obtiene desde en Language-->
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
          <!--$action se obtiene en el admin a partir del link/url-->

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-featured" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                    <select name="status" id="input-status" class="form-control">
                        <?php if ($status) { ?>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <hr>
            <h3><?php echo $entry_container; ?> 1</h3>
          <div class="form-group">
              <label class="col-sm-2 control-label" for="input-categories"><?php echo $entry_categories; ?> 1</label>
              <div class="col-sm-5">
                  <select name="categories1" id="input-categories" class="form-control">
                      <?php foreach ($allCategories[0]['children'] as $cat) { ?>
                      <?php if ($cat['category_id'] == $categories1) { ?>
                      <option value="<?php echo $cat['category_id']; ?>" selected="selected"><?php echo $cat['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                  </select>
              </div>
              <label class="col-sm-2 control-label" for="input-alias1"><?php echo $entry_texto; ?> 1</label>
              <div class="col-sm-3">
                  <input type="text" name="alias1" value="<?php echo $alias1; ?>" placeholder="<?php echo $entry_texto; ?>" id="input-text1" class="form-control" />
                  <?php if ($error_text1) { ?>
                  <div class="text-danger"><?php echo $error_text1; ?></div>
                  <?php } ?>
              </div>
          </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-color"><?php echo $entry_color; ?> 1</label>
            <div class="col-sm-10">
                <select name="color1" id="input-Color" class="form-control">
                    <?php foreach ($colors as $col) { ?>
                    <?php if ($col == $color1) { ?>
                    <option value="<?php echo $col; ?>" selected="selected"><?php echo $col; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $col; ?>"><?php echo $col; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div>

            <hr>
            <h3><?php echo $entry_container; ?> 2</h3>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-categories"><?php echo $entry_categories; ?> 2</label>
                <div class="col-sm-5">
                    <select name="categories2" id="input-categories" class="form-control">
                        <?php foreach ($allCategories[0]['children'] as $cat) { ?>
                        <?php if ($cat['category_id'] == $categories2) { ?>
                        <option value="<?php echo $cat['category_id']; ?>" selected="selected"><?php echo $cat['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <label class="col-sm-2 control-label" for="input-alias2"><?php echo $entry_texto; ?> 2</label>
                <div class="col-sm-3">
                    <input type="text" name="alias2" value="<?php echo $alias2; ?>" placeholder="<?php echo $entry_texto; ?>" id="input-text2" class="form-control" />
                    <?php if ($error_text2) { ?>
                    <div class="text-danger"><?php echo $error_text2; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-color"><?php echo $entry_color; ?> 2</label>
                <div class="col-sm-10">
                    <select name="color2" id="input-Color" class="form-control">
                        <?php foreach ($colors as $col) { ?>
                        <?php if ($col == $color2) { ?>
                        <option value="<?php echo $col; ?>" selected="selected"><?php echo $col; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $col; ?>"><?php echo $col; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <hr>
            <h3><?php echo $entry_container; ?> 3</h3>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-categories"><?php echo $entry_categories; ?> 3</label>
                <div class="col-sm-5">
                    <select name="categories3" id="input-categories" class="form-control">
                        <?php foreach ($allCategories[0]['children'] as $cat) { ?>
                        <?php if ($cat['category_id'] == $categories3) { ?>
                        <option value="<?php echo $cat['category_id']; ?>" selected="selected"><?php echo $cat['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <label class="col-sm-2 control-label" for="input-alias3"><?php echo $entry_texto; ?> 3</label>
                <div class="col-sm-3">
                    <input type="text" name="alias3" value="<?php echo $alias3; ?>" placeholder="<?php echo $entry_texto; ?>" id="input-text3" class="form-control" />
                    <?php if ($error_text3) { ?>
                    <div class="text-danger"><?php echo $error_text3; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-color"><?php echo $entry_color; ?> 3</label>
                <div class="col-sm-10">
                    <select name="color3" id="input-Color" class="form-control">
                        <?php foreach ($colors as $col) { ?>
                        <?php if ($col == $color3) { ?>
                        <option value="<?php echo $col; ?>" selected="selected"><?php echo $col; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $col; ?>"><?php echo $col; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>

            </div>

            <hr>
            <h3><?php echo $entry_container; ?> 4</h3>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-categories"><?php echo $entry_categories; ?> 4</label>
                <div class="col-sm-5">
                    <select name="categories4" id="input-categories" class="form-control">
                        <?php foreach ($allCategories[0]['children'] as $cat) { ?>
                        <?php if ($cat['category_id'] == $categories4) { ?>
                        <option value="<?php echo $cat['category_id']; ?>" selected="selected"><?php echo $cat['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <label class="col-sm-2 control-label" for="input-alias4"><?php echo $entry_texto; ?> 4</label>
                <div class="col-sm-3">
                    <input type="text" name="alias4" value="<?php echo $alias4; ?>" placeholder="<?php echo $entry_texto; ?>" id="input-text4" class="form-control" />
                    <?php if ($error_text4) { ?>
                    <div class="text-danger"><?php echo $error_text4; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-color"><?php echo $entry_color; ?> 4</label>
                <div class="col-sm-10">
                    <select name="color4" id="input-Color" class="form-control">
                        <?php foreach ($colors as $col) { ?>
                        <?php if ($col == $color4) { ?>
                        <option value="<?php echo $col; ?>" selected="selected"><?php echo $col; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $col; ?>"><?php echo $col; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->


        </form>
      </div>
    </div>
  </div>
    <script type="text/javascript"><!--
        $('#submitISBN').on('click', function ()
        {
            var dataISBN= $('input[name=\'product_ISBN\']').val();

            $.ajax({
                type: "POST",
                data: {data:dataISBN},
                //url: 'index.php?route=catalog/product/addByISBN&token=<?php echo $token; ?>&isbn=' +  dataISBN
                url: 'index.php?route=catalog/product/addByISBN&token=<?php echo $token; ?>',
                success: function(productos) {
                        for (var i = 0, len = productos['libros'].length; i < len; i++) {
                            $('#featured-product' + productos['libros'][i]['product_id']).remove();

                            $('#featured-product').append('<div id="featured-product' + productos['libros'][i]['product_id'] + '"><i class="fa fa-minus-circle"></i> ' + productos['libros'][i]['name'] + ' ( ' + productos['libros'][i]['isbn'] + ' )<input type="hidden" name="product[]" value="' + productos['libros'][i]['product_id'] + '" /></div>');
                           // console.log(productos[i]['name']);
                        }
                        if(productos['error']!=="")
                        {
                            alert("Los siguientes ISBNs no se encontraron: "+productos['error']);
                        }
                    },
                dataType:"json"
                });


        });
    //--></script>

  <script type="text/javascript"><!--

    $('input[name=\'product_name\']').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        select: function(item) {
            $('input[name=\'product_name\']').val('');

            $('#featured-product' + item['value']).remove();

            $('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');
           // $('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i>HOLA <input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');

        }
    });

      $('input[name=\'product_ISBN\']').autocomplete({
          source: function(request, response) {
              $.ajax({
                  url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_isbn=' +  encodeURIComponent(request),
                  dataType: 'json',
                  success: function(json) {
                      response($.map(json, function(item) {
                          return {
                              label: item['name'],
                              value: item['product_id']
                          }
                      }));
                  }
              });
          },
          select: function(item) {
              $('input[name=\'product_ISBN\']').val('');

              $('#featured-product' + item['value']).remove();

              $('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');
              // $('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i>HOLA <input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');

          }
      });


      $('input[name=\'filter_model\']').autocomplete({
          'source': function(request, response) {
              $.ajax({
                  url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
                  dataType: 'json',
                  success: function(json) {
                      response($.map(json, function(item) {
                          return {
                              label: item['name'],
                              value: item['product_id']
                          }
                      }));
                  }
              });
          },
          'select': function(item) {
              $('input[name=\'filter_model\']').val('');
              $('#featured-product' + item['value']).remove();

              $('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');

          }
      });

    $('#featured-product').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });


//--></script></div>
<?php echo $footer; ?>
