<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-featured" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
          <!--Se manda a llamar text_edit que en el php de admin, se obtiene desde en Language-->
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
          <!--$action se obtiene en el admin a partir del link/url-->

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-featured" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
          <div class="form-group">
              <h4 style="font-weight: bold; margin-left:10px;"><?php echo $header_add; ?></h4>
              <hr>
              <div class="row">
                  <div class="col-md-2">
                      <label class="col-sm-2 control-label" for="input-product"><span data-toggle="tooltip" title="<?php echo $help_product; ?>"><?php echo $entry_product; ?></span></label>
                  </div>
                  <div class="col-md-4">
                      <input type="text" name="product_name" value="" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
                  </div>
                  <div class="col-md-2">
                      <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
                  </div>
                  <div class="col-md-4">
                      <input type="text" name="filter_model" value="" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
                  </div>
              </div>
              <div class="row" style="margin:20px 0;">
                  <div class="col-md-1">
                      <label class="control-label" for="input-model"><?php echo $text_isbn; ?></label>
                  </div>
                  <div class="col-md-9">
                      <input type="textarea" rows="4" name="product_ISBN" value="" placeholder="<?php echo $text_isbn; ?>" id="input-isbn" class="form-control" />
                  </div>
                  <div class="col-md-2">
                      <input type='button' id="submitISBN" value='Agregar'>
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-9 col-md-offset-2">
                      <table id="featured-product" class="table table-striped" style="height: 150px; overflow: auto;">
                          <tbody>
                          <?php foreach ($products as $product) { ?>
                          <tr id="featured-product<?php echo $product['product_id']; ?>"><td><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                              <input type="hidden" name="product[]" value="<?php echo $product['product_id']; ?>" />
                              </td>
                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                  </div>

              </div>

          </div>
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------------------->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-Pagina"><?php echo $text_paginas; ?></label>
                <div class="col-sm-10">
                    <select name="paginaPromo" id="input-Pagina" class="form-control">
                        <?php for($i=1; $i<6; $i++) { ?>
                            <?php if($i==$paginaPromo) { ?>
                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                            <?php } else { ?>
                             <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                             <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
    <script>
        var fixHelperModified = function(e, tr) {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function(index) {
                        $(this).width($originals.eq(index).width())
                    });
                    return $helper;
                },
                updateIndex = function(e, ui) {
                    $('td.index', ui.item.parent()).each(function (i) {
                        // console.log($(this).children('input').val());
                        //  $(this).children('input').val(i + 1);
                        // $(this).html(i + 1);
                        // $('#module tr input[name=\'layout_module[][sort_order]\']:checked').val();

                    });
                };

        $("#featured-product tbody").sortable({
            helper: fixHelperModified,
            stop: updateIndex
        }).disableSelection();
    </script>
    <script type="text/javascript"><!--
        $('#submitISBN').on('click', function ()
        {
            var dataISBN= $('input[name=\'product_ISBN\']').val();

            $.ajax({
                type: "POST",
                data: {data:dataISBN},
                //url: 'index.php?route=catalog/product/addByISBN&token=<?php echo $token; ?>&isbn=' +  dataISBN
                url: 'index.php?route=catalog/product/addByISBN&token=<?php echo $token; ?>',
                success: function(productos) {
                        for (var i = 0, len = productos['libros'].length; i < len; i++) {
                            $('#featured-product' + productos['libros'][i]['product_id']).remove();

                            $('#featured-product').append('<tr id="featured-product' + productos['libros'][i]['product_id'] + '"><td><i class="fa fa-minus-circle"></i> ' + productos['libros'][i]['name'] + ' ( ' + productos['libros'][i]['isbn'] + ' )<input type="hidden" name="product[]" value="' + productos['libros'][i]['product_id'] + '" /></td></tr>');
                           // console.log(productos[i]['name']);
                        }
                        if(productos['error']!=="")
                        {
                            alert("Los siguientes ISBNs no se encontraron: "+productos['error']);
                        }
                    },
                dataType:"json"
                });


        });
    //--></script>

  <script type="text/javascript"><!--

    $('input[name=\'product_name\']').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        select: function(item) {
            $('input[name=\'product_name\']').val('');

            $('#featured-product' + item['value']).remove();

            $('#featured-product').append('<tr id="featured-product' + item['value'] + '"><td><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></td></tr>');
           // $('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i>HOLA <input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');

        }
    });

      $('input[name=\'product_ISBN\']').autocomplete({
          source: function(request, response) {
              $.ajax({
                  url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_isbn=' +  encodeURIComponent(request),
                  dataType: 'json',
                  success: function(json) {
                      response($.map(json, function(item) {
                          return {
                              label: item['name'],
                              value: item['product_id']
                          }
                      }));
                  }
              });
          },
          select: function(item) {
              $('input[name=\'product_ISBN\']').val('');

              $('#featured-product' + item['value']).remove();

              $('#featured-product').append('<tr id="featured-product' + item['value'] + '"><td><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></td></tr>');
              // $('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i>HOLA <input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');

          }
      });


      $('input[name=\'filter_model\']').autocomplete({
          'source': function(request, response) {
              $.ajax({
                  url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
                  dataType: 'json',
                  success: function(json) {
                      response($.map(json, function(item) {
                          return {
                              label: item['name'],
                              value: item['product_id']
                          }
                      }));
                  }
              });
          },
          'select': function(item) {
              $('input[name=\'filter_model\']').val('');
              $('#featured-product' + item['value']).remove();

              $('#featured-product').append('<tr id="featured-product' + item['value'] + '"><td><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></td></tr>');

          }
      });

    $('#featured-product').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });


//--></script></div>
<?php echo $footer; ?>
