<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php foreach ($resultados as $resultado) { ?>
                <h3 class="panel-title"><?php echo $heading_title; ?></h3>
                <br><br>
                <p><?php echo 'Libros agregados: ' . $resultado['agregados']; ?></p>
                <p><?php echo 'Libros actualizados: ' . $resultado['actualizados']; ?></p>
                <p><?php echo 'Libros desactivados: ' . $resultado['desactivados']; ?></p>
                <br><br>
                <?php } ?>
            </div>
            <div class="panel-body">

            </div>
        </div>
    </div>
    <?php echo $footer; ?>
</div>